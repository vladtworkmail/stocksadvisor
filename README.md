# Getting Started with Stocks Advisor

***
## Branches
### `main` - for production (use already deployed server).
### `development` - for developing new features (you have to run BE project as well).

***

## Files
### `.env` - create in root directory and contact with owner for the content.

***

## Run Scripts

### `npm start`

Runs the app in the `production` mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

It uses the `production` server that is already deployed.

### `npm run dev`

Runs the app in the `development` mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

You have to run the BE project as well.

***

## Deploy
 Each push to `main` automatically run the deploy process

