import { JWT, USER_ID, USER_MAIL, USER_NAME } from '../constants';

export const setupUserData = ({jwtToken, id, email, firstName}) => {
    if (!jwtToken || !email || !id || !firstName) {
        return false;
    }
    localStorage.setItem(JWT, jwtToken);
    localStorage.setItem(USER_ID, id);
    localStorage.setItem(USER_MAIL, email);
    localStorage.setItem(USER_NAME, firstName);
};
