import {toast} from "react-toastify";

export const requestErrorHandler = async (error) => {
    if (error?.request?.status === 404) {
        toast.error('Wrong link');
    } else if (error?.request?.status === 500) {
        toast.error('Something went wrong.');
    } else {
        toast.error(
            error?.response?.data?.error ||
            error?.response?.data?.message ||
            error.message
        );

    }
};
