export const countPriceChangeColor = price => {
    if(price < 0) return '#bf0806';
    if(price === 0) return 'grey';
    if(price > 0) return 'green';
};
