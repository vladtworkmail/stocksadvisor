export const countMarketPrice = price => {
    const stringPrice = price.toString();
    if(!stringPrice.includes('.')) return price;

    const splittedPrice = stringPrice.split('.');
    const abs = splittedPrice[0];
    const decimals = splittedPrice[1];
    const zerosInDecimals = [];

    for(let decimal of decimals.split('')) {
        if(decimal === '0') zerosInDecimals.push('0');
        else break;
    }

    const result = +`${abs}.${decimals.slice(0, zerosInDecimals.length + 3)}`
    
    return result;
};
