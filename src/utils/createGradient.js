
export const createGradient = (color1, color2) => `linear-gradient(to bottom, ${color1}, ${color2})`;
