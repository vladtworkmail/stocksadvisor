export const getPeterLynchFairValue = (eps = 0, dividend = 0, pe) => {
    const newPe = pe === 0 ? 0.1 : pe;
    return parseFloat(((eps + dividend) / newPe).toFixed(2));
};

export const getHistoricalPe = (historicalData) => (
    parseFloat((
        historicalData.reduce((acc, current) => acc + current.peRation, 0) / historicalData.length)
        .toFixed(2)
    )
);
