export const getChartColor = (chartData, theme) => {
    const firstDayValue = chartData?.length ? chartData[0]?.y || chartData[0] : 0;
    const todayValue = chartData?.length ? chartData[chartData.length - 1]?.y || chartData[chartData.length - 1] : 0;
    return firstDayValue > todayValue
        ? theme.palette.primary.main
        : theme.palette.success.dark;
};
