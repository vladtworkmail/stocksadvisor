import * as React from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

const Loader = ({ style, size }) => (
    <Box sx={{ display: 'flex', justifyContent: 'center', paddingTop: 3, ...style }}>
        <CircularProgress size={size || 40} />
    </Box>
);

export default Loader;
