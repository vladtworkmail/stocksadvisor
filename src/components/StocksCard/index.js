import { memo } from 'react';
import { useTheme } from '@mui/styles';
import { getChartColor } from '../../utils';
import useFetchItemCardQuote from 'src/hook/useFetchItemCardQuote';
import useFetchItemCardChart from 'src/hook/useFetchItemCardChart';
import ListItemCard from '../ListItemCard';
import { countPriceChangeColor } from '../../utils';
import { formatChartData } from 'src/formatters/chartData';
import { getChartOptions } from 'src/constants/chartOptions';

const StocksCard = ({ onClick, symbol, image, name, showImage }) => {
    const theme = useTheme();
    const {
        shortQuote: {
            latestPrice = 0,
            latestVolume = 0,
            change = 0,
            currency
        },
        shortQuoteIsLoading
    } = useFetchItemCardQuote(symbol);

    const { chartData, isLoadingChart } = useFetchItemCardChart(symbol);
    const stockChangeColor = countPriceChangeColor(change);
    const formattedChartData = formatChartData(chartData);
    const chartSeries = [{data: formattedChartData}];
    const chartOptions = getChartOptions(theme, getChartColor(formattedChartData, theme));
    
    return (
       <ListItemCard
            onClick={onClick}
            symbol={symbol}
            image={image}
            name={name}
            showImage={showImage}
            latestPrice={latestPrice}
            latestVolume={latestVolume}
            currency={currency}
            shortQuoteIsLoading={shortQuoteIsLoading}
            isLoadingChart={isLoadingChart}
            changeColor={stockChangeColor}
            chartSeries={chartSeries}
            chartOptions={chartOptions}
            chartData={chartData}
            change={change}
       />
    );
};

export default memo(StocksCard);
