import PropTypes from 'prop-types';
import {styled} from "@mui/material/styles";
import {Box, Card, CardActions, CardContent, Divider, Typography} from "@mui/material";
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import moment from "moment";
import {useTheme} from "@mui/styles";

const NewsContainer = styled(Card)(({theme}) => ({
    width: 300,
    backgroundColor: theme.palette.widgetColor,
    color: theme.palette.widgetItemTextColor,
    boxShadow: 'none',
    transition: '.1s',
    '&:hover': {
        cursor: 'pointer',
        transform: 'scale(1.05)',
        zIndex: 1
    }
}));
export const ImageBox = styled(Box)(({backgroundImage}) => ({
    width: '100%',
    height: 200,
    opacity: 0.85,
    backgroundImage: `url('${backgroundImage}')`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center'
}));
const DateText = styled(Typography)(({theme}) => ({
    color: theme.palette.widgetItemTextColor,
    fontSize: 13
}));
const SummaryText = styled(Typography)(({theme}) => ({
    marginTop: 20,
    color: theme.palette.common.white,
    minHeight: 85,
    display: '-webkit-box',
    overflow: 'hidden',
    WebkitBoxOrient: 'vertical',
    WebkitLineClamp: 3,
}));
export const CardActionText = styled(Typography)(({theme}) => ({
    color: theme.palette.widgetItemTextColor,
    margin: 0,
    marginLeft: '3px !important',
    fontSize: 12
}));
const CustomActions = styled(CardActions)(({theme}) => ({
    paddingLeft: 20,
    paddingRight: 20,
}));
export const CustomDivider = styled(Divider)(({theme}) => ({
    borderColor: theme.palette.grey[500],
    marginLeft: 3
}));

export default function NewsCard({data, onNewsCardClick}) {
    const theme = useTheme();
    const { image, dateTime, source, headline } = data;
    return (
        <NewsContainer onClick={() => onNewsCardClick(data)} >
            <ImageBox backgroundImage={image}/>
            <CardContent>
                <DateText variant="subtitle1">
                    {moment(dateTime).format('ll')}
                </DateText>
                <SummaryText variant="h6">
                    {headline || 'No headline...'}
                </SummaryText>
            </CardContent>
            <CustomActions>
                <AccessTimeIcon fontSize="small" style={{color: theme.palette.grey[600]}}/>
                <CardActionText variant="caption">
                    {moment(dateTime).startOf('day').fromNow()}
                </CardActionText>
                <CustomDivider orientation="vertical" flexItem/>
                <CardActionText variant="caption">
                    {source}
                </CardActionText>
            </CustomActions>
        </NewsContainer>
    )
}

NewsCard.propTypes = {
  post: PropTypes.object,
  index: PropTypes.number
};
