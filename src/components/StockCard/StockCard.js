import {Card} from '@mui/material';
import {fShortenNumber} from '../../utils/formatNumber';
import { styled } from '@mui/material/styles';


const CustomCard = styled(Card)(({theme}) => ({
    padding: '20px',
    minHeight: '100px',
    borderRadius: '10px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    '&:hover': {
        cursor: 'pointer',
        backgroundColor: theme.palette.grey[200]
    }
}));
const MainInfo = styled('div')(() => ({
    maxWidth: '60px',
    textAlign: 'center',
    '& h3': {
        opacity: 0.65
    }
}));
const Price = styled('h5')(() => ({
    fontSize: '17px',
    textAlign: 'center'
}));
const StockChange = styled('div')(() => ({
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
}));
const StockChangeText = styled('span')(() => ({
    fontSize: '15px',
}));
const SolutionText = styled('span')(() => ({
    fontSize: '10px',
    opacity: 0.8
}));

export default function StockCard({ stock, handleClick }) {
    const {
        symbol,
        regularMarketChangePercent,
        regularMarketPrice,
        financialCurrency,
        currency,
        regularMarketVolume,
        shortName, fullExchangeName
    } = stock;

    const name = shortName.includes(' ') ? shortName.split(' ').slice(0,3).join(' ') : shortName;
    const percentChanges = `${regularMarketChangePercent?.toFixed(2)}%`;
    const currencyValue = financialCurrency === 'USD' ? '$' : financialCurrency || currency;

    return (
        <CustomCard onClick={() => handleClick(symbol)}>
            <MainInfo>
                <h6>
                    { name }
                </h6>
            </MainInfo>
            <MainInfo>
                <h3>
                    { symbol }
                </h3>
                <Price>
                    { `${regularMarketPrice.toFixed(2)}${currencyValue}` }
                </Price>
            </MainInfo>
            <StockChange>
           <StockChangeText
                style={{
                    color: regularMarketChangePercent > 0
                        ? 'green'
                        : regularMarketChangePercent < 0 ? 'red' : 'grey'
                }}
           >
               {percentChanges}
           </StockChangeText>
                <div>
                <SolutionText>
                    Volume: {fShortenNumber(regularMarketVolume)}
                </SolutionText>
                </div>
            </StockChange>
        </CustomCard>
    );
}
