import React from 'react';
import {Box, Card, Tooltip} from '@mui/material';
import {styled} from '@mui/material/styles';
import {Icon} from '@iconify/react';
import ApexChart from 'react-apexcharts';
import {countMarketPrice, fShortenNumber } from '../../utils';
import slashIcon from '@iconify/icons-eva/slash-outline';
import Loader from "../Loader";
import {DEFAULT_COMPANY_LOGO, LIST_ITEM_CARD_CHART_HEIGHT, LIST_ITEM_CARD_CHART_WIDTH} from "../../constants";

const CustomCard = styled(Card)(({theme}) => ({
    display: 'flex',
    padding: '15px',
    backgroundColor: theme.palette.widgetColor,
    boxShadow: 'none',
    color: theme.palette.widgetItemTextColor,
    justifyContent: 'space-between',
    height: 150,
    width: 320,
    borderRadius: 10,
    flexWrap: 'wrap',
    transition: '.1s',
    '&:hover': {
        cursor: 'pointer',
        transform: 'scale(1.25)',
        zIndex: 1
    }
}));
const LeftInfo = styled(Box)(() => ({
    display: 'flex'
}));
const MainInfo = styled(Box)(() => ({
    display: 'flex',
    alignItems: 'center',
    maxWidth: '200px'
}));
const Symbol = styled('h3')(() => ({
    fontSize: '15px',
    opacity: 0.65
}));
const SymbolName = styled('h3')(() => ({
    fontSize: '11px',
    fontWeight: 'normal',
    maxWidth: 125,
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    marginLeft: 5
}));
const SymbolImage = styled('img')(() => ({
    height: 'auto',
    width: '45px',
    marginRight: '15px'
}));
const Price = styled('h5')(() => ({
    fontSize: '15px'
}));
const StockChange = styled('div')(() => ({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
    marginLeft: 'auto'
}));
const StockChangeText = styled('span')(() => ({
    fontSize: '15px',
}));
const SolutionText = styled('span')(() => ({
    fontSize: '10px',
    opacity: 0.8
}));
const ChartContainer = styled('div')(() => ({
    position: 'relative',
    width: '100%',
    height: 80,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
}));
const TransparentContainer = styled('div')(() => ({
    width: 'inherit',
    height: 'inherit',
    position: 'absolute',
    marginBottom: -20
}));
const EmptyGraphContainer = styled('div')(() => ({
    opacity: 0.6,
    paddingTop: 10,
    alignSelf: 'center',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
}));

const EmptyGraph = () => (
    <EmptyGraphContainer>
        <Icon icon={slashIcon} style={{fontSize: 30, opacity: 0.6}}/>
        <p>Not enough data</p>
    </EmptyGraphContainer>
);

// -----------------
export const ListItemCard = ({
    onClick, symbol, image, name, showImage,
    latestPrice, latestVolume, currency,
    shortQuoteIsLoading, isLoadingChart,
    changeColor, chartSeries,
    chartOptions, chartData, change,
    id, crypto
}) => {
    const renderChart = () => {
        if(isLoadingChart) return <Loader style={{ padding: 0 }} size={20} />;
        if(chartData.length < 2) return <EmptyGraph />;
        return (
            <ApexChart
                style={{
                    marginBottom: -20,
                    marginLeft: -20
                }}
                options={chartOptions}
                series={chartSeries}
                type='area'
                width={200}
                height={100}
            />
        );
    };

    if (shortQuoteIsLoading || isLoadingChart) {
        return (
            <CustomCard>
                <Loader size={60} style={{margin: '0 auto'}}/>
            </CustomCard>
        )
    }

    return (
        <CustomCard onClick={() => onClick(crypto ? id : symbol, image)}>
            <LeftInfo>
                {showImage && <SymbolImage src={image || DEFAULT_COMPANY_LOGO}/>}
                <Box>
                    <MainInfo>
                        <Symbol>
                            {symbol?.toUpperCase()}
                        </Symbol>
                        <Tooltip title={name}>
                            <SymbolName>
                                {`(${name})`}
                            </SymbolName>
                        </Tooltip>
                    </MainInfo>
                    <Price>
                        {`${countMarketPrice(latestPrice || '').toLocaleString()} ${currency}`}
                    </Price>
                </Box>
            </LeftInfo>
            <StockChange>
                <StockChangeText style={{ color: changeColor }}>
                    {change?.toFixed(2)}%
                </StockChangeText>
                <SolutionText>
                    Vol: {fShortenNumber(latestVolume)}
                </SolutionText>
            </StockChange>
            <ChartContainer>
                {chartData.length < 2 ? <EmptyGraph/> : (
                    <ApexChart
                        style={{
                            marginBottom: -20,
                            marginLeft: -35,
                        }}
                        options={chartOptions}
                        series={chartSeries}
                        type='area'
                        width={LIST_ITEM_CARD_CHART_WIDTH}
                        height={LIST_ITEM_CARD_CHART_HEIGHT}
                    />
                )}
                <TransparentContainer/>
            </ChartContainer>
        </CustomCard>
    );
};

export default ListItemCard;
