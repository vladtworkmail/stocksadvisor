import React from 'react';
import { Card, Grid } from '@mui/material';
import { styled } from '@mui/material/styles';
import Chart from '../Chart';
import CryptoInfoComponent from './components/CryptoInfoComponent';

import TableInfo from './components/TableInfo';

// ---------------
const CryptoSummaryContainer = styled('div')(() => ({
    display: 'flex',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    gap: 20
}));
const CryptoContainer = styled('div')(() => ({
    flexGrow: 1,
    width: 700
}));
const CryptoInfo = styled('div')(() => ({
    flexGrow: 1,
    maxWidth: 300
}));

const CryptoSummary = ({
    symbol, chartData,
    selectedChartFilterValue,
    cryptoDetails, isLoadingDetails,
    handleChartFilterChange,
    info, isLoadingInfo
}) => {
    return (
        <CryptoSummaryContainer>
            <Grid container spacing={2}>
                <Grid item lg={8} md={8} sm={12}>
                    <Chart
                        symbol={symbol}
                        chart={chartData}
                        showChartButtons
                        selectedFilterValue={selectedChartFilterValue}
                        onChartButtonClick={handleChartFilterChange}
                    />
                </Grid>
                <Grid item lg={4} md={4} sm={12}>
                    <CryptoInfoComponent
                        title='Details'
                        info={info}
                        isLoading={isLoadingInfo}
                    />
                </Grid>
            </Grid>
            <TableInfo
                cryptoDetails={cryptoDetails}
                isLoadingDetails={isLoadingDetails}
            />
        </CryptoSummaryContainer>
    );
};

export default CryptoSummary;
