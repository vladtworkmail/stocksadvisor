import React from 'react';
import {
    Card, Table, TableContainer,
    TableBody, TableRow, TableCell
} from '@mui/material';
import Loader from '../../../Loader';
import { rows } from './constants';
import { fShortenNumber } from '../../../../utils';

const TableInfo = ({ cryptoDetails, isLoadingDetails }) => {
    const getRowsData = tableRows => (
        tableRows.map(({ label, value }) => (
            <TableRow key={label} hover sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell sx={{ opacity: 0.7, fontSize: 13 }}>
                    { label }
                </TableCell>
                <TableCell align='right' sx={{ fontWeight: 'bold', fontSize: 13 }}>
                    { cryptoDetails[value] ? `${cryptoDetails[value]?.toLocaleString()} $` : '-' }
                </TableCell>
            </TableRow>
        ))
    );

    return (
        <Card sx={{ width: '100%' }}>
            { isLoadingDetails ? <Loader style={{ p: 5 }} /> : (
                <TableContainer sx={{ display: 'flex' }}>
                    <Table>
                        <TableBody>
                            { getRowsData(rows.slice(0, 3)) }
                        </TableBody>
                    </Table>
                    <Table>
                        <TableBody>
                            { getRowsData(rows.slice(3, 6)) }
                        </TableBody>
                    </Table>
                </TableContainer>
            ) }
        </Card>
    );
}

export default TableInfo;
