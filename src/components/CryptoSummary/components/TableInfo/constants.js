export const rows = [
    {
        label: 'Market Cap',
        value: 'market_cap'
    },
    {
        label: 'Total Volume',
        value: 'total_volume'
    },
    {
        label: 'Fully Diluted Valuation',
        value: 'fullyDilutedValuation'
    },
    {
        label: 'Circulating Supply',
        value: 'circulatingSupply'
    },
    {
        label: 'Total Supply',
        value: 'totalSupply'
    },
    {
        label: 'Max Supply',
        value: 'maxSupply'
    }
];
