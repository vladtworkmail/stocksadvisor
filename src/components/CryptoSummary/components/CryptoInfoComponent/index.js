import React from 'react';
import {
    Box,
    Card,
    Typography
} from '@mui/material';
import { rows } from './constants';
import Loader from '../../../Loader';
import { useStyles } from './styles';

const CryptoInfoComponent = ({ title, info, isLoading }) => {
    const classes = useStyles();
    return (
        <Card className={classes.container}>
            <Typography variant='h5' sx={{ pb: 1 }}>
                { title }
            </Typography>
            { isLoading ? <Loader size={30} style={{ p: 10 }} /> : (
                <Box className={classes.table}>
                    { rows.map(({ label, value }) => {
                        const item = info[value];
                        return (
                            <Box key={label} hover className={classes.tableRow}>
                                <Box className={classes.tableLeftCell}>
                                    { label }
                                </Box>
                                <Box sx={{ display: 'table-cell' }}>
                                    <Box className={classes.tableRightCellContainer}>
                                        { item?.map(({ name, url }) => (
                                            <Box
                                                component='a'
                                                href={url}
                                                target='_blank'
                                                rel='noreferrer'
                                                className={classes.tableRightCellItem}
                                            >
                                                { name }
                                            </Box>
                                        )) }
                                    </Box>
                                </Box>
                            </Box>
                        );
                    }) }
                </Box>
            ) }
        </Card>
    );
};

export default CryptoInfoComponent;
