import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(theme => ({
    container: {
        padding: theme.spacing(2)
    },
    table: {
        display: 'table'
    },
    tableRow: {
        display: 'table-row'
    },
    tableLeftCell: {
        display: 'table-cell',
        opacity: 0.8,
        fontSize: 13,
        paddingRight: theme.spacing(5),
        whiteSpace: 'nowrap'
    },
    tableRightCellContainer: {
        display: 'flex',
        flexGrow: 1,
        flexWrap: 'wrap'
    },
    tableRightCellItem: {
        backgroundColor: theme.palette.grey[200],
        color: 'black',
        fontWeight: 'bold',
        fontSize: 13,
        borderRadius: theme.spacing(0.5),
        textDecoration: 'none',
        padding: theme.spacing(0.5),
        paddingLeft: theme.spacing(1),
        paddingRight: theme.spacing(1),
        margin: theme.spacing(0.3),
        transition: '.2s',
        '&:hover': {
            zIndex: 1,
            boxShadow: '0px 0px 50px 1px rgba(0, 0, 0, 0.5)',
            transform: 'scale(1.05)'
        }
    }
}));
