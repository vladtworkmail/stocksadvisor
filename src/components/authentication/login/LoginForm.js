import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { useFormik, Form, FormikProvider } from 'formik';
import { Icon } from '@iconify/react';
import eyeFill from '@iconify/icons-eva/eye-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
import {
  Link,
  Stack,
  Checkbox,
  TextField,
  IconButton,
  InputAdornment,
  FormControlLabel
} from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { localizeText } from '../../../i18next';
import { useAuth } from '../../../hook/auth';
import { useTheme } from '@mui/styles';
import {useUserAuth} from "../../../hook/useUserAuth";
// ----------------------------------------------------------------------

export default function LoginForm() {
  const navigate = useNavigate();
  const theme = useTheme();
  const [showPassword, setShowPassword] = useState(false);
  const { isAuthorized } = useAuth();
  const { mutate: loginUser, isLoading} = useUserAuth();
  const LoginSchema = Yup.object().shape({
    email: Yup.string().email('Email must be a valid email address').required('Email is required'),
    password: Yup.string().required('Password is required')
  });

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      remember: true
    },
    validationSchema: LoginSchema,
    onSubmit: () => {
      const { getFieldProps } = formik;
      const params = {
        email: getFieldProps('email').value,
        password: getFieldProps('password').value,
        remember: getFieldProps('remember').value
      };
      loginUser(params);
    }
  });

  useEffect(() => {
    if(isAuthorized) {
      navigate('/', { replace: true });
    }
  }, [isAuthorized]);

  const { errors, touched, values, handleSubmit, getFieldProps } = formik;

  const handleShowPassword = () => {
    setShowPassword(show => !show);
  };

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={3}>
          <TextField
            fullWidth
            autoComplete='username'
            type='email'
            label='Email address'
            inputProps={{ style: { color: theme.palette.common.white } }}
            { ...getFieldProps('email') }
            error={Boolean(touched.email && errors.email)}
            helperText={touched.email && errors.email}
          />

          <TextField
            fullWidth
            autoComplete='current-password'
            type={showPassword ? 'text' : 'password'}
            label='Password'
            { ...getFieldProps('password') }
            InputProps={{
              style: { color: theme.palette.common.white },
              endAdornment: (
                <InputAdornment position='end'>
                  <IconButton onClick={handleShowPassword} edge='end'>
                    <Icon icon={showPassword ? eyeFill : eyeOffFill} />
                  </IconButton>
                </InputAdornment>
              )
            }}
            error={Boolean(touched.password && errors.password)}
            helperText={touched.password && errors.password}
          />
        </Stack>

        <Stack direction='row' alignItems='center' justifyContent='space-between' sx={{ my: 2 }}>
          <FormControlLabel
            sx={{ color: theme.palette.common.white }}
            control={<Checkbox { ...getFieldProps('remember') } checked={values.remember} />}
            label={localizeText('login.rememberMe')}
          />

          <Link component={RouterLink} variant='subtitle2' to='#'>
            { localizeText('login.forgotPassword') }
          </Link>
        </Stack>

        <LoadingButton
          fullWidth
          size='large'
          type='submit'
          variant='contained'
          loading={isLoading}
        >
          { localizeText('login.login') }
        </LoadingButton>
      </Form>
    </FormikProvider>
  );
};
