import { Stack, Divider, Typography } from '@mui/material';
import { toast } from 'react-toastify';
import { LoadingButton } from '@mui/lab';
import { GoogleLogin } from 'react-google-login';
import useRegisterWithGoogle from "../../hook/useRegisterWithGoogle";
import useLoginWithGoogle from "../../hook/useLoginWithGoogle";
import { Icon } from '@iconify/react';
import googleFill from '@iconify/icons-eva/google-fill';

export default function AuthSocial({action, disabled}) {
  const {registerWithGoogle, registerWithGoogleIsLoading} = useRegisterWithGoogle();
  const {loginWithGoogle, loginWithGoogleIsLoading} = useLoginWithGoogle();

  const googleSuccessHandler = async ({tokenId}) => {
      if (action === 'register') {
          registerWithGoogle(tokenId);
      } else {
          loginWithGoogle(tokenId);
      }
  };
  const googleFailureHandler = error => {
    toast.error('Google Auth error. Please try again later');
    console.error(error);
  };

  const isLoading = registerWithGoogleIsLoading || loginWithGoogleIsLoading;
  return (
    <>
      <Stack direction='row' spacing={2}>
        <GoogleLogin
            disabled={disabled}
          clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
          onSuccess={googleSuccessHandler}
          onFailure={googleFailureHandler}
          cookiePolicy='single_host_origin'
          render={props => (
                        <LoadingButton
                          fullWidth
                          size='large'
                          color='inherit'
                          variant='outlined'
                          onClick={props.onClick}
                          disabled={props.disabled}
                          loading={isLoading}
                        >
                          { isLoading ? null : <Icon icon={googleFill} color='#DF3E30' height={24} /> }
                        </LoadingButton>
                      )}
        />
      </Stack>

      <Divider sx={{ my: 3 }}>
        <Typography variant='body2' sx={{ color: 'text.secondary' }}>
          OR
        </Typography>
      </Divider>
    </>
  );
};
