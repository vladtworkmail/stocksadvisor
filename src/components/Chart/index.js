import { Box, Stack, Card, CardHeader, Button } from '@mui/material';
import PropTypes from 'prop-types';
import ReactApexChart from 'react-apexcharts';
import moment from 'moment';
import { merge } from 'lodash';
import { BaseOptionChart } from '../charts';
import { styled } from '@mui/material/styles';
import { buttons } from './constants';
import {CustomCard} from "../StockFinancials/components/StockOverview/components/CompanyInfo";
// ----------------------------------------------------------------------

export const CardHeaderContainer = styled('div')(() => ({
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    padding: '10px 30px 10px 30px',
}));

const Chart = ({
    chart = [], title = '',
    symbol, showChartButtons,
    styles, showHeader, selectedFilterValue,
    onChartButtonClick, customButtonsProps
}) => {
    const chartData = [{
        name: symbol,
        type: 'area',
        data: chart
    }];
    const chartOptions = merge(BaseOptionChart(), {
        xaxis: {
            // categories: dates,
            labels: { show: false }
        },
        options: {
            chart: {
                type: 'area',
                height: 350,
                stacked: true,
                events: {
                    selection: function (chart, e) {
                        console.log(new Date(e.xaxis.min))
                    }
                },
            },
            colors: ['#008FFB', '#00E396', '#CED4DC'],
            dataLabels: { enabled: false },
            stroke: { curve: 'smooth' },
            fill: {
                type: 'gradient',
                gradient: {
                    opacityFrom: 0.6,
                    opacityTo: 0.8,
                }
            },
            legend: {
                position: 'top',
                horizontalAlign: 'left'
            }
        },
        yaxis: { labels: { formatter: val => val.toFixed(2) } },
        tooltip: {
            y: {
                formatter: y => {
                    if (typeof y !== 'undefined') {
                        return `${y.toFixed(2)} $`;
                    }
                    return y;
                }
            },
            x: {
                formatter: x => {
                    if (typeof x !== 'undefined') {
                        return moment(x).format('ll');
                    }
                }
            }
        }
    });


    const handleClick = ({ target: { value } }) => onChartButtonClick(value);

    return (
        <CustomCard style={{...styles}}>
            <CardHeaderContainer>
                { showHeader && <CardHeader title={title} /> }
                { showChartButtons && (
                    <Stack spacing={1} direction='row'  sx={{ width: '100%', overflow: 'hidden' }}>
                        { (customButtonsProps || buttons).map(({ label, value }) => (
                            <Button
                                variant={selectedFilterValue === value ? 'contained' : 'outlined'}
                                key={label}
                                size='small'
                                onClick={handleClick}
                                value={value}
                            >
                                { label }
                            </Button>
                        )) }
                    </Stack>
                ) }
            </CardHeaderContainer>
            <Box dir='ltr'>
                <ReactApexChart type='area' series={chartData} options={chartOptions} height={193} />
            </Box>
        </CustomCard>
    );
};

Chart.propTypes = {
    showChartButtons: PropTypes.bool,
    showHeader: PropTypes.bool,
    onChartButtonClick: PropTypes.func
};

Chart.defaultProps = {
    showChartButtons: false,
    showHeader: false,
    onChartButtonClick: () => {}
};

export default Chart;
