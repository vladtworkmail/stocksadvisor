export const buttons = [
    {
        label: '5D',
        value: '5dm'
    },
    {
        label: '1M',
        value: '1m'
    },
    {
        label: '3M',
        value: '2q'
    },
    {
        label: '6M',
        value: '6m'
    },
    {
        label: '1Y',
        value: '1y'
    },
    {
        label: '5Y',
        value: '5y'
    },
    {
        label: 'MAX',
        value: 'max'
    },
]
