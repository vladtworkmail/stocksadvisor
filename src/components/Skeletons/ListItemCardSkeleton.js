import React from 'react';
import { Grid, Skeleton } from '@mui/material';
import { useTheme } from '@mui/styles';
import { alpha } from '@mui/material/styles';

const ListItemCardSkeleton = ({
    width = '100%', height = 175,
    skeletonsCount = 4
}) => {
    const theme = useTheme();
    const arr = new Array(skeletonsCount).fill(0);
    return (
        <Grid container spacing={2}>
            { arr.map((_, i) => (
                <Grid key={i} item xs={12} sm={6} md={4} lg={3}>
                    <Skeleton
                        animation='wave'
                        variant='rectangular'
                        width={width}
                        height={height}
                        sx={{ backgroundColor: alpha(theme.palette.grey[200], 0.25), borderRadius: theme.spacing(1) }}
                    />
                </Grid>
            )) }
        </Grid>
    );
};

export default ListItemCardSkeleton;
