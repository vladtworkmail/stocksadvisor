import { styled } from '@mui/material/styles';
import { Card, CardHeader } from '@mui/material';
import DonutChart from 'react-donut-chart';

// ----------------------------------------------------------------------

const CHART_HEIGHT = 220;
const CHART_WIDTH = 320;

const ChartWrapperStyle = styled('div')(({ theme }) => ({
  width: CHART_WIDTH,
  margin: '0 auto',
  marginTop: theme.spacing(3),
}));

// ----------------------------------------------------------------------


export default function PortfolioEvaluation({data = []}) {
  const colors = [
    '#0E53A7', '#FF6700', '#274E7D',
    '#00A287', '#04346C',
    '#34D0B6', '#4284D3',
    '#FF8D40', '#6899D3',
    '#FFFF40', '#9F3ED5',
    '#E6399B', '#B9F73E',
    '#DDFB3F', '#CD35D3',
    '#FFD540', '#5B4CD8',
    '#FF8940', '#34D2AF'
  ];
  return (
    <Card>
      <CardHeader title="Diary Evaluation" />
      <ChartWrapperStyle dir="ltr">
        {
          data.length &&
          <DonutChart
              data={data}
              colors={colors}
              height={CHART_HEIGHT}
              width={CHART_WIDTH}
          />
        }
      </ChartWrapperStyle>
    </Card>
  );
}
