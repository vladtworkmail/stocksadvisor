import React from "react";
import {Box} from "@mui/material";
import {styled} from "@mui/material/styles";

const Container = styled(Box)(({ theme }) => ({
    color: theme.palette.common.white,
    maxWidth: 650,
    paddingBottom: '4.5rem'
}));

const TitleText = styled('p')(({ theme }) => ({
    fontSize: '1.88rem',
    letterSpacing: 1,
    textDecoration: 'underline',
    lineHeight: '150%',
    paddingBottom: 10,
    fontWeight: 'bold'
}));

const Description = styled('p')(({ theme }) => ({
    fontSize: '1.1rem',
    fontWeight: 100,
    letterSpacing: 0,
    lineHeight: '150%',
}));

const MainInfoText = ({title, description, styleContainer}) => (
    <Container style={styleContainer}>
        <TitleText>{title.toUpperCase()}</TitleText>
        <Description>{description}</Description>
    </Container>
);

export default MainInfoText;
