import { styled } from '@mui/material/styles';

const Container = styled('p')(() => ({ textAlign: 'center' }));

const LoadMoreEndMessage = () => (
    <Container>
        <b>Yay! You have seen it all</b>
    </Container>
);

export default LoadMoreEndMessage;