import React from 'react';
import { Typography } from '@mui/material';
import { useTheme } from '@mui/styles';

const PageTitle = ({ title }) => {
    const theme = useTheme();
    return (
        <Typography
            variant='h4'
            color={theme.palette.common.white}
            sx={{ opacity: 0.8, marginTop: 2, marginBottom: 1 }}
        >
            { title }
        </Typography>
    );
};

export default PageTitle;
