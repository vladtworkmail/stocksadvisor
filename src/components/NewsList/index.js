import React from 'react';
import { Grid, Box } from '@mui/material';
import Loader from '../Loader';
import PageTitle from '../PageTitle';
import NewsCard from "../NewsCard";
import NewsModal from "../../modals/NewsModal";
import {nanoid} from "nanoid";

const NewsList = ({
    title,
    news,
    isLoading,
    onNewsCardClick,
    isNewsModalOpen,
    onNewsModalClose,
    newsContent
}) => (
    isLoading ? <Loader/> : (
        <>
            <PageTitle title={title}/>
            <Box style={{paddingBottom: 30}}/>
            <Grid container spacing={10}>
                {news.map(item => (
                    <Grid
                        key={nanoid()}
                        item
                        xs={12}
                        sm={6}
                        md={4}
                        lg={3}
                    >
                        <NewsCard
                            data={item}
                            onNewsCardClick={onNewsCardClick}
                        />
                    </Grid>
                ))}
            </Grid>
            <NewsModal
                open={isNewsModalOpen}
                onClose={onNewsModalClose}
                content={newsContent}
            />
        </>
    )
);

export default NewsList;
