import DatePicker from 'react-date-picker';
import './styles.css';

const CustomDatePicker = ({value, onChange}) => {
    return (
        <DatePicker
            calendarClassName="calendar"
            value={value}
            onChange={onChange}
            format="dd/MM/yyyy"
            maxDate={new Date()}
        />
    );
};

export default CustomDatePicker;
