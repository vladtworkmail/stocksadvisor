import { useMemo } from 'react';
import { Dialog, DialogContent, DialogContentText, Backdrop, IconButton, DialogTitle } from '@mui/material';
import { Close as CloseIcon } from '@mui/icons-material';
import Loader from '../Loader';
import { useStyles } from './styles';

const ScrollableDialog = ({ open, closeHandler, isLoading, children }) => {
    const classes = useStyles();
    const isLoaded = useMemo(() => !(isLoading || !children), [isLoading, children]);
    return (
        <Dialog
            open={open}
            scroll='paper'
            onClose={closeHandler}
            BackdropComponent={Backdrop}
            BackdropProps={{ className: classes.backdrop }}
            maxWidth='md'
            fullWidth={isLoaded}
        >
            { !isLoaded ? null : (
                <DialogTitle sx={{ p: 0 }}>
                    <IconButton
                        className={classes.closeButton}
                        size='large'
                        onClick={closeHandler}
                        sx={{ backdropFilter: 'blur(1px)', WebkitBackdropFilter: 'blur(1px)' }}
                    >
                        <CloseIcon color='inherit' sx={{ color: 'white' }} />
                    </IconButton>
                    { children.Header }
                </DialogTitle>
            ) }
            <DialogContent>
                { !isLoaded ? <Loader style={{ padding: 0 }} /> : (
                    <DialogContentText tabIndex={-1}>
                        { children.Body }
                    </DialogContentText>
                ) }
            </DialogContent>
        </Dialog>
    );
};

export default ScrollableDialog;