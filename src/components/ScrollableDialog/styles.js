import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
    backdrop: {
        '&.MuiBackdrop-root': {
            backdropFilter: 'blur(2px)',
            WebkitBackdropFilter: 'blur(2px)'
        }
    },
    closeButton: {
        position: 'absolute',
        top: 0,
        right: 0,
        zIndex: 2
    }
});
