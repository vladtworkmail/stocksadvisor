import React from 'react';
import { Box, Card, CardHeader } from '@mui/material';
import Stack from '@mui/material/Stack';
import { buttons } from '../Chart/constants';
import Button from '@mui/material/Button';
import ReactApexChart from 'react-apexcharts';
import { CardHeaderContainer } from '../Chart';
import { merge } from 'lodash';
import { BaseOptionChart } from '../charts';
import Loader from '../Loader';

const DividendHistoryChart = ({
    title, companies, data,
    isLoading, onLoadMore,
    disableLoadMore,
}) => {
    if (isLoading || !data.length) {
        return (
            <Card style={{padding: 10}}>
                <Loader />
            </Card>
        );
    }

    const chartData = [
        { name: 'Value($)', data }
    ];

    const options = merge(BaseOptionChart(),{
        chart: {
            type: 'bar',
            id: 'apexchart-example'
        },
        title: {
            text: title,
            align: 'center',
            floating: true,
            style: {
                fontSize:  '20px',
                fontWeight:  'bold',
                color:  '#263238'
            },
        },
        dataLabels: { enabled: false},
        plotOptions: {
            bar: {
                horizontal: true,
                dataLabels: {
                    position: 'top'
                }
            }
        },
        xaxis: {
            categories: companies
        }
    });

    return (
        <Card style={{padding: 10}}>
            <CardHeaderContainer>
                <Button
                    variant="outlined"
                    size="small"
                    disabled={disableLoadMore || isLoading}
                    onClick={onLoadMore}
                >
                    Load More
                </Button>
            </CardHeaderContainer>
            <Box dir="ltr">
                <ReactApexChart series={chartData} options={options} height={380} type="bar"/>
            </Box>
        </Card>
    );
};

export default DividendHistoryChart;
