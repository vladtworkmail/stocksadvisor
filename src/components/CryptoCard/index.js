import { useMemo, memo } from 'react';
import { useTheme } from '@mui/styles';
import { getChartColor } from '../../utils';
import moment from 'moment';
import useFetchCryptoChartDetails from '../../hook/useFetchCryptoChartDetails';
import ListItemCard from '../ListItemCard';

const CryptoCard = ({ crypto, handleClick }) => {
    const theme = useTheme();
    const {
        id,
        name,
        symbol,
        regularMarketChangePercent,
        regularMarketPrice,
        financialCurrency,
        currency,
        regularMarketVolume
    } = crypto;
    let { image } = crypto;

    // @TODO: Remove this logics when it's done on backend, replace from & to parameters to range.
    const range = '1mo';
    let from, to;
    const date = moment();
    switch(range) {
        case '1mo':
            from = Date.parse(date.format());
            to = Date.parse(date.clone().subtract(1, 'M').format());
    }
    const { chartData, isLoadingChart } = useFetchCryptoChartDetails(id, from, to);

    const isLoading = useMemo(() => !chartData?.length, [chartData]);
    
    const chartColor = getChartColor(chartData, theme);
    
    return (
       <ListItemCard
            isLoadingChart={isLoading}
            onClick={handleClick}
            image={image}
            showImage
            regularMarketChangePercent={regularMarketChangePercent}
            financialCurrency={financialCurrency}
            chartData={chartData}
            regularMarketPrice={regularMarketPrice}
            regularMarketVolume={regularMarketVolume}
            name={name}
            chartColor={chartColor}
            currency={currency}
            symbol={id}
       />
    );
};

export default memo(CryptoCard);
