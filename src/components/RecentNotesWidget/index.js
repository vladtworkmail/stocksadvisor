import React from "react";
import {Card, CardContent, CardHeader} from "@mui/material";
import {alpha, styled} from "@mui/material/styles";
import Grid from "@mui/material/Grid";
import WidgetTitle from "../WidgetTitle/WidgetTitle";
import WidgetItem from "../WidgetItem/WidgetItem";
////////
const CustomCard = styled(Card)(({ theme }) => ({
    backgroundColor: alpha(theme.palette.widgetColor, 0.88),
    boxShadow: 'none'
}));
const CustomCardContent = styled(CardContent)(({ theme }) => ({
    display: 'flex',
    gap: 20,
    overflow: 'auto'
}));

const RecentNotesWidget = () => {

    return (
      <CustomCard>
          <CardHeader title={<WidgetTitle title="Recent Notes"/>} />
          <CustomCardContent>
              <WidgetItem
                  title="Some Note title"
                  content="dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd addsdmaskdalkdlad; sdsda asdasd asd asd asd ad"
                  date="10/11/12"
              /> <WidgetItem
                  title="Some Note title"
                  content="dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd addsdmaskdalkdlad; sdsda asdasd asd asd asd ad"
                  date="10/11/12"
              />
              <WidgetItem
                  title="Some Note title"
                  content="dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd addsdmaskdalkdlad; sdsda asdasd asd asd asd ad"
                  date="10/11/12"
              /><WidgetItem
                  title="Some Note title"
                  content="dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd addsdmaskdalkdlad; sdsda asdasd asd asd asd ad"
                  date="10/11/12"
              /><WidgetItem
                  title="Some Note title"
                  content="dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd addsdmaskdalkdlad; sdsda asdasd asd asd asd ad"
                  date="10/11/12"
              /><WidgetItem
                  title="Some Note title"
                  content="dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd ad dsdmaskdalkdlad; sdsda asdasd asd asd asd addsdmaskdalkdlad; sdsda asdasd asd asd asd ad"
                  date="10/11/12"
              />
          </CustomCardContent>
      </CustomCard>
    );
};

export default RecentNotesWidget;
