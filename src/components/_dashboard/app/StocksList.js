import React from 'react';
import { Container, Box, Grid } from '@mui/material';
import Page from '../../../components/Page';
import PageTitle from '../../PageTitle';
import LoadMoreButton from '../../../components/LoadMoreButton';
import LoadMoreEndMessage from '../../LoadMoreEndMessage';
import ListItemCard from '../../ListItemCard';
import StocksCard from 'src/components/StocksCard';
import Searchbar from '../../../layouts/dashboard/Searchbar';
import ListItemCardSkeleton from '../../Skeletons/ListItemCardSkeleton';
import {styled} from "@mui/material/styles";

const ContentContainer = styled(Box)(() => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: '116px',
    marginBottom: 10
}));

const StocksList = ({
    isLoading,
    stocksList,
    loadMore,
    onStockCardClick,
    onSearch,
    searchValue,
    hasSearchResults,
    onSearchClear,
    isLoadingSearch,
    searchList
}) => {
    const showList = () => (
        <Grid container spacing={6}>
            { stocksList?.map(item => (
                <Grid key={item.symbol} item xs={12} sm={6} md={4} lg={4} xl={3}>
                    <StocksCard
                        isLoadingChart={false}
                        onClick={onStockCardClick}
                        image={item.logo}
                        showImage
                        name={item.companyName}
                        symbol={item.symbol}
                    />
                </Grid>
            )) }
            <Grid item lg={12} md={12} sm={12} xs={12} sx={{ display: 'flex', justifyContent: 'center' }}>
                { !stocksList.length ? <LoadMoreEndMessage/> : (
                    <LoadMoreButton
                        title='Load More...'
                        loadMore={loadMore}
                        isLoading={isLoading}
                    />
                ) }
            </Grid>
        </Grid>
    );

    return (
        <Page title='Stocks'>
            <Container maxWidth='xl'>
                <ContentContainer>
                    <PageTitle title='Stocks' />
                    <Searchbar
                        searchValue={searchValue}
                        list={searchList}
                        isLoading={isLoadingSearch}
                        isSearchResults={hasSearchResults}
                        handleSearch={onSearch}
                        redirectionLink='details'
                        style={{mr: 2}}
                        handleClear={onSearchClear}
                    />
                </ContentContainer>
                { isLoading && !stocksList.length ? <ListItemCardSkeleton /> : showList() }
            </Container>
        </Page>
    );
}

export default StocksList;
