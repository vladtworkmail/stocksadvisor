import React from 'react';
import { Box, Container, Grid } from '@mui/material';
import Page from '../../../components/Page';
import PageTitle from '../../PageTitle';
import LoadMoreButton from 'src/components/LoadMoreButton';
import LoadMoreEndMessage from '../../LoadMoreEndMessage';
import Searchbar from '../../../layouts/dashboard/Searchbar';
import ListItemCardSkeleton from '../../Skeletons/ListItemCardSkeleton';
import { CryptoCard } from './';

const CryptoList = ({
    isLoading,
    list,
    loadMore,
    onItemCardClick,
    onSearch,
    onSearchClear,
    hasSearchResults,
    searchValue,
    searchList,
    isLoadingSearch
}) => {
    const showList = () => (
        <Grid container spacing={2}>
            { list.map(item => (
                <Grid key={item.symbol} item xs={12} sm={6} md={4} lg={3}>
                    <CryptoCard crypto={item} handleClick={onItemCardClick} />
                </Grid>
            )) }
            <Grid item lg={12} md={12} sm={48} xs={48} sx={{ display: 'flex', justifyContent: 'center' }}>
                { !list.length ? <LoadMoreEndMessage /> : (
                    <LoadMoreButton
                        title='Load More...'
                        loadMore={loadMore}
                        isLoading={isLoading}
                    />
                ) }
            </Grid>
        </Grid>
    );

    return (
        <Page title='Cryptocurrencies'>
            <Container maxWidth='xl' sx={{ paddingTop: '116px' }}>
                <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                    <PageTitle title='Cryptocurrencies' />
                    <Searchbar
                        searchValue={searchValue}
                        list={searchList}
                        isLoading={isLoadingSearch}
                        isSearchResults={hasSearchResults}
                        handleSearch={onSearch}
                        handleClear={onSearchClear}
                        redirectionLink='details'
                        style={{ mr: 2 }}
                    />
                </Box>
                { isLoading && !list.length ? <ListItemCardSkeleton /> : showList() }
            </Container>
        </Page>
    );
}

export default CryptoList;
