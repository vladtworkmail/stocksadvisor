import React from 'react';
import { Box, Tabs, Tab } from '@mui/material';
import TabPanel from '../tabPanels';
import StockIncomes from './components/StockIncomes';
import StockBalance from './components/StockBalance';
import StockCashFlow from './components/StockCashFlow';
import StockOverview from './components/StockOverview';
import {useLocation} from "react-router-dom";
import queryString from "query-string";


const StockFinancials = () => {
    const [value, setValue] = React.useState(0);
    const location = useLocation();
    const {
        query: {
            symbol
        }
    } = queryString.parseUrl(location.search);

    const handleChange = (event, newValue) => setValue(newValue);

    return (
        <Box sx={{ display: 'flex' }}>
            <Tabs
                orientation='vertical'
                variant='scrollable'
                value={value}
                onChange={handleChange}
                sx={{ borderRight: 1, borderColor: 'divider', minWidth: 110 }}
            >
                <Tab label='Incomes' />
                <Tab label='Balance' />
                <Tab label='Cash Flow' />
                <Tab label='Overview' />
            </Tabs>

            <TabPanel value={value} index={0} style={{ pl: 3, pt: 0 }}>
               <StockIncomes
                   symbol={symbol}
               />
            </TabPanel>
            <TabPanel value={value} index={1} style={{ pl: 3, pt: 0 }}>
               <StockBalance
                   symbol={symbol}
               />
            </TabPanel>
            <TabPanel value={value} index={2} style={{ pl: 3, pt: 0 }}>
                <StockCashFlow
                    symbol={symbol}
                />
            </TabPanel>
            <TabPanel value={value} index={3} style={{ pl: 3, pt: 0 }}>
                <StockOverview
                    symbol={symbol}
                />
            </TabPanel>
        </Box>
    );
};

export default StockFinancials;
