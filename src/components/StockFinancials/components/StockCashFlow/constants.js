export const rows = [
    {
        label: 'Capital Expenditures',
        value: 'capitalExpenditures'
    },
    {
        label: 'Changes InCash',
        value: 'cashChange'
    },
    {
        label: 'Changes in Account Receivables',
        value: 'changesInReceivables'
    },
    {
        label: 'Changes To Inventory',
        value: 'changesInInventories'
    },
    {
        label: 'Depreciation & Amortization',
        value: 'depreciation'
    },
    {
        label: 'Net Income',
        value: 'netIncome'
    },
    {
        label: 'Net Borrowings',
        value: 'netBorrowings'
    },
    {
        label: 'Cash Flow',
        value: 'cashFlow'
    },
    {
        label: 'Total Investing Cash Flow',
        value: 'totalInvestingCashFlows'
    }
];
