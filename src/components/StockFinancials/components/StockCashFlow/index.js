import React from "react";
import {Card} from "@mui/material";
import Loader from "../../../Loader";
import {rows} from './constants';
import FinancialTable from "../FinancialTable";
import useFetchStockCashFlow from "../../../../hook/useFetchStockCashFlow";
import {CustomCard} from "../StockOverview/components/CompanyInfo";

const StockCashFlow = ({symbol}) => {
    const {cashFlow, isLoadingCashFlow} = useFetchStockCashFlow(symbol);
    if (isLoadingCashFlow) {
        return <Loader/>
    }

    if (!cashFlow.length) {
        return (<span>No Data...</span>)
    }
    return (
        <CustomCard sx={{p: 2, overflow: 'hidden'}}>
            <FinancialTable
                data={cashFlow}
                rows={rows}
            />
        </CustomCard>
    );
};

export default StockCashFlow;
