import React from "react";
import {Card} from "@mui/material";
import Loader from "../../../Loader";
import {rows} from './constants';
import FinancialTable from "../FinancialTable";
import useFetchStockBalance from "../../../../hook/useFetchStockBalance";
import {CustomCard} from "../StockOverview/components/CompanyInfo";

const StockBalance = ({symbol}) => {

    const {balance, isLoadingBalance} = useFetchStockBalance(symbol);
    if (isLoadingBalance) {
        return <Loader/>
    }

    if (!balance.length) {
        return (<span>No Data...</span>)
    }
    return (
        <CustomCard sx={{p: 2, overflow: 'hidden'}}>
            <FinancialTable
                data={balance}
                rows={rows}
            />
        </CustomCard>
    );
};

export default StockBalance;
