export const rows = [
    {
        label: 'Cash',
        value: 'currentCash'
    },
    {
        label: 'Common Stock',
        value: 'commonStock'
    },
    {
        label: 'Inventory',
        value: 'inventory'
    },
    {
        label: 'Long Term Debt',
        value: 'longTermDebt'
    },
    {
        label: 'Long Term Investments',
        value: 'longTermInvestments'
    },
    {
        label: 'Net Receivables',
        value: 'receivables'
    },
    {
        label: 'Net Tangible Assets',
        value: 'netTangibleAssets'
    },
    {
        label: 'Retained Earnings',
        value: 'retainedEarnings'
    },
    {
        label: 'Goodwill',
        value: 'goodwill'
    },
    {
        label: 'Short Term Investments',
        value: 'shortTermInvestments'
    },
    {
        label: 'Total Assets',
        value: 'totalAssets'
    },
    {
        label: 'Other Assets',
        value: 'otherAssets'
    },
    {
        label: 'Other Current Assets',
        value: 'otherCurrentAssets'
    },
    {
        label: 'Total Current Liabilities',
        value: 'totalCurrentLiabilities'
    },
    {
        label: 'Total Liabilities ',
        value: 'totalLiabilities'
    },
    {
        label: 'Total Stockholder Equity',
        value: 'shareholderEquity'
    }
];
