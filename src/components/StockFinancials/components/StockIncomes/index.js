import React from "react";
import {Card} from "@mui/material";
import Loader from "../../../Loader";
import {rows} from './constants';
import FinancialTable from "../FinancialTable";
import useFetchStockIncomes from "../../../../hook/useFetchStockIncomes";
import {CustomCard} from "../StockOverview/components/CompanyInfo";

const StockIncomes = ({symbol}) => {
    const {incomes, isLoadingIncomes} = useFetchStockIncomes(symbol);
    if (isLoadingIncomes) {
        return <Loader/>
    }

    if (!incomes.length) {
        return (<span>No Data...</span>)
    }
    return (
        <CustomCard sx={{p: 2, overflow: 'hidden'}}>
            <FinancialTable
                data={incomes}
                rows={rows}
            />
        </CustomCard>
    );
};

export default StockIncomes;
