export const rows = [
    {
        label: 'Cost Of Revenue',
        value: 'costOfRevenue'
    },
    {
        label: 'EBIT',
        value: 'ebit'
    },
    {
        label: 'Gross Profit',
        value: 'grossProfit'
    },
    {
        label: 'Income Before Tax',
        value: 'pretaxIncome'
    },
    {
        label: 'Income Tax Expense',
        value: 'incomeTax'
    },
    {
        label: 'Interest Expense',
        value: 'interestIncome'
    },
    {
        label: 'Net Income',
        value: 'netIncome'
    },
    {
        label: 'Net Income Basis',
        value: 'netIncomeBasic'
    },
    {
        label: 'Operating Income',
        value: 'operatingIncome'
    },
    {
        label: 'Research Development',
        value: 'researchAndDevelopment'
    },
    {
        label: 'Research Development',
        value: 'researchAndDevelopment'
    },
    {
        label: 'Selling General Administrative',
        value: 'sellingGeneralAndAdmin'
    },
    {
        label: 'Total Operation Expenses',
        value: 'operatingExpense'
    },
    {
        label: 'Total Other Income Expense Net',
        value: 'otherIncomeExpenseNet'
    },
    {
        label: 'Total Revenue',
        value: 'totalRevenue'
    },

];
