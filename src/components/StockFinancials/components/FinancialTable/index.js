import React from "react";
import Table from "@mui/material/Table";
import TableRow from "@mui/material/TableRow";
import TableBody from "@mui/material/TableBody";
import {nanoid} from "nanoid";
import TableContainer from "@mui/material/TableContainer";
import {styled} from "@mui/styles";
import {CustomTableCell} from "../StockOverview/components/Valuation/valuationTable";
import moment from "moment";

export const CustomTableRow = styled(TableRow)(({theme}) => ({
    fontSize: 17,
}));

const FinancialTable = ({data, rows}) => (
    <TableContainer sx={{ maxHeight: 510}}>
        <Table sx={{minWidth: 650}} stickyHeader>
                <CustomTableRow>
                    <CustomTableCell>Fiscal Date</CustomTableCell>
                    {
                        data.map(elem => (
                            <CustomTableCell align="center" key={elem?.fiscalDate}>
                                {moment(elem?.fiscalDate).format('DD/MM/YYYY')}
                            </CustomTableCell>
                        ))
                    }
                </CustomTableRow>
            <TableBody>
                {
                    rows.map(row => (
                        <TableRow hover key={nanoid()}>
                            <CustomTableCell>{row.label}</CustomTableCell>
                            {
                                data.map(elem => (
                                    <CustomTableCell key={nanoid()} align="center">
                                        {
                                            elem[row.value] || 0
                                        }
                                    </CustomTableCell>
                                ))
                            }
                        </TableRow>
                    ))
                }
            </TableBody>
        </Table>
    </TableContainer>
);

export default FinancialTable;
