export const leftRows = [
    {
        label: 'Market Cap',
        value: 'marketCap'
    },
    {
        label: 'P/E Ratio',
        value: 'trailingPe'
    },
    {
        label: 'PEG Ratio',
        value: 'pegRatio'
    },
    {
        label: 'Price/Sales',
        value: 'priceSales'
    },
];

export const rightRows = [
    {
        label: 'Price/Book',
        value: 'priceSales'
    },
    {
        label: 'Price/Tangible Book',
        value: 'priceTangb'
    },
    {
        label: 'Price/Cash Flow',
        value: 'priceCf'
    },
    {
        label: 'Enterprise Value/Sales',
        value: 'evSales'
    },
];
