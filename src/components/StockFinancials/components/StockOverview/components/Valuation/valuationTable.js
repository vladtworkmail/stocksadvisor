import React from "react";
import {leftRows, rightRows} from './constants';
import {
    Card,
    Grid,
    TableContainer,
    TableBody,
    Table,
    TableCell,
    CardHeader,
    TableRow,
    CardContent
} from "@mui/material";
import {fShortenNumber} from "../../../../../../utils";
import {styled} from "@mui/styles";

export const CustomTableCell = styled(TableCell)(({theme}) => ({
    fontSize: 13,
    opacity: 0.75,
    color: theme.palette.common.white,
}));

export const CustomTableCellLink = styled('a')(({theme}) => ({
    color: 'white'
}));

export const getTable = (rows, data, useShortNumbers) => (
    <TableContainer>
        <Table aria-label="simple table">
            <TableBody>
                {
                    rows.map(({label, value}) => (
                        <TableRow hover>
                            <CustomTableCell>{label}</CustomTableCell>
                            {
                                useShortNumbers ?
                                    <CustomTableCell>{data[value] ? fShortenNumber(data[value]) : '-'}</CustomTableCell>
                                    :
                                    <CustomTableCell>
                                        {
                                            data[value] ?
                                                value === 'website' ?
                                                    <CustomTableCellLink
                                                        href={data[value]}
                                                        target="_blank"
                                                        rel="noreferrer"
                                                    >
                                                        {data[value]}
                                                    </CustomTableCellLink>
                                                    :
                                                data[value]
                                                :
                                                '-'
                                        }
                                    </CustomTableCell>
                            }
                        </TableRow>
                    ))
                }
            </TableBody>
        </Table>
    </TableContainer>
);

const ValuationTable = ({data}) => {
    return (
        <Card sx={{minWidth: 500}}>
            <CardHeader title="Valuation" sx={{paddingTop: '10px', opacity: 0.8}}/>
            <CardContent>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={6} lg={6} >
                    {getTable(leftRows, data, true)}
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                    {getTable(rightRows, data, true)}
                </Grid>
            </Grid>
            </CardContent>
        </Card>
    );
}

export default ValuationTable;
