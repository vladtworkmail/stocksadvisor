import React from "react";
import {Card, CardContent, CardHeader, Grid, Typography} from "@mui/material";
import {leftRows, rightRows} from "./constants";
import {getTable} from "../Valuation/valuationTable";
import {styled} from "@mui/material/styles";

export const CustomCard = styled(Card)(({theme}) => ({
    backgroundColor: theme.palette.widgetColor,
    color: theme.palette.common.white,
    boxShadow: 'none'
}));

const CompanyInfo = ({data}) => (
    <CustomCard sx={{minWidth: 300}}>
        <CardHeader title="Company Profile" sx={{paddingTop: '10px', opacity: 0.8}}/>
        <CardContent>
            <Typography variant="body2" color="text.secondary">
                {data.description}
            </Typography>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={6} lg={6} >
                    {getTable(leftRows, data)}
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                    {getTable(rightRows, data)}
                </Grid>
            </Grid>
        </CardContent>
    </CustomCard>
);

export default CompanyInfo;
