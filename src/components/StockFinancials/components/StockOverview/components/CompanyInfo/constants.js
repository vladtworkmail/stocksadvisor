export const leftRows = [
    {
        label: 'Sector',
        value: 'sector'
    },
    {
        label: 'Industry',
        value: 'industry'
    },
    {
        label: 'Employees',
        value: 'employees'
    },
    {
        label: 'Country',
        value: 'country'
    },
];

export const rightRows = [
    {
        label: 'City',
        value: 'city'
    },
    {
        label: 'Phone Number',
        value: 'phone'
    },
    {
        label: 'Website',
        value: 'website'
    },
    {
        label: 'Exchange',
        value: 'exchange'
    },
];
