import React  from 'react';
import {
    Grid, Box
} from '@mui/material';
import Loader from '../../../Loader';
import CompanyInfo from './components/CompanyInfo';
import useFetchStockOverview from "../../../../hook/useFetchStockOverview";


const StockOverview = ({symbol}) => {
    const { overview, isLoadingOverview } = useFetchStockOverview(symbol);
    if(isLoadingOverview) return <Loader />;

    return (
        <Box sx={{maxWidth: 1200}}>
        <Grid container spacing={3}>
            <Grid item xs={12} sm={12} md={12} lg={12} >
                <CompanyInfo data={overview} />
            </Grid>
        </Grid>
        </Box>
    );
};

export default StockOverview;
