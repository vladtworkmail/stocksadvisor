import { LoadingButton } from '@mui/lab';

const LoadMoreButton = ({ title, loadMore, isLoading }) => (
    <LoadingButton
        variant='contained'
        sx={{ minWidth: '25%' }}
        onClick={loadMore}
        loading={isLoading}
    >
        { title }
    </LoadingButton>
);

export default LoadMoreButton;
