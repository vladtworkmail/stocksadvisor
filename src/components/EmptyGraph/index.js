import { Typography } from '@mui/material';
import { Icon } from '@iconify/react';
import slashIcon from '@iconify/icons-eva/slash-outline';
import { styled } from '@mui/material/styles';
import { localizeText } from '../../i18next';

const EmptyGraphContainer = styled('div')(() => ({
    opacity: 0.6,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center'
}));

const EmptyGraph = () => (
    <EmptyGraphContainer>
        <Icon icon={slashIcon} style={{ fontSize: 30, opacity: 0.6 }} />
        <Typography>
            { localizeText('chart.notEnoughData') }
        </Typography>
    </EmptyGraphContainer>
);

export default EmptyGraph;
