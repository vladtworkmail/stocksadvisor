import React  from 'react';
import { styled } from '@mui/material/styles';
import Chart from '../Chart';
import SummaryInfoComponent from './components/SummaryInfo';
import NewsList from '../NewsList';
import TableInfo from './components/TableInfo';
// ---------------
const StockSummaryContainer = styled('div')(() => ({
    display: 'flex',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    gap: 20
}));
const StockContainer = styled('div')(() => ({
    flexGrow: 1,
    width: 700
}));
const SummaryInfo = styled('div')(() => ({
    flexGrow: 1,
    minWidth: 300
}));

const StockSummary = ({
    symbol, chartData,
    selectedChartFilterValue, isLoadingDetail,
    handleChartFilterChange, stockDetails, onNewsCardClick,
    isLoadingNews, news, fullQuote,
    isNewsModalOpen,
    onNewsModalClose,
    newsContent
}) => {

    return (
        <div>
            <StockSummaryContainer>
                <StockContainer>
                    <Chart
                        symbol={symbol}
                        chart={chartData}
                        showChartButtons
                        selectedFilterValue={selectedChartFilterValue}
                        onChartButtonClick={handleChartFilterChange}
                    />
                </StockContainer>
                <SummaryInfo>
                    <SummaryInfoComponent
                        fullQuote={fullQuote}
                        isLoadingDetail={isLoadingDetail}
                    />
                </SummaryInfo>
                <TableInfo
                    stockDetails={stockDetails}
                    fullQuote={fullQuote}
                    isLoadingDetail={isLoadingDetail}
                />
            </StockSummaryContainer>
            { news.length && (
                <NewsList
                    title='News'
                    isLoading={isLoadingNews}
                    news={news}
                    onNewsCardClick={onNewsCardClick}
                    isNewsModalOpen={isNewsModalOpen}
                    onNewsModalClose={onNewsModalClose}
                    newsContent={newsContent}
                />
            ) }
        </div>
    );
};

export default StockSummary;
