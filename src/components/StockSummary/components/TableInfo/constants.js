export const EXCHANGE = 'primaryExchange';

export const rows = [
    {
        label: 'Exchange',
        value: 'primaryExchange'
    },
    {
        label: 'EPS',
        value: 'incomeNetPerWabsoSplitAdjusted',
        shortVersionAvailable: true
    },
    {
        label: 'Dividend Yield %',
        value: 'dividendYield',
        shortVersionAvailable: true
    },
    {
        label: 'Dividend Per Share',
        value: 'dividendPerShare',
        shortVersionAvailable: true
    },
    {
        label: 'Current Ration',
        value: 'currentRatio'
    },
    {
        label: 'BVPS',
        value: 'bookValuePerShare'
    },
    {
        label: 'P/E',
        value: 'peRatio',
        shortVersionAvailable: true
    },
    {
        label: 'EBIT Growth %',
        value: 'ebitGrowth'
    },
    {
        label: 'EBIT',
        value: 'ebitReported',
        isShortenNumber: true
    },
    {
        label: 'EBITDA Growth %',
        value: 'ebitdaGrowth'
    },
    {
        label: 'Market Cap',
        value: 'marketCap',
        isShortenNumber: true
    },
    {
        label: 'Net Incomes growth %',
        value: 'netIncomeGrowth'
    },
    {
        label: 'Net Debt',
        value: 'netDebt',
        isShortenNumber: true
    },
    {
        label: 'Debt To Asset',
        value: 'debtToAssets',
        isShortenNumber: true
    },
    {
        label: 'P/B',
        value: 'pToBv',
        shortVersionAvailable: true
    },
    {
        label: 'P/S',
        value: 'priceToRevenue',
        shortVersionAvailable: true
    },
    {
        label: 'Quick Ration',
        value: 'quickRatio',
    },
    {
        label: 'Total Debt',
        value: 'totalDebt',
        isShortenNumber: true
    },
    {
        label: 'ROA',
        value: 'returnOnAssets',
        shortVersionAvailable: true
    },
    {
        label: 'ROE',
        value: 'returnOnEquity',
        shortVersionAvailable: true
    },
    {
        label: 'Total Capital',
        value: 'totalCapital',
        isShortenNumber: true
    },
    {
        label: 'FCF Growth %',
        value: 'freeCashFlowGrowth'
    },
    {
        label: 'FCF',
        value: 'freeCashFlow',
        isShortenNumber: true
    },
    {
        label: '1Y High',
        value: 'week52High'
    },
    {
        label: '1Y Low',
        value: 'week52Low'
    },
];
