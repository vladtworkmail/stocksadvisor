import React from 'react';
import {
    Card, Table, TableContainer,
    TableBody, TableRow, TableCell
} from '@mui/material';
import Loader from '../../../Loader';
import {EXCHANGE, rows} from './constants';
import { fShortenNumber } from '../../../../utils';
import moment from "moment";
import {CustomTableCell} from "../../../StockFinancials/components/StockOverview/components/Valuation/valuationTable";
import {CustomCard} from "../../../StockFinancials/components/StockOverview/components/CompanyInfo";

const TableInfo = ({ shortenVersion, stockDetails, isLoadingDetail, fullQuote, styles }) => {
    const getRowValue = (value, isShortenNumber) => {
        if (isShortenNumber) {
            return fShortenNumber(stockDetails[value] || fullQuote[value]);
        }else if (value === EXCHANGE) {
            return fullQuote[value];
        }
        return stockDetails[value]?.toFixed(2) || fullQuote[value]?.toFixed(2);
    };

    const getTableRow = (label, value, isShortenNumber) => (
        <TableRow key={label} hover>
            <CustomTableCell sx={{ minWidth: 170, fontWeight: 600, opacity: 0.7, fontSize: 13 }}>
                { label }:
            </CustomTableCell>
            <CustomTableCell align='right' sx={{ fontWeight: 'bold', fontSize: 13 }}>
                {getRowValue(value, isShortenNumber)}
            </CustomTableCell>
        </TableRow>
    );

    const getRowsData = tableRows => (
        tableRows.map(({ label, value, isShortenNumber, shortVersionAvailable }) => {
            if (shortenVersion) {
                return shortVersionAvailable && getTableRow(label, value, isShortenNumber);
            }
            return getTableRow(label, value, isShortenNumber);
        })
    );

    return (
        <CustomCard style={{ width: '100%', margin: '0 auto', ...styles }}>
            { isLoadingDetail ? <Loader style={{ padding: 10 }} /> : (
                <TableContainer sx={{ display: 'flex', padding: 1 }}>
                    {
                        shortenVersion ?
                            (
                               <>
                                   <Table>
                                       <TableBody>
                                           { getRowsData(rows.slice(0, 12)) }
                                       </TableBody>
                                   </Table>
                                   <Table>
                                       <TableBody>
                                           { getRowsData(rows.slice(12, 24)) }
                                       </TableBody>
                                   </Table>
                               </>
                            )
                            :
                            (
                                <>
                                    <Table>
                                        <TableBody>
                                            { getRowsData(rows.slice(0, 5)) }
                                        </TableBody>
                                    </Table>
                                    <Table>
                                        <TableBody>
                                            { getRowsData(rows.slice(5, 10)) }
                                        </TableBody>
                                    </Table>
                                    <Table>
                                        <TableBody>
                                            { getRowsData(rows.slice(10, 15)) }
                                        </TableBody>
                                    </Table>
                                    <Table>
                                        <TableBody>
                                            { getRowsData(rows.slice(15, 20)) }
                                        </TableBody>
                                    </Table>
                                    <Table>
                                        <TableBody>
                                            { getRowsData(rows.slice(20, 25)) }
                                        </TableBody>
                                    </Table>
                                </>
                            )
                    }
                </TableContainer>
            ) }
        </CustomCard>
    );
}

export default TableInfo;
