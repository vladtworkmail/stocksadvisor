import React from 'react';
import {
    CardHeader,
    Table,
    TableContainer,
    TableBody,
    TableRow,
    TableCell
} from '@mui/material';
import {rows, YEAR_RANGE_PRICE} from './constants';
import Loader from '../../../Loader';
import {styled} from "@mui/material/styles";
import {CustomCard} from "../../../StockFinancials/components/StockOverview/components/CompanyInfo";

const CustomTableCell = styled(TableCell)(({theme}) => ({
    fontWeight: 'bold',
    fontSize: 13,
    color: theme.palette.common.white
}));

const SummaryInfoComponent = ({ fullQuote, isLoadingDetail }) => (
    <CustomCard>
        <CardHeader title='Details' sx={{ p: 2, pb: 1 }} />
        { isLoadingDetail ? <Loader size={30} style={{ padding: 10 }} /> : (
            <TableContainer>
                <Table>
                    <TableBody>
                        { rows.map(({ label, value }) => {
                            const newValue = fullQuote[value];
                            return (
                                <TableRow key={label} hover>
                                    <CustomTableCell sx={{ fontWeight: 600, opacity: 0.7, fontSize: 13 }}>
                                        { label }
                                    </CustomTableCell>
                                    { value === YEAR_RANGE_PRICE ? (
                                        <CustomTableCell
                                            align='right'
                                        >
                                            {`${fullQuote['week52Low']} - ${fullQuote['week52High']}`}
                                        </CustomTableCell>
                                    ) : (
                                        <CustomTableCell
                                            align='right'
                                        >
                                            { newValue?.toString()?.toUpperCase() || '' }
                                        </CustomTableCell>
                                    ) }
                                </TableRow>
                            );
                        }) }
                    </TableBody>
                </Table>
            </TableContainer>
        ) }
    </CustomCard>
);

export default SummaryInfoComponent;
