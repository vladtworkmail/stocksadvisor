export const YEAR_RANGE_PRICE = 'rangePrice';

export const rows = [
    {
        label: 'Exchange',
        value: 'primaryExchange'
    },
    {
        label: 'Market Open',
        value: 'isUsMarketOpen'
    },
    {
        label: 'P/E',
        value: 'peRatio'
    },
    {
        label: '1Y Range Price',
        value: YEAR_RANGE_PRICE
    },

];
