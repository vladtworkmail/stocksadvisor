import React from "react";
import {styled} from "@mui/material/styles";
import {Card, CardContent, Typography} from "@mui/material";
import { TypographyStylesProvider } from '@mantine/core';
/////////
const CustomCard = styled(Card)(({ theme }) => ({
    backgroundColor: theme.palette.widgetItemColor,
    color: theme.palette.common.white,
    boxShadow: 'none',
}));
const CustomContent = styled(CardContent)(({ theme }) => ({
    color: theme.palette.widgetItemTextColor,
    padding: 10,
    display: 'flex',
    whiteSpace: 'wrap',
    minWidth: 120,
    maxWidth: 269,
    minHeight: 70,
    maxHeight: 270,
    overflow: 'auto',
    flexDirection: 'column',
    justifyContent: 'space-between'
}));
const DateText = styled(Typography)(({ theme }) => ({
    color: theme.palette.widgetItemTextColor,
    paddingTop: 10,
    paddingLeft: 10,
    paddingBottom: 10,
    fontSize: 13
}));


const WidgetItem = ({content, date, styles}) => (
    <CustomCard style={{...styles}}>
        <CustomContent>
            <TypographyStylesProvider>
                <div dangerouslySetInnerHTML={{__html: content}}/>
            </TypographyStylesProvider>
        </CustomContent>
        <DateText variant="body2">
            {date}
        </DateText>
    </CustomCard>
);

export default WidgetItem;
