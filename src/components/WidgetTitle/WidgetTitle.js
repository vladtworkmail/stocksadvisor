import React from "react";
import {Link} from "react-router-dom";
import {styled} from "@mui/material/styles";
import {Card} from "@mui/material";
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import {useTheme} from "@mui/styles";

const CustomLink = styled(Link)(({ theme }) => ({
    textDecoration: 'none',
    display: 'flex',
    fontSize: 15,
    gap: 8,
    width: 'fit-content',
    padding: 0,
    alignItems: 'center',
    color: theme.palette.common.white,
    '&:hover': {
        cursor: 'pointer',
        textDecoration: 'underline'
    }
}));

const WidgetTitle = ({title, path = '#'}) => {
    const theme = useTheme();
    return (
        <CustomLink
            to={path}
        >
            {title.toUpperCase()}
            <ArrowForwardIosIcon sx={{color: theme.palette.primary.main, fontSize: 15}}/>
        </CustomLink>
    );
}

export default WidgetTitle;
