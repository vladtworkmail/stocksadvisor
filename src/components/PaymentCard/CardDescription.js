import React from "react";

const CardDescription = ({ title, description }) => (
    <div className="card-description">
        <h2>{ title }</h2>
        <p>{ description }</p>
    </div>
);

export default CardDescription;
