import React from "react";
import { Icon } from '@iconify/react';
import checkIcon from '@iconify/icons-eva/checkmark-outline';
import {nanoid} from "nanoid";

const CardFeatures = ({ data }) => (
    <div className="card-features">
        {
            data.map((item, index) => {
                return (
                    <div key={nanoid()} className="card-features-list">
                        <Icon icon={checkIcon} style={{fontSize: 17, color: 'green'}} />
                        <p key={index}>{item}</p>
                    </div>
                )
            })
        }
    </div>
);

export default CardFeatures;
