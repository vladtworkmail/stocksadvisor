import React from "react";

const CardBilling = ({ price, recurrency }) => (
    <div className="card-billing">
        <p>
            <strong className="price">$ { price }</strong>
            <strong> / mo.</strong>
        </p>
        <p>
				<span className="recurrency">
					Billed Anually or	$ { recurrency }/monthly
				</span>
        </p>
    </div>
);

export default CardBilling;
