import React from "react";
import CardDescription from "./CardDescription";
import CardBilling from "./CardBilling";
import CardFeatures from "./CardFeatures";
import CardAction from "./CardAction";
import './styles.css';

const PaymentCard = (props) => {
    const {
        type,
        title,
        description,
        price,
        recurrency,
        mostPopular,
        data,
        onClick
    } = props;

    return (
        <div className={`card pricing-card ${type}`}>
            { (mostPopular) ? <span className="most-popular">Most Popular</span> : null }
            <CardDescription title={title} description={description} />
            <CardBilling price={price} recurrency={recurrency} />
            <CardFeatures data={data} />
            <CardAction clickMe={onClick} />
        </div>
    );
};

export default PaymentCard;
