import React from "react";

const CardAction = ({ onclick }) => (
    <div className="card-action">
        <button onClick={onclick}>BUY NOW</button>
    </div>
);

export default CardAction;
