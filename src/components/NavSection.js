import { useState } from 'react';
import PropTypes from 'prop-types';
import { Icon } from '@iconify/react';
import { NavLink as RouterLink, matchPath, useLocation } from 'react-router-dom';
import arrowIosForwardFill from '@iconify/icons-eva/arrow-ios-forward-fill';
import arrowIosDownwardFill from '@iconify/icons-eva/arrow-ios-downward-fill';
// material
import { alpha, useTheme, styled } from '@mui/material/styles';
import {Box, List, Collapse, ListItemText, ListItemIcon, ListItemButton, Button, Tooltip} from '@mui/material';

// ----------------------------------------------------------------------

const ListItemStyle = styled((props) => <Button {...props} />)(
  ({ theme }) => ({
    ...theme.typography.body2,
    position: 'relative',
    color: theme.palette.common.white,
  })
);

// ----------------------------------------------------------------------

NavItem.propTypes = {
  item: PropTypes.object,
  active: PropTypes.func
};

function NavItem({ item, active, isLastElem }) {
  const theme = useTheme();
  const isActiveRoot = active(item.path);
  const { title, path, icon, info } = item;

  const activeRootStyle = {
    fontWeight: 'fontWeightMedium',
    color: alpha(theme.palette.primary.main, 0.88),
    '&:before': { display: 'block' }
  };

  return (
      <ListItemStyle
          component={RouterLink}
          to={path}
          size="medium"
          sx={{
            ...(isActiveRoot && activeRootStyle),
              paddingLeft: 2,
              paddingRight: 2,
              borderRadius: 0,
              borderRight: isLastElem ? 0 : 1,
              borderColor: theme.palette.grey[0]
          }}
      >
          <Box style={{display: 'flex', gap: 4, alignItems: 'center'}}>
              {icon}
              {title}
          </Box>
      </ListItemStyle>
  );
}

NavSection.propTypes = {
  navConfig: PropTypes.array
};

export default function NavSection({ navConfig, ...other }) {
  const { pathname } = useLocation();
  const match = (path) => (path ? !!matchPath({ path, end: false }, pathname) : false);

  return (
    <Box {...other} sx={{display: 'flex', justifyContent: 'center'}}>
        {navConfig.map((item, index, arr) => (
          <NavItem
              key={item.title}
              item={item}
              active={match}
              isLastElem={index === arr.length - 1}
          />
        ))}
    </Box>
  );
}
