import * as React from 'react';
import {TextField} from "@mui/material";
import InputAdornment from '@mui/material/InputAdornment';
import {useTheme} from "@mui/styles";

export default function CustomTextField ({
    icon, iconPosition,
    size, onInputChange,
    placeholder,
    value, ...props
}) {
    const theme = useTheme();
    return (
        <TextField
            {...props}
            autoFocus
            size={size || 'medium'}
            autoComplete='off'
            onChange={onInputChange}
            value={value}
            InputProps={{
                    startAdornment: icon && (
                        <InputAdornment position={iconPosition || 'start'}>
                            {icon}
                        </InputAdornment>
                    ),
                    style: {color: theme.palette.common.white}
                }
            }
            placeholder={placeholder}
        />
    );
}
