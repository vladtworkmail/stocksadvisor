import { Icon } from '@iconify/react';
import pieChart2Fill from '@iconify/icons-eva/pie-chart-2-fill';
import DashboardIcon from '@mui/icons-material/Dashboard';
import btc from '@iconify/icons-ic/baseline-currency-bitcoin';
import NewspaperIcon from '@mui/icons-material/Newspaper';
// ----------------------------------------------------------------------

const getIcon = name => <Icon icon={name} width={24} height={24} />;

const sidebarConfig = [
  {
    title: 'News',
    path: '/news',
    icon: <NewspaperIcon/>
  },
  {
    title: 'My Dashboard',
    path: '/dashboard',
    icon: <DashboardIcon/>
  },
  {
    title: 'Stocks',
    path: '/stock',
    icon: getIcon(pieChart2Fill)
  },
  {
    title: 'Cryptocurrency',
    path: '/crypto',
    icon: getIcon(btc)
  },
];

export default sidebarConfig;
