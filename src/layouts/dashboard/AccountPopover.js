import {useRef, useState} from 'react';
import { Button, Box, Divider, MenuItem, Typography, Avatar, IconButton } from '@mui/material';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import DashboardIcon from '@mui/icons-material/Dashboard';
import SettingsIcon from '@mui/icons-material/Settings';
import PersonIcon from '@mui/icons-material/Person';
import { alpha, styled } from '@mui/material/styles';
import MenuPopover from '../../components/MenuPopover';
import { useAuth } from '../../hook/auth';
import AvaIcon from '../../assets/ava.png';
// ----------------------------------------------------------------------

const MENU_OPTIONS = [
	{
		label: 'My Dashboard',
		icon: DashboardIcon,
		linkTo: '/dashboard'
	},
	{
		label: 'Profile',
		icon: PersonIcon,
		linkTo: '#'
	},
	{
		label: 'Settings',
		icon: SettingsIcon,
		linkTo: '#'
	}
];

// ----------------------------------------------------------------------

const RowBox = styled(Box)(({ theme }) => ({
	display: 'flex',
	justifyContent: 'space-around',
	gap: 20,
	color: theme.palette.common.white
}));
const StyledLink = styled(RouterLink)(({ theme, customstyle = {} }) => ({
	color: theme.palette.common.white,
	...customstyle
}));

export default function AccountPopover() {
	const anchorRef = useRef(null);
	const { isAuthorized, userName, userMail, logOut } = useAuth();
	const [open, setOpen] = useState(false);
	const navigate = useNavigate();
	const handleOpen = () => setOpen(true);
	const handleClose = () => setOpen(false);
	const handleLogin = () => navigate('/auth/login');
	const handleLogout = () => {
		logOut();
		handleClose();
	}

	if (!isAuthorized) {
		return (
			<RowBox>
				<StyledLink customstyle={{textDecoration: 'none'}} to="/auth/login">Login</StyledLink>
				<StyledLink to="/auth/register">Register</StyledLink>
			</RowBox>
		)
	}

	return (
		<>
			<IconButton
				ref={anchorRef}
				onClick={handleOpen}
				sx={{
					padding: 0,
					width: 44,
					height: 44,
					...(open && {
						'&:before': {
							zIndex: 1,
							content: "''",
							width: '100%',
							height: '100%',
							borderRadius: '50%',
							position: 'absolute',
							bgcolor: theme => alpha(theme.palette.grey[900], 0.72)
						}
					})
				}}
			>
				<Avatar alt='photoURL' src={ isAuthorized ? AvaIcon : ''} sx={{ borderRadius: 0.4 }} />
			</IconButton>

			<MenuPopover
				open={open}
				onClose={handleClose}
				anchorEl={anchorRef.current}
				sx={{ width: 220 }}
			>
				{ !isAuthorized ? (
					<Box sx={{ p: 2, pt: 1.5 }}>
						<Button fullWidth color='inherit' variant='outlined' onClick={handleLogin}>
							Login
						</Button>
					</Box>
				) : (
					<>
						<Box sx={{ my: 1.5, px: 2.5 }}>
							<Typography variant='subtitle1' noWrap>
								{ userName }
							</Typography>
							<Typography variant='body2' sx={{ color: 'text.secondary' }} noWrap>
								{ userMail }
							</Typography>
						</Box>

						<Divider sx={{ my: 1 }} />

						{ MENU_OPTIONS.map(option => (
							<MenuItem
								key={option.label}
								to={option.linkTo}
								component={RouterLink}
								onClick={handleClose}
								sx={{ typography: 'body2', py: 1, px: 2.5 }}
							>
								<Box
									component={option.icon}
									sx={{
										mr: 2,
										width: 24,
										height: 24
									}}
								/>
								{ option.label }
							</MenuItem>
						))}

						<Box sx={{ p: 2, pt: 1.5 }}>
							<Button fullWidth color='inherit' variant='outlined' onClick={handleLogout}>
								Logout
							</Button>
						</Box>
					</>
				)}
			</MenuPopover>
		</>
	);
};
