import { styled, alpha } from '@mui/material/styles';
import { Box, ClickAwayListener, Fade } from '@mui/material';
import { Link } from 'react-router-dom';
import Loader from '../../components/Loader';
import InputBase from '@mui/material/InputBase';

import SearchIcon from '@mui/icons-material/Search';
import { value } from 'lodash/seq';
import { useTheme } from '@mui/styles';
// ----------------------------------------------------------------------

const StyledLink = styled(Link)(({ theme }) => ({
	display: 'flex',
	flexDirection: 'row',
	alignItems: 'center',
	textDecoration: 'none',
	color: theme.palette.grey[0],
	'&:hover': {
		cursor: 'pointer',
		textDecoration: 'none'
	}
}));
const StyledLinkImage = styled('img')(() => ({
	width: 25,
	height: 25,
	margin: 10
}));
const StyledLinkSymbol = styled('p')(() => ({
	fontSize: 13,
	fontWeight: 'bold',
}));
const StyledLinkName = styled('p')(() => ({
	fontSize: 11
}));

const StyledLinkContainer = styled(Box)(({ theme }) => ({
	width: '100%',
	textDecoration: 'none',
	color: theme.palette.grey[0],
	transition: '.5s',
	borderRadius: 20,
	'&:hover': {
		backgroundColor: alpha(theme.palette.primary.dark, 0.5),
		borderRadius: 10
	}
}));

const Search = styled('div')(({ theme }) => ({
	position: 'relative',
	height: 36,
	borderRadius: theme.spacing(1),
	transition: '.2s',
	backgroundColor: theme.palette.primary.light,
	marginRight: theme.spacing(4),
	'&:focus': {
		backgroundColor: alpha(theme.palette.primary.dark, 0.25)
	}
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
	padding: theme.spacing(0, 2),
	height: '100%',
	position: 'absolute',
	pointerEvents: 'none',
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
	width: 'inherit',
	color: 'white',
	'& .MuiInputBase-input': {
		padding: theme.spacing(1, 1, 1, 0),
		// vertical padding + font size from searchIcon
		paddingLeft: `calc(1em + ${theme.spacing(4)})`,
		transition: theme.transitions.create('width'),
		width: '100%'
	}
}));

const SearchResultsStyle = styled(Box)(({ theme }) => ({
	padding: theme.spacing(1),
	backgroundColor: alpha(theme.palette.primary.dark, 0.88),
	backdropFilter: 'blur(2px)',
	position: 'absolute',
	width: 'inherit',
	maxHeight: 300,
	overflow: 'scroll',
	boxShadow: '0px 0px 20px 0px rgba(0, 0, 0, 0.5)'
}));

const NoResults = styled('p')(() => ({
	fontSize: 13,
	fontWeight: 'bold'
}));
// ----------------------------------------------------------------------

export default function Searchbar({
	searchValue,
	list,
	isLoading,
	isSearchResults,
	handleSearch,
	handleClear,
	redirectionLink,
	style
}) {
	const theme = useTheme();
	const getResults = () => {
		if(isLoading) return <Loader style={{ padding: 1 }} size={20} />;
		if(!list?.length) return <NoResults style={{ color: 'white' }}>No Results found...</NoResults>;
		return list?.map(elem => (
			<StyledLinkContainer key={elem.name}>
				<StyledLink
					to={`${redirectionLink}?symbol=${elem.symbol}`}
					onClick={handleClear}
				>
					<StyledLinkImage src={elem.logo} />
					<div>
						<StyledLinkSymbol>
							{ elem.symbol }
						</StyledLinkSymbol>
						<StyledLinkName>
							{ elem.name }
						</StyledLinkName>
					</div>
				</StyledLink>
			</StyledLinkContainer>
		));
	};

	return (
		<ClickAwayListener onClickAway={handleClear}>
			<Box sx={{ position: 'relative', width: 300, zIndex: 2, ...style }}>
				<Search
					sx={{
						width: '100%',
						borderBottomLeftRadius: theme.spacing(+!isSearchResults),
						borderBottomRightRadius: theme.spacing(+!isSearchResults)
					}}
				>
					<SearchIconWrapper>
						<SearchIcon style={{ color: theme.palette.common.white }} />
					</SearchIconWrapper>
					<StyledInputBase
						placeholder='Search…'
						inputProps={{ 'aria-label': 'search' }}
						onChange={e => handleSearch(e.target.value)}
						value={searchValue}
					/>
				</Search>
				<Fade in={!!searchValue}>
					<SearchResultsStyle
						visibility={value}
						sx={{
							borderBottomLeftRadius: theme.spacing(1),
							borderBottomRightRadius: theme.spacing(1)
						}}
					>
						{ getResults() }
					</SearchResultsStyle>
				</Fade>
			</Box>
		</ClickAwayListener>
  	);
};
