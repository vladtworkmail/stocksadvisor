import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
// material
import { styled } from '@mui/material/styles';
import { Typography } from '@mui/material';
// components
import Logo from '../components/Logo';
//
import { MHidden } from '../components/@material-extend';
import { useTheme } from '@mui/styles';

// ----------------------------------------------------------------------

const HeaderStyle = styled('header')(({theme}) => ({
    top: 0,
    zIndex: 9,
    lineHeight: 0,
    right: 0,
    position: 'absolute',
    padding: theme.spacing(3),
    justifyContent: 'space-between',
    [theme.breakpoints.up('md')]: {
        alignItems: 'flex-start',
        padding: theme.spacing(7, 5, 0, 7)
    }
}));

// ----------------------------------------------------------------------

const AuthLayout = ({ children }) => {
    const theme = useTheme();
    return (
        <HeaderStyle>
            <MHidden width="smDown">
                <Typography
                    variant="body2"
                    sx={{
                        mt: {md: -2},
                        color: theme.palette.common.white
                    }}
                >
                    {children}
                </Typography>
            </MHidden>
        </HeaderStyle>
    );
};

AuthLayout.propTypes = {
    children: PropTypes.node
};

export default AuthLayout;
