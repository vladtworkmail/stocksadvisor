import React, {useState} from "react";
import {Box, Modal, Typography, Button} from "@mui/material";
import {styled} from "@mui/material/styles";
import Grid from "@mui/material/Grid";
import CloseIcon from '@mui/icons-material/Close';
import CustomTextField from "../../components/TextField";
import {value} from "lodash/seq";
import Loader from "../../components/Loader";

const Container = styled(Grid)(({ theme }) => ({
    minWidth: 460,
    maxWidth: 900,
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    backgroundColor: theme.palette.widgetItemColor,
    boxShadow: 24,
    padding: '20px 25px 20px',
    color: theme.palette.common.white
}));

const TitleTextContainer = styled(Typography)(({ theme }) => ({
    display: 'flex',
    justifyContent: 'space-between'
}));

const Subtitle = styled(Typography)(({ theme }) => ({
    marginTop: 2,
    fontSize: 14,
    color: theme.palette.widgetItemTextColor,
    whiteSpace: 'pre-line',
    'mark': {
        backgroundColor: theme.palette.grey[700],
        padding: 3,
        fontWeight: 800,
        color: theme.palette.common.white
    }
}));

const ButtonContainer = styled(Box)(({ theme }) => ({
    marginTop: 40,
    display: 'flex',
    gap: 20,
    justifyContent: 'flex-end'
}));

const RemoveDiaryModal = ({onClose, open, onDeleteDiary, diaryName, diaryId}) => {
    return (
        <Modal
            keepMounted
            open={open}
            onClose={onClose}
            aria-labelledby="keep-mounted-modal-title"
            aria-describedby="keep-mounted-modal-description"
        >
            <Container>
                <TitleTextContainer id="keep-mounted-modal-title" variant="h6" component="h2">
                    Confirm delete
                    <CloseIcon
                        fontSize="small"
                        onClick={onClose}
                    />
                </TitleTextContainer>
                <Subtitle id="keep-mounted-modal-description">
                    This diary will be deleted with all its contents and you wont be able to see them again.
                    {'\n'} {'\n'}
                    <span>
                        Are you sure you want to delete <mark>{diaryName.toUpperCase()}</mark> diary?
                    </span>
                </Subtitle>
                <ButtonContainer>
                    <Button
                        variant="text"
                        onClick={onClose}
                    >
                        Close
                    </Button>
                    <Button
                        variant="contained"
                        onClick={() => onDeleteDiary(diaryId)}
                    >
                        Delete
                    </Button>
                </ButtonContainer>
            </Container>
        </Modal>
    );
};

export default RemoveDiaryModal;
