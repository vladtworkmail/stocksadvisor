import { toast } from 'react-toastify';

export const isRemovedStocksValid = (stocks, tableData) => {
    if (stocks.length > tableData.length) {
        toast.error('stocks to remove more than existing in the portfolio');
        return false;
    }

    let isValid = true;
    stocks.forEach(stock => {
        const currentTableDataStock = tableData.find(({symbol}) => symbol === stock.symbol);
        if (!currentTableDataStock) {
            isValid = false;
            toast.error(`Stock ${stock.symbol} does not exist in your portfolio`);
        } else if (currentTableDataStock.quantity < stock.quantity) {
            isValid = false;
            toast.error(`Quantity of ${stock.symbol} exceeded`);
        }

    });

    return isValid;
};

export const isAddedStockValid = (candidateSymbol, currentStocks, addedItems, clearResults) => {
    if (currentStocks.find(({symbol}) => symbol === candidateSymbol)) {
        toast.warn('This stock is already in your portfolio!');
        clearResults();
        return false;
    }
    if (addedItems.find((addedStock) => addedStock.id === candidateSymbol)) {
        toast.warn('You have already added this stock!');
        clearResults();
        return false;
    }
    return true;
};
