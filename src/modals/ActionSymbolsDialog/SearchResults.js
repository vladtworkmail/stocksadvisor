import React from 'react';
import {List, ListItemText, ListItemButton} from '@mui/material';
import Loader from "../../components/Loader";
import {styled} from "@mui/material/styles";

const CustomList = styled(List)(({ theme }) => ({
    position: 'absolute',
    zIndex: 10,
    minWidth: 227,
    maxHeight: 250,
    overflow: 'auto',
    backgroundColor: theme.palette.common.black,
    boxShadow: 'none',
    color: theme.palette.common.white,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5
}));

const SearchResults = ({ searchList, onItemButtonClick, selectedId, searchValue, searchIsLoading }) => {
    if (searchIsLoading) {
        return (
            <Loader
                size={30}
                style={{ position: 'absolute', right: 0, top: 5, p: 1 }}
            />
        );
    }

    return (
        !searchList.length || !searchValue ? null : (
            <CustomList>
                { searchList.map(({ symbol, name, content }) => (
                    <ListItemButton
                        key={symbol}
                        selected={selectedId === symbol}
                        onClick={() => onItemButtonClick({ id: symbol, name, content })}
                    >
                        <ListItemText
                            primary={name}
                        />
                    </ListItemButton>
                )) }
            </CustomList>
        )
    );
};

export default SearchResults;
