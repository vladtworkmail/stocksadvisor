import * as React from 'react';
import SearchIcon from '@mui/icons-material/Search';
import {ClickAwayListener, TextField} from "@mui/material";
import InputAdornment from '@mui/material/InputAdornment';
import {useTheme} from "@mui/styles";

export default function CustomizedInputBase({ onSearchClear, size, onSearchInputChange, searchValue, ...props }) {
    const theme = useTheme();

    return (
        <ClickAwayListener onClickAway={onSearchClear}>
            <TextField
                { ...props }
                autoFocus
                size={size || 'medium'}
                autoComplete='off'
                onChange={onSearchInputChange}
                value={searchValue}
                InputProps={{
                    style: {
                        color: theme.palette.common.white,
                        borderRadius: 4
                    },
                    startAdornment: (
                        <InputAdornment position='start'>
                            <SearchIcon />
                        </InputAdornment>
                    ),
                }}
                placeholder='Symbol...'
            />
        </ClickAwayListener>
    );
}
