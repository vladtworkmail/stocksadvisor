import React from "react";
import {
    Box,
    List,
    ListItem,
    ListItemText,
    Typography,
    IconButton,
    Divider,
    TextField,
    ListItemIcon
} from "@mui/material";
import DeleteIcon from '@mui/icons-material/Delete';
import {styled} from "@mui/material/styles";
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import CustomDatePicker from "../../components/DatePicker";
import moment from "moment";


const HeaderContainer = styled(Box)(({ theme }) => ({
    marginTop: 5,
    display: 'flex',
    justifyContent: 'space-between',
    backgroundColor: theme.palette.grey[700],
    padding: 4,
}));

const AddedSymbols = ({
    addedItems, onDeleteItem,
    quantityValues, onPriceChange,
    priceValues, onQuantityChange, titleList,
                          onDatePickerChange, datePickerValues
}) => (
    <Box sx={{ mt: 7 }}>
        <Typography variant="subtitle1">{titleList}:</Typography>
        <HeaderContainer>
            <Typography variant="subtitle2">Symbol</Typography>
            <Typography variant="subtitle2">Quantity</Typography>
            <Typography variant="subtitle2">Price</Typography>
            <Typography variant="subtitle2">Date</Typography>
            <Typography variant="subtitle2">Action</Typography>
        </HeaderContainer>
            <List sx={{ height: '30vh', overflow: 'scroll' }}>
                {
                    !addedItems.length ? null : addedItems.map(({content, name, id}, index) => (
                        <ListItem
                            key={id}
                            divider
                            secondaryAction={
                                <IconButton
                                    edge="end"
                                    aria-label="delete"
                                    onClick={() => onDeleteItem(id)}
                                >
                                    <DeleteIcon />
                                </IconButton>
                            }
                        >
                            <ListItemText
                                primary={name}
                                secondary={content}
                            />
                            <Box sx={{display: 'flex', gap: 15, width: '75%'}}>
                                <TextField
                                    size="small"
                                    autoComplete='off'
                                    variant="standard"
                                    sx={{width: 65}}
                                    inputProps={{style: { color: 'white', textAlign: 'center' }}}
                                    placeholder="Quantity"
                                    value={quantityValues[index]}
                                    onChange={({target: {value}}) => onQuantityChange(value, index)}
                                />
                                <TextField
                                    size="small"
                                    autoComplete='off'
                                    variant="standard"
                                    sx={{width: 80}}
                                    placeholder="Price $"
                                    inputProps={{style: { color: 'white', textAlign: 'center' }}}
                                    value={priceValues[index]}
                                    onChange={({target: {value}}) => onPriceChange(value, index)}
                                />
                                <CustomDatePicker
                                    value={datePickerValues[index]}
                                    onChange={(newValue) => onDatePickerChange(newValue, index)}
                                />
                            </Box>
                        </ListItem>
                    ))
                }
            </List>
    </Box>
);

export default AddedSymbols;
