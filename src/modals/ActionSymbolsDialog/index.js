import React, {useState} from "react";
import {Dialog, DialogTitle, Typography, Box, Button, DialogContent} from "@mui/material";
import CloseIcon from '@mui/icons-material/Close';
import SearchInput from "./SearchInput";
import SearchResults from "./SearchResults";
import AddedSymbols from "./AddedSymbols";
import {isAddedStockValid} from "./validation";
import {useTheme} from "@mui/styles";
import useSearchStocks from "../../hook/useSearchStocks";
import moment from "moment";

const ActionSymbolDialog = ({handleClose, isOpenDialog, titleList, title, onSubmitClick, diaryId, currentStocks}) => {
    const [searchValue, setSearchValue] = useState('');
    const [selectedSearchResultId, setSelectedSearchResultId] = useState('');
    const [addedItems, setAddedItems] = useState([]);
    const [quantityValues, setQuantityValues] = useState([]);
    const [priceValues, setPriceValues] = useState([]);
    const [datePickerValues, setDatePickerValues] = useState([]);
    const { searchList, isLoadingSearch } = useSearchStocks(searchValue, setSearchValue);
    const theme = useTheme();

    const clearResults = () => {
        setSelectedSearchResultId('');
        setSearchValue('');
    };

    const handleSearchResultItemButtonClick = ({content, name, id}) => {
        setSearchValue('');

        if (!isAddedStockValid(id, currentStocks, addedItems, clearResults)) {
            return false;
        }

        setSelectedSearchResultId(id);
        const items = [...addedItems];
        items.push({content, name, id});
        setAddedItems(items);

        const quantities = [...quantityValues];
        quantities.push(1);
        setQuantityValues(quantities);

        const prices = [...priceValues];
        prices.push(100);
        setPriceValues(prices);

        const dates = [...datePickerValues];
        dates.push(new Date());
        setDatePickerValues(dates);

        clearResults();
    };

    const handleDeleteSelectedItem = (id) => {
        const items = [...addedItems];
        const itemIndex = items.findIndex((item) => item.id === id );
        items.splice(itemIndex, 1);
        setAddedItems(items);

        const quantities = [...quantityValues];
        quantities.splice(itemIndex, 1);
        setQuantityValues(quantities);

        const prices = [...priceValues];
        prices.splice(itemIndex, 1);
        setPriceValues(prices);

        const dates = [...datePickerValues];
        dates.splice(itemIndex, 1);
        setDatePickerValues(dates);

    };
    const handleSearchInputChange = ({target: {value}}) => setSearchValue(value);

    const handleQuantityChange = (value, index) => {
        const quantities = [...quantityValues];
        quantities.splice(index, 1, value);
        setQuantityValues(quantities);
    };
    const handleDatePickerChange = (value, index) => {
        const dates = [...datePickerValues];
        dates.splice(index, 1, value);
        setDatePickerValues(dates);
    };
    const handlePriceChange = (value, index) => {
        const prices = [...priceValues];
        prices.splice(index, 1, value);
        setPriceValues(prices);
    };
    const paperProps = {
        style: {
            backgroundColor: theme.palette.widgetColor,
            boxShadow: 'none',
            color: theme.palette.common.white
        }
    };

    const handleSubmitClick = () => {
        const stocksList = {
            diaryId,
            stocks: addedItems.map(({content, name, id}, i) => ({
                symbol: id,
                companyName: name,
                quantity: quantityValues[i],
                price: priceValues[i],
                buyingDate: moment(datePickerValues[i]).format("DD-MM-YYYY"),
            }))
        };
        clearResults();
        setAddedItems([]);
        onSubmitClick(stocksList);
    };

    const handleDialogClose = () => {
        setAddedItems([]);
        clearResults();
        handleClose();
    };

    return (
        <Dialog onClose={handleDialogClose} open={isOpenDialog} maxWidth="md" fullWidth PaperProps={{...paperProps}}>
            <DialogTitle sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <Typography variant="subtitle">{title}</Typography>
                <CloseIcon onClick={handleDialogClose} sx={{cursor: 'pointer'}}/>
            </DialogTitle>
            <DialogContent sx={{ display: 'flex', flexDirection: 'column' }}>
                <Box sx={{ position: 'relative', alignSelf: 'center' }}>
                    <SearchInput
                        onSearchInputChange={handleSearchInputChange}
                        searchValue={searchValue}
                        onSearchClear={clearResults}
                    />
                    <SearchResults
                        searchList={searchList}
                        searchIsLoading={isLoadingSearch}
                        selectedId={selectedSearchResultId}
                        searchValue={searchValue}
                        onItemButtonClick={handleSearchResultItemButtonClick}
                    />
                </Box>
                <AddedSymbols
                    addedItems={addedItems}
                    titleList={titleList}
                    onDeleteItem={handleDeleteSelectedItem}
                    quantityValues={quantityValues}
                    onQuantityChange={handleQuantityChange}
                    priceValues={priceValues}
                    onPriceChange={handlePriceChange}
                    onDatePickerChange={handleDatePickerChange}
                    datePickerValues={datePickerValues}
                />
                <Button
                    variant='contained'
                    size='medium'
                    disabled={!addedItems.length}
                    sx={{ width: 200, alignSelf: 'center', mt: 3 }}
                    onClick={handleSubmitClick}
                >
                    Done
                </Button>
            </DialogContent>

        </Dialog>
    );
}
export default ActionSymbolDialog;
