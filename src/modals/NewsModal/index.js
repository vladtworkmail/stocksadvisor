import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import moment from "moment";
import {styled} from "@mui/material/styles";
import {Box} from "@mui/material";
import {useTheme} from "@mui/styles";
import {CardActionText, CustomDivider, ImageBox} from "../../components/NewsCard";

const CustomDialogActions = styled(DialogActions)(({theme}) => ({
    display: 'flex',
    justifyContent: 'space-between'
}));

const CustomDialog = styled(Dialog)(({theme}) => ({
    color: theme.palette.widgetItemTextColor,
    '& .css-11zwbe3-MuiPaper-root-MuiDialog-paper': {
        backgroundColor: theme.palette.widgetColor,
        color: theme.palette.widgetItemTextColor,
        boxShadow: 'none',
    }
}));

const NewsModal = ({open, onClose, content}) => {
    const {summary, dateTime, source, headline, image} = content;
    const theme = useTheme();
    return (
        <div>
            <CustomDialog
                open={open}
                onClose={onClose}
                scroll="paper"
            >
                <DialogTitle>{headline}</DialogTitle>
                <DialogContent dividers>
                    <ImageBox
                        backgroundImage={image}
                        style={{marginBottom: 15}}
                    />
                    <DialogContentText
                        style={{color: theme.palette.common.white}}
                        tabIndex={-1}
                    >
                        {summary || 'No content here...'}
                    </DialogContentText>
                </DialogContent>
                <CustomDialogActions>
                    <Box style={{display: 'flex', marginLeft: 10}}>
                        <AccessTimeIcon fontSize="small" style={{color: theme.palette.grey[600]}}/>
                        <CardActionText variant="caption">
                            {moment(dateTime).startOf('day').fromNow()}
                        </CardActionText>
                        <CustomDivider orientation="vertical" flexItem/>
                        <CardActionText variant="caption">
                            {source}
                        </CardActionText>
                    </Box>
                    <Button onClick={onClose}>Cancel</Button>
                </CustomDialogActions>
            </CustomDialog>
        </div>
    );
};

export default NewsModal;
