import React, {useState} from "react";
import {Box, Modal, Typography, Button, List, ListItemText, TextField} from "@mui/material";
import {styled} from "@mui/material/styles";
import Grid from "@mui/material/Grid";
import CloseIcon from '@mui/icons-material/Close';
import moment from "moment";
import CustomDatePicker from "../../components/DatePicker";

const Container = styled(Grid)(({ theme }) => ({
    minWidth: 460,
    maxWidth: 900,
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    backgroundColor: theme.palette.widgetItemColor,
    boxShadow: 24,
    padding: '20px 25px 20px',
    color: theme.palette.common.white
}));

const TitleTextContainer = styled(Typography)(({ theme }) => ({
    display: 'flex',
    justifyContent: 'space-between'
}));

const Subtitle = styled(Typography)(({ theme }) => ({
    marginTop: 2,
    fontSize: 14,
    color: theme.palette.widgetItemTextColor,
    whiteSpace: 'pre-line',
}));

const ButtonContainer = styled(Box)(({ theme }) => ({
    marginTop: 40,
    display: 'flex',
    gap: 20,
    justifyContent: 'flex-end'
}));
const InputContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'flex-end',
    marginTop: 12,
    justifyContent: 'space-around',
    maxWidth: 210
}));

const ChangeDiaryStocksQuantityModal = ({onClose, open, onChangeStockQuantity, changeStockQtParams}) => {
    const { actionType, stock} = changeStockQtParams;
    const [quantity, setQuantity] = useState(1);
    const [price, setPrice] = useState(100);
    const [dateValue, setDateValue] = useState(new Date());

    const setDefaultParams = () => {
        setQuantity(1);
        setPrice(100);
    };

    const handleChangeStockQuantity = () => {
        const params = {
            diaryStockId: stock.id,
            changedQuantity: actionType === 'Buy' ? quantity : -quantity,
            price,
            newDate: moment(dateValue).format("DD-MM-YYYY"),
            note: ''
        };
        setDefaultParams();
        onChangeStockQuantity(params);
    };

    const handleClose = () => {
        setDefaultParams();
        onClose();
    };

    return (
        <Modal
            keepMounted
            open={open}
            onClose={onClose}
            aria-labelledby="keep-mounted-modal-title"
            aria-describedby="keep-mounted-modal-description"
        >
            <Container>
                <TitleTextContainer id="keep-mounted-modal-title" variant="h6" component="h2">
                    {actionType === 'Buy' ? 'Buying' : 'Selling'} {stock.symbol}
                    <CloseIcon
                        fontSize="small"
                        onClick={onClose}
                    />
                </TitleTextContainer>
                <Subtitle id="keep-mounted-modal-description">
                    You are {actionType === 'Buy' ? 'buying' : 'selling'} stocks of {stock.companyName}
                </Subtitle>
                <InputContainer>
                    <Typography variant="body1">Price:</Typography>
                    <TextField
                        size="small"
                        autoComplete='off'
                        variant="standard"
                        sx={{width: 80}}
                        placeholder="Price"
                        inputProps={{style: { color: 'white', textAlign: 'center' }}}
                        value={price}
                        onChange={({target: {value}}) => setPrice(value)}
                    />
                </InputContainer>
                <InputContainer>
                    <Typography variant="body1">Quantity:</Typography>
                    <TextField
                        size="small"
                        autoComplete='off'
                        variant="standard"
                        sx={{width: 80}}
                        inputProps={{style: { color: 'white', textAlign: 'center' }}}
                        placeholder="Quantity"
                        value={quantity}
                        onChange={({target: {value}}) => setQuantity(value)}
                    />
                </InputContainer>
                <InputContainer>
                    <CustomDatePicker
                        value={dateValue}
                        onChange={(val) => setDateValue(val)}
                    />
                </InputContainer>
                <ButtonContainer>
                    <Button
                        variant="text"
                        onClick={handleClose}
                    >
                        Close
                    </Button>
                    <Button
                        variant="contained"
                        onClick={handleChangeStockQuantity}
                    >
                        {actionType}
                    </Button>
                </ButtonContainer>
            </Container>
        </Modal>
    );
};

export default ChangeDiaryStocksQuantityModal;
