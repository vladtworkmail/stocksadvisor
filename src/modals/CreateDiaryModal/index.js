import React, {useState} from "react";
import {Box, Modal, Typography, Button} from "@mui/material";
import {styled} from "@mui/material/styles";
import Grid from "@mui/material/Grid";
import CloseIcon from '@mui/icons-material/Close';
import CustomTextField from "../../components/TextField";
import {value} from "lodash/seq";
import Loader from "../../components/Loader";

const Container = styled(Grid)(({ theme }) => ({
    minWidth: 460,
    maxWidth: 900,
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    backgroundColor: theme.palette.widgetItemColor,
    boxShadow: 24,
    padding: '20px 25px 20px',
    color: theme.palette.common.white
}));

const TitleTextContainer = styled(Typography)(({ theme }) => ({
    display: 'flex',
    justifyContent: 'space-between'
}));

const Subtitle = styled(Typography)(({ theme }) => ({
    marginTop: 2,
    fontSize: 14,
    color: theme.palette.widgetItemTextColor
}));

const TextFieldTitle = styled(Typography)(({ theme }) => ({
    marginTop: 15,
    marginBottom: 5,
    fontSize: 13,
    color: theme.palette.common.white
}));

const ButtonContainer = styled(Box)(({ theme }) => ({
    marginTop: 60,
    display: 'flex',
    gap: 20,
    justifyContent: 'flex-end'
}));

const CreateDiaryModal = ({onClose, open, onDiaryCreate, isLoading}) => {
    const [inputValue, setInputValue] = useState('');

    const handleInputChange = ({target: {value}}) => setInputValue(value);
    const handleDiaryClose = () => {
        setInputValue('');
        onClose();
    };

    const handleDiaryCreate = () => {
        onDiaryCreate(inputValue);
        setInputValue('');
        onClose();
    }

    return (
        <Modal
            keepMounted
            open={open}
            onClose={handleDiaryClose}
            aria-labelledby="keep-mounted-modal-title"
            aria-describedby="keep-mounted-modal-description"
        >
            <Container>
                <TitleTextContainer id="keep-mounted-modal-title" variant="h6" component="h2">
                    Create new diary
                    <CloseIcon
                        fontSize="small"
                        onClick={handleDiaryClose}
                    />
                </TitleTextContainer>
                <Subtitle id="keep-mounted-modal-description">
                    In diaries, it is convenient to group stocks you have and track them. They can be personal or shared.
                </Subtitle>
                <TextFieldTitle>Name</TextFieldTitle>
                <CustomTextField
                    style={{width: '100%'}}
                    size="small"
                    value={inputValue}
                    onInputChange={handleInputChange}
                />
                <ButtonContainer>
                    {
                        isLoading ?
                        <Loader/>
                        :
                         <>
                             <Button
                                 variant="outlined"
                                 onClick={handleDiaryClose}
                             >
                                 Close
                             </Button>
                             <Button
                                 variant="contained"
                                 disabled={!inputValue}
                                 onClick={handleDiaryCreate}
                             >
                                 Create
                             </Button>
                         </>
                    }
                </ButtonContainer>
            </Container>
        </Modal>
    );
};

export default CreateDiaryModal;
