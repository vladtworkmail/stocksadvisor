import moment from "moment";

const formatDiaryActionHistory = (data) => {
    if (!data) {
        return [];
    }

    const result = [];

    data.forEach(elem => {
        const formattedAction = elem.actions.map((action) => ({
            ...action,
            date: moment(action.date).format('DD/MM/YYYY'),
            symbol: elem.symbol
        }));
        result.push(...formattedAction);
    });

    return result;
};

export default formatDiaryActionHistory;
