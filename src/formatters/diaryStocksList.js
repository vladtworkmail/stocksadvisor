export const formatDiaryStocksList = (stocks = []) => (
    stocks.map(stock => ({
        ...stock,
        actions: stock?.actions.filter(({type}) => type === 'Buy' || type === 'Sell'),
    }))
);
