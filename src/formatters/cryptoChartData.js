export const formatCryptoChartData = chartData => chartData.map(({ value, date }) => ({ y: value, x: date }));
