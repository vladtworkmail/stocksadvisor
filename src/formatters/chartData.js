export const formatChartData = (chartData) => chartData.map(({close, timeStamp}) => ({y: close, x: timeStamp}));
