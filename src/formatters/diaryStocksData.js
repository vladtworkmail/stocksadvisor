const formatDiaryStocks=  (stocks = [], shortQuotesList = []) => (
    stocks.map(stock => {
        const shortQuote = shortQuotesList.find(({ symbol = '' }) => symbol === stock?.symbol);
        const latestPrice = shortQuote?.latestPrice || 0;
        const changePercent = shortQuote?.changePercent?.toFixed(2) || 0;
        const change = shortQuote?.change || 0;
        const value = (latestPrice * stock?.quantity) || 0;
        const profit = (value - (stock?.averageBuyingPrice * stock?.quantity))?.toFixed(2) || 0;

        return {
            ...stock,
            profit,
            latestPrice,
            changePercent,
            change,
            value
        };
    })
);

export default formatDiaryStocks;
