import i18next from 'i18next';
import locale from './locale';

i18next.init({
    lng: 'en',
    fallbackLng: ['en', 'ru'],
    debug: false,
    resources: {
        en: {
            translation: locale.en
        },
    },
    interpolation: {
        escapeValue: false
    }
});

export const localizeText = (keys, args) => i18next.t(keys, args);
