import axios from "axios";
import jwtDecode from "jwt-decode";
import dayjs from "dayjs";
import {setupUserData} from "../utils/auth";

const getCoreLink = () => process.env.REACT_APP_ENV === 'production' ? process.env.REACT_APP_PROD_URL : process.env.REACT_APP_DEV_URL;
let token = localStorage.getItem('jwt');

const getRefreshTokenData = async () => {
    const {data} = await axios.post(`${getCoreLink()}user/refreshToken`, {}, {
        withCredentials: true,
        headers: {
            'X-API-KEY': process.env.REACT_APP_X_API_KEY
        }
    });
    return data;
};

const axiosInstance = axios.create({
    baseURL: getCoreLink(),
    withCredentials: true,
    headers: {
        Authorization: token,
        'X-API-KEY': process.env.REACT_APP_X_API_KEY
    }
});

axiosInstance.interceptors.request.use(async (req) => {
    token = localStorage.getItem('jwt') || '';
    req.headers.Authorization = token;
    return req;
});

axiosInstance.interceptors.response.use((res) => res,
    async function (error) {
        const originalRequest = error.config;
        const isUserLogin = error.request.responseURL.includes('authenticate');
        const isUserRegister = error.request.responseURL.includes('register');
        if (error.response.status === 401 && !isUserLogin && !isUserRegister && !originalRequest._retry) {
            console.dir(error)
            originalRequest._retry = true;
            try{
                const data = await getRefreshTokenData();
                setupUserData(data);
                axios.defaults.headers.common['Authorization'] = data?.jwtToken || '';
                return axiosInstance(originalRequest);
            } catch (e) {
                localStorage.clear();
                window.location.pathname = '/auth/login';
            }
        } else {
            return Promise.reject(error);
        }
});

export default axiosInstance;

