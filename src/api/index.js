import axiosInstance from "./axiosInstance";

export const postData = async (url) => await axiosInstance.post(url);
export const fetchData = async (url) => await axiosInstance.get(url);
export const updateData = async (url) => await axiosInstance.put(url);
export const deleteData = async (url) => await axiosInstance.delete(url);
