import { createGradient } from '../../utils';
import { darkTheme } from './darkTheme';

export const getPalette = themeMode => {
    const {
        GREY,
        PRIMARY,
        SECONDARY,
        INFO,
        SUCCESS,
        WARNING,
        ERROR
    } = darkTheme;

    const GRADIENTS = {
        primary: createGradient(PRIMARY.light, PRIMARY.main),
        info: createGradient(INFO.light, INFO.main),
        success: createGradient(SUCCESS.light, SUCCESS.main),
        warning: createGradient(WARNING.light, WARNING.main),
        error: createGradient(ERROR.light, ERROR.main)
    };

    const CHART_COLORS = {
        violet: ['#826AF9', '#9E86FF', '#D0AEFF', '#F7D2FF'],
        blue: ['#2D99FF', '#83CFFF', '#A5F3FF', '#CCFAFF'],
        green: ['#2CD9C5', '#60F1C8', '#A4F7CC', '#C0F2DC'],
        yellow: ['#FFE700', '#FFEF5A', '#FFF7AE', '#FFF3D6'],
        red: ['#FF6C40', '#FF8F6D', '#FFBD98', '#FFF2D4']
    };

    const palette = {
        common: { black: '#000', white: '#F8F8FF' },
        primary: { ...PRIMARY },
        secondary: { ...SECONDARY },
        info: { ...INFO },
        success: { ...SUCCESS },
        widgetColor: '#1a1a1a',
        widgetItemColor: '#262626',
        widgetItemTextColor: '#a6a6a6',
        diaryDetailsBackground: '#222222',
        warning: { ...WARNING },
        error: { ...ERROR },
        grey: GREY,
        widgetBackground: '',
        gradients: GRADIENTS,
        chart: CHART_COLORS,
        divider: GREY[500_24],
        text: { primary: GREY[800], secondary: GREY[600], disabled: GREY[500] },
        background: { paper: '#fff', default: '#fff', neutral: GREY[200] },
        action: {
            active: GREY[600],
            hover: GREY[500_8],
            selected: GREY[500_16],
            disabled: GREY[500_80],
            disabledBackground: GREY[500_24],
            focus: GREY[500_24],
            hoverOpacity: 0.08,
            disabledOpacity: 0.48
        }
    };

    return palette;
};
