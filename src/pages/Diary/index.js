import React from "react";
import Page from "../../components/Page";
import DiaryContainer from "./Diary2/DiaryContainer";

const Diary = () => (
    <Page title="My Dashboard">
        <DiaryContainer/>
    </Page>
);

export default Diary;
