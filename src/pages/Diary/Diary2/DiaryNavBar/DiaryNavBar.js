import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import {useTheme} from "@mui/styles";
import CustomDrawer from "./CustomDrawer";

export const drawerWidth = 225;

function DiaryNavBar({
    children, diaries,
    isLoadingDiaries, selectedNavItemId,
    onNavItemClick, onAddDiaryClick
}) {
    const theme = useTheme();

    return (
        <Box sx={{ display: 'flex' }}>
            <Box
                sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 }, zIndex: 0 }}
            >
                <Drawer
                    variant="permanent"
                    sx={{
                        display: { xs: 'none', sm: 'block'},
                        '& .MuiDrawer-paper': {
                            paddingTop: 4,
                            boxSizing: 'border-box', width: drawerWidth,
                            backgroundColor: theme.palette.primary.darker,
                            border: 'none',
                            color: 'white'
                        },
                    }}
                    open
                >
                    <CustomDrawer
                        diaries={diaries}
                        isLoadingDiaries={isLoadingDiaries}
                        onNavItemClick={onNavItemClick}
                        selectedNavItemId={selectedNavItemId}
                        onAddDiaryClick={onAddDiaryClick}
                    />
                </Drawer>
            </Box>
            {children}
        </Box>
    );
}

DiaryNavBar.propTypes = {
    window: PropTypes.func,
};

export default DiaryNavBar;
