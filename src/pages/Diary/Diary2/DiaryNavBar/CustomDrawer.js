import React from "react";
import Toolbar from "@mui/material/Toolbar";
import BookIcon from '@mui/icons-material/Book';
import TreeView from '@mui/lab/TreeView';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import StyledNavigationTreeItem from "./StyledNavigationTreeItem";
import HomeIcon from '@mui/icons-material/Home';
import CollectionsBookmarkIcon from '@mui/icons-material/CollectionsBookmark';
import StarIcon from '@mui/icons-material/Star';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import Loader from "../../../../components/Loader";

const CustomDrawer = ({
    onNavItemClick,
    diaries, selectedNavItemId,
    isLoadingDiaries, onAddDiaryClick
}) => {

    const handleAddDiaryClick = (e) => {
        e.stopPropagation();
        onAddDiaryClick();
    }

    return (
        <div>
            <Toolbar style={{marginBottom: 15}}/>
            <TreeView
                defaultSelected="1"
                onNodeSelect={(e, id) => onNavItemClick(id)}
                selected={selectedNavItemId}
                defaultCollapseIcon={<ArrowDropDownIcon />}
                defaultExpandIcon={<ArrowRightIcon />}
                defaultEndIcon={<div style={{ width: 24 }} />}
                sx={{ height: '85vh', flexGrow: 1, overflowY: 'auto'}}
            >
                <StyledNavigationTreeItem nodeId="1" labelText="Home" labelIcon={HomeIcon} />
                {/*<StyledNavigationTreeItem nodeId="2" labelText="Favourites" labelIcon={StarIcon} />*/}
                <StyledNavigationTreeItem
                    nodeId="3"
                    labelText="Diaries"
                    labelIcon={CollectionsBookmarkIcon}
                    labelInfo={AddCircleIcon}
                    onLabelInfoClick={handleAddDiaryClick}
                >
                    {
                        isLoadingDiaries ?
                            <Loader/>
                            :
                            diaries.map(({name, id}) => (
                                <StyledNavigationTreeItem
                                    key={id}
                                    nodeId={id}
                                    labelText={name.toUpperCase()}
                                    labelIcon={BookIcon}
                                />
                            ))
                    }
                </StyledNavigationTreeItem>
            </TreeView>
        </div>
    );
};

export default CustomDrawer;
