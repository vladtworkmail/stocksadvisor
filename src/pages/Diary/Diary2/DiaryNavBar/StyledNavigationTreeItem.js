import React from "react";
import {Box, Typography} from "@mui/material";
import TreeItem, {treeItemClasses} from "@mui/lab/TreeItem";
import { styled } from '@mui/material/styles';
import PropTypes from "prop-types";

const StyledTreeItemRoot = styled(TreeItem)(({ theme }) => ({
    color: theme.palette.common.white,
    [`& .${treeItemClasses.content}`]: {
        paddingRight: theme.spacing(1),
        fontWeight: 500,
        '&:hover': {
            backgroundColor: theme.palette.action.hover,
        },
        '&.Mui-focused, &.Mui-selected, &.Mui-selected.Mui-focused': {
            backgroundColor: `var(--tree-view-bg-color, ${theme.palette.action.selected})`,
            color: 'var(--tree-view-color)',
        },

    },
    [`& .${treeItemClasses.group}`]: {
        marginLeft: 0,
        [`& .${treeItemClasses.content}`]: {
            paddingLeft: theme.spacing(2.5),
        },
    },
}));

const StyledNavigationTreeItem = (props) => {
    const {
        bgColor,
        color,
        labelIcon: LabelIcon,
        labelInfo,
        labelText,
        onLabelInfoClick,
        ...other
    } = props;

    return (
        <StyledTreeItemRoot
            label={
                <Box sx={{ display: 'flex', alignItems: 'center', p: 0.5, pr: 0 }}>
                    <Box component={LabelIcon} color="inherit" sx={{ mr: 1 }} />
                    <Typography variant="body2" sx={{ fontWeight: 'inherit', flexGrow: 1 }}>
                        {labelText}
                    </Typography>
                    {
                        typeof labelInfo === 'string' ?
                            (
                                <Typography variant="caption" color="inherit">
                                    {labelInfo}
                                </Typography>
                            )
                            :
                            (<Box component={labelInfo} color="inherit" sx={{ mr: 0, fontSize: 19 }} onClick={onLabelInfoClick} />)
                    }
                </Box>
            }
            style={{
                '--tree-view-color': color,
                '--tree-view-bg-color': bgColor,
            }}
            {...other}
        />
    );
};

StyledNavigationTreeItem.propTypes = {
    bgColor: PropTypes.string,
    color: PropTypes.string,
    labelIcon: PropTypes.elementType.isRequired,
    labelInfo: PropTypes.oneOfType([PropTypes.elementType, PropTypes.string]),
    labelText: PropTypes.string.isRequired,
};

export default StyledNavigationTreeItem;
