import React, {useState} from "react";
import DiaryNavBar from "./DiaryNavBar/DiaryNavBar";
import useFetchDiaries from "../../../hook/useFetchDiaries";
import DiaryHome from "./DiaryHome/DiaryHomeContainer";
import useCreateDiary from "../../../hook/useCreateDiary";
import CreateDiaryModal from "../../../modals/CreateDiaryModal";
import DiaryDetailPage from "./DiaryDetailPage";

const DiaryContainer = () => {
    const {diaries, isLoadingDiaries} = useFetchDiaries();
    const defaultSelectedNavItemId = '1';
    const [selectedNavItemId, setSelectedNavItemId] = useState(defaultSelectedNavItemId);
    const [createDiaryModalOpen, setCreateDiaryModalOpen] = useState(false);
    const { creationDiaryLoading, createDiary } = useCreateDiary();

    const handleNavItemClick = (id) => setSelectedNavItemId(id);

    const handleCreateDiaryModalToggle = () => setCreateDiaryModalOpen(old => !old);

    const getSelectedDiaryName = () => diaries.find(({id}) => id === selectedNavItemId)?.name || '';

    const handleChooseOtherDiary = (prevDiaryId) => {
        const prevDiaryIndex = diaries.findIndex(({ id }) => id === prevDiaryId);
        const nextSelectedDiaryIndex = prevDiaryIndex - 1;
        const isNextIndexExist = nextSelectedDiaryIndex >= 0 && nextSelectedDiaryIndex < diaries.length;
        const nextSelectedDiaryId = isNextIndexExist ? diaries[nextSelectedDiaryIndex].id : defaultSelectedNavItemId;
        setSelectedNavItemId(nextSelectedDiaryId);
    };

    const showContent = () => {
        switch (selectedNavItemId) {
            case '1':
            case '2':
            case '3':
                return <DiaryHome/>;
            default:
                return (
                    <DiaryDetailPage
                        diaryId={selectedNavItemId}
                        diaryName={getSelectedDiaryName()}
                        onChooseOtherDiary={handleChooseOtherDiary}
                    />
                );
        }
    };

    const handleDiaryCreate = (diaryName) => {
        createDiary(diaryName);
    };

    return (
        <DiaryNavBar
            diaries={diaries}
            isLoadingDiaries={isLoadingDiaries}
            selectedNavItemId={selectedNavItemId}
            onNavItemClick={handleNavItemClick}
            onAddDiaryClick={handleCreateDiaryModalToggle}
        >
            {showContent()}
            <CreateDiaryModal
                isLoading={creationDiaryLoading}
                open={createDiaryModalOpen}
                onClose={handleCreateDiaryModalToggle}
                onDiaryCreate={handleDiaryCreate}
            />
        </DiaryNavBar>
    )
};

export default DiaryContainer;
