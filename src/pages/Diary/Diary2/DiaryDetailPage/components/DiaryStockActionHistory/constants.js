export const cols = [
    { label: 'Symbol', name: 'symbol', options: { sort: true, sortThirdClickReset: true } },
    { label: 'Price', name: 'price', options: { sort: true, sortThirdClickReset: true } },
    { label: 'Qty', name: 'quantity', options: { sort: true, sortThirdClickReset: true } },
    { label: 'Date', name: 'date', options: { sort: true, sortThirdClickReset: true } },
    { label: 'Action type', name: 'type', options: { sort: true, sortThirdClickReset: true } },
    { label: 'Gains', name: 'gains', options: { sort: true, sortThirdClickReset: true } },
];

export const tableOptions = {
    expandableRows: false,
    selectableRows: false,
    filter: false,
    viewColumns: false
};
