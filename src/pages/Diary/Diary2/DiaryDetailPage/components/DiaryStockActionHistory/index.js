import React from "react";
import MUIDataTable from 'mui-datatables';
import {cols, tableOptions} from "./constants";
import moment from "moment";
import {styled} from "@mui/material/styles";
import {Box} from "@mui/material";
///////////
const Container = styled(Box)(({ theme }) => ({
    marginBottom: 5,
    marginTop: 5,
}));
//////////
const DiaryStockActionHistory = ({ actionHistory, symbol, diaryName }) => {
    const data = actionHistory.map(action => ({
        ...action,
        date: moment(action.date).format('DD/MM/YYYY'),
        symbol
    }));
    return (
        <Container>
            <MUIDataTable
                columns={cols}
                data={data}
                options={{
                    ...tableOptions,
                    downloadOptions: { filename: `StocksAdvisor__${diaryName}_${symbol}_history` }
                }}
            />
        </Container>
    );
};

export default DiaryStockActionHistory;
