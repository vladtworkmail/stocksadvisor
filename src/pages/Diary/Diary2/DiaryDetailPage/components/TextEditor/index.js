import React from "react";
import { RichTextEditor } from '@mantine/rte';
import {styled} from "@mui/material/styles";
import {Box, Button} from "@mui/material";
///////////
const Container = styled(Box)(({ theme }) => ({
    marginBottom: 5,
    marginTop: 5,
    display: 'flex',
    flexDirection: 'column',
    gap: 10,
}));
//////////
const TextEditor = ({id, value, onChange, onSaveNote}) => {
    return (
        <Container id={id}>
            <RichTextEditor
                controls={[
                    ['bold', 'italic', 'underline', 'link', 'image'],
                    ['unorderedList', 'h1', 'h2', 'h3'],
                    ['alignLeft', 'alignCenter', 'alignRight'],
                ]}
                value={value}
                placeholder="Write your notes here..."
                onChange={onChange}
            />
            <Button
                variant="contained"
                color="info"
                onClick={onSaveNote}
            >
                Save note
            </Button>
        </Container>
    );
};

export default TextEditor;
