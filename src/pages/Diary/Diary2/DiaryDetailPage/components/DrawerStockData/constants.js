export const customButtonsProps = [
    {
        label: '5D',
        value: '5dm'
    },
    {
        label: '1M',
        value: '1m'
    },
    {
        label: '3M',
        value: '2q'
    },
    {
        label: '1Y',
        value: '1y'
    },
];
