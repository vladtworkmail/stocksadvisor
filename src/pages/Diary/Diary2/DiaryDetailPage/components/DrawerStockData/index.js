import React, {useState} from "react";
import {Box, Typography} from "@mui/material";
import {useLocation} from "react-router-dom";
import queryString from "query-string";
import {styled} from "@mui/material/styles";
import useFetchDiaryStockIncomeAnalysis from "../../../../../../hook/useFetchDiaryStockIncomeAnalysis";
import useFetchStockChart from "../../../../../../hook/useFetchStockChart";
import useFetchStockFundamentals from "../../../../../../hook/useFetchStockFundamentals";
import useFetchStockDetails from "../../../../../../hook/useFetchStockDetails";
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import HeaderCard from "./HeaderCard";
import AnanlysisCard from "./AnanlysisCard";
import Chart from "../../../../../../components/Chart";
import {customButtonsProps} from "./constants";
import TableInfo from "../../../../../../components/StockSummary/components/TableInfo";
import DiaryStockActionHistory from "../DiaryStockActionHistory";
import TextEditor from "../TextEditor";
import WidgetItem from "../../../../../../components/WidgetItem/WidgetItem";
import TradingViewWidget from 'react-tradingview-widget';
import moment from "moment";
/////////
const Container = styled(Box)(({ theme }) => ({
    padding: 20,
    paddingBottom: 40,
    maxWidth: 600,
    backgroundColor: theme.palette.grey[200],
    ".MuiPaper-root": {
        backgroundColor: 'white',
        color: theme.palette.common.black,
        "td": {
            color: theme.palette.common.black,
            fontWeight: 500,
            fontSize: '0.73em'
        }
    }
}));
const Icon = styled(Box)(({ theme }) => ({
    fontSize: 18,
    '&:hover': {
        cursor: 'pointer'
    }
}));
const NotesContainer = styled(Box)(({ theme }) => ({
    display: 'flex',

    gap: 20,
    flexWrap: 'wrap'
}));
const BlockHeaderText = styled(Typography)(({ theme }) => ({
    marginTop: 10,
    marginBottom: 10,
    textTransform: 'uppercase',
    color: theme.palette.grey[700]
}));
/////////
const DrawerStockData = ({
    onChangeNote, noteValue,
    stocks, onClose,
    onChangeQuantityClick, onSaveNote,
    diaryName
}) => {
    const location = useLocation();
    const [makeNoteOpen, setMakeNoteOpen] = useState(false);
    const {
        query: {
            symbol,
            id,
            value
        }
    } = queryString.parseUrl(location.search);
    const openedStock = stocks.find((stock) => stock.symbol === symbol) || {};
    const {notes = []} = openedStock;

    const {
        isLoadingDiaryStockIncomeAnalysis,
        diaryStockIncomeAnalysis,
    } = useFetchDiaryStockIncomeAnalysis(id);
    const [selectedChartFilterValue, setSelectedChartFilterValue] = useState('1m');
    const {chartData } = useFetchStockChart(symbol, selectedChartFilterValue);
    const {fundamentals, isLoadingFundamentals} = useFetchStockFundamentals(symbol);
    const {
        fullQuote,
        isLoadingDetail
    } = useFetchStockDetails(symbol);

    const handleChartFilter = value => setSelectedChartFilterValue(value);
    const toggleMakeNoteClick = () => setMakeNoteOpen(old => !old);

    const handleSaveNote = () => {
        setMakeNoteOpen(false);
        onSaveNote();
    }

    return (
        <Container>
            <Icon component={ArrowBackIosIcon} onClick={onClose} />
            <HeaderCard
                stock={openedStock}
                onMakeNoteClick={toggleMakeNoteClick}
                symbol={symbol}
                makeNoteOpen={makeNoteOpen}
                onChangeQuantityClick={onChangeQuantityClick}
                companyName={fullQuote?.companyName}
                change={fullQuote?.change}
                marketCap={fullQuote?.marketCap}
            />
            {
                makeNoteOpen &&
                (
                    <>
                        <BlockHeaderText variant="overline">Make note</BlockHeaderText>
                        <TextEditor
                            value={noteValue}
                            onChange={onChangeNote}
                            onSaveNote={handleSaveNote}
                            id="drawerTextEditor"
                        />
                    </>
                )
            }
            {
                !!notes.length &&
                (
                   <>
                       <BlockHeaderText variant="overline">Your notes</BlockHeaderText>
                       <NotesContainer>
                           {
                               notes?.map(({id, lastUpdated, notes}) => (
                                   <WidgetItem
                                       key={id}
                                       styles={{marginTop: 5}}
                                       content={notes}
                                       date={moment(lastUpdated).format('DD/MM/YYYY')}
                                   />
                               ))
                           }
                       </NotesContainer>
                   </>
                )
            }
            <BlockHeaderText variant="overline">Stock analysis</BlockHeaderText>
            <AnanlysisCard
                value={value}
                isLoadingDiaryStockIncomeAnalysis={isLoadingDiaryStockIncomeAnalysis}
                diaryStockIncomeAnalysis={diaryStockIncomeAnalysis}
                currency={fullQuote?.currency}
            />
            <BlockHeaderText variant="overline">Chart</BlockHeaderText>
            <Chart
                symbol={symbol}
                chart={chartData}
                showChartButtons
                styles={{marginTop: 5, marginBottom: 5}}
                customButtonsProps={customButtonsProps}
                selectedFilterValue={selectedChartFilterValue}
                onChartButtonClick={handleChartFilter}
            />
            <BlockHeaderText variant="overline">Fundamentals</BlockHeaderText>
            <TableInfo
                stockDetails={fundamentals}
                fullQuote={fullQuote}
                shortenVersion
                styles={{marginTop: 5, marginBottom: 5}}
                isLoadingDetail={isLoadingFundamentals || isLoadingDetail}
            />
            <BlockHeaderText variant="overline">Action history</BlockHeaderText>
            <DiaryStockActionHistory
                actionHistory={openedStock?.actions || []}
                symbol={symbol}
                diaryName={diaryName}
            />
        </Container>
    );
};

export default DrawerStockData;
