import React from "react";
import {Box, Button, Card, Typography} from "@mui/material";
import {fShortenNumber} from "../../../../../../utils";
import {styled} from "@mui/material/styles";
import {useTheme} from "@mui/styles";
////////
const TitleContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginBottom: 5,
    gap: 30
}));
const TitleText = styled(Typography)(({ theme }) => ({
    maxWidth: 200,
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
}));
export const DiaryDrawerCustomCard = styled(Card)(({ theme }) => ({
    paddingTop: 10,
    paddingRight: 20,
    paddingLeft: 20,
    paddingBottom: 10,
    marginBottom: 5,
    marginTop: 5,
}));
////////
const HeaderCard = ({
    stock, symbol, companyName,
    marketCap, change,
    onChangeQuantityClick, onMakeNoteClick,
    makeNoteOpen
}) => {
    const theme = useTheme();
  return (
      <DiaryDrawerCustomCard>
          <TitleContainer>
              <TitleText variant="h6">{symbol} - {companyName}</TitleText>
          </TitleContainer>
          <TitleContainer>
              <Typography variant="caption">
                  Change: <b>{change}</b>
              </Typography>
              <Typography variant="caption">
                  Market Cap: <b>{fShortenNumber(marketCap)}</b>
              </Typography>
          </TitleContainer>
          <TitleContainer style={{gap: 10, marginTop: 15}}>
              <Button
                  variant="contained"
                  style={{
                      borderRadius: 3,
                      borderColor: theme.palette.success.main,
                      color: theme.palette.common.white,
                      backgroundColor: theme.palette.success.main
                  }}
                  onClick={() => onChangeQuantityClick('Buy', stock)}
              >
                  Buy
              </Button>
              <Button
                  variant="contained"
                  style={{
                      borderRadius: 3,
                      borderColor: theme.palette.primary.main,
                      color: theme.palette.common.white,
                      backgroundColor: theme.palette.primary.main
                  }}
                  onClick={() => onChangeQuantityClick('Sell', stock)}
              >
                  Sell
              </Button>
          <Button
              variant="outlined"
              color="info"
              style={{ borderRadius: 3 }}
              onClick={onMakeNoteClick}
          >
              {`${makeNoteOpen ? 'Close' : 'Make'} note`}
          </Button>
          </TitleContainer>
      </DiaryDrawerCustomCard>
  );
};

export default HeaderCard;
