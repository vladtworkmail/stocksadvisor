import React from "react";
import {Grid} from "@mui/material";
import AnalysisStockList from "../AnalysisStockList/AnalysisStockList";
import {DiaryDrawerCustomCard} from "./HeaderCard";
import {leftListData, rightListData} from "../AnalysisStockList/constants";
///////
const AnalysisCard = ({diaryStockIncomeAnalysis, currency, value, isLoadingDiaryStockIncomeAnalysis}) => (
    <DiaryDrawerCustomCard>
        <Grid container>
            <Grid item xs={6} md={6} lg={6}>
                <AnalysisStockList
                    data={diaryStockIncomeAnalysis}
                    rows={leftListData}
                    isLoading={isLoadingDiaryStockIncomeAnalysis}
                    currency={currency}
                />
            </Grid>
            <Grid item xs={6} md={6} lg={6}>
                <AnalysisStockList
                    value={value}
                    data={diaryStockIncomeAnalysis}
                    rows={rightListData}
                    isLoading={isLoadingDiaryStockIncomeAnalysis}
                />
            </Grid>
        </Grid>
    </DiaryDrawerCustomCard>
);

export default AnalysisCard;
