import React from "react";
import {Card, CardContent, CardHeader, Grid} from "@mui/material";
import AnalysisStockList from "../AnalysisStockList/AnalysisStockList";
import {leftListData, rightListData} from "../AnalysisStockList/constants";

const DiaryAnalysis = ({value, currency, diaryIncomeAnalysis, isLoadingDiaryIncomeAnalysis}) => {
    return (
        <Card>
            <CardHeader title="Diary Analysis" />
            <CardContent>
                <Grid container>
                    <Grid item lg={5} md={5} sm={6}>
                        <AnalysisStockList
                            data={diaryIncomeAnalysis}
                            rows={leftListData}
                            isLoading={isLoadingDiaryIncomeAnalysis}
                            currency={currency}
                            currencyStyles={{fontSize: '0.9em'}}
                            labelStyles={{fontSize: '0.9em'}}
                        />
                    </Grid>
                    <Grid item lg={7} md={7} sm={7}>
                        <AnalysisStockList
                            value={value}
                            data={diaryIncomeAnalysis}
                            rows={rightListData}
                            isLoading={isLoadingDiaryIncomeAnalysis}
                            labelStyles={{fontSize: '0.9em'}}
                        />
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    );
};

export default DiaryAnalysis;
