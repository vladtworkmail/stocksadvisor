import React from "react";
import {Box, Button, Typography} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import {styled} from "@mui/material/styles";
///////
const Container = styled(Box)(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    gap: 1,
    justifyContent: 'center',
    height: 440,
}));

const EmptyPortfolioState = ({onAddSymbolClick}) => (
    <Container>
        <Typography variant='subtitle' sx={{opacity: 0.70, marginBottom: 1}}>
            Add first symbol to your portfolio
        </Typography>
        <Button
            size="small"
            startIcon={<AddIcon/>}
            variant="contained"
            onClick={onAddSymbolClick}
        >
            Add Symbol
        </Button>
    </Container>
);

export default EmptyPortfolioState;
