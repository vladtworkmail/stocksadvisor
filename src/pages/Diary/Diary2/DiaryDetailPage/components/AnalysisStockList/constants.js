export const leftListData = [
    {
        label: 'Gains',
        value: 'gains',
        tooltipText: 'Realised gains / loses'
    },
    {
        label: 'Dividends',
        value: 'dividends',
        tooltipText: 'Received dividends'
    },
];

export const rightListData = [
    {
        label: 'Unrealized gains',
        value: 'unrealizedGainsBase',
        tooltipText: 'Unrealised gains / loses'
    },
    {
        label: 'Sales price',
        value: 'salesPrice',
        tooltipText: 'Total amount from sales'
    },
    {
        label: 'Cost of investments',
        value: 'costOfInvestments',
        tooltipText: 'Pure adjusted cost base'
    },
];

