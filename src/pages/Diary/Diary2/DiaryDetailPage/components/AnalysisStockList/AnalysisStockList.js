import {List, ListItem, ListItemText, Tooltip, Typography} from "@mui/material";
import {styled} from "@mui/material/styles";
/////////
const CustomListItemText = styled(ListItemText)(({theme}) => ({
    '&:hover': {
        cursor: 'pointer'
    },
    'span': {
        color: theme.palette.common.black
    }
}));

const AnalysisStockList = ({
    value: generalValue, rows,
    data, styles, isLoading,
    currency, currencyStyles, labelStyles
}) => (
    <List style={{...styles, maxWidth: 'fit-content'}}>
        {
            currency &&
            (
                <ListItem>
                    <CustomListItemText>
                        <Typography variant="body2" style={{fontSize: '.73em', ...currencyStyles}}>
                            <span>Currency:</span> <b>{currency}</b>
                        </Typography>
                    </CustomListItemText>
                </ListItem>
            )
        }
        {
            rows.map(({label, value, tooltipText}) => (
                <ListItem key={label}>
                    <Tooltip title={tooltipText} placement="right">
                        <CustomListItemText>
                            <Typography variant="body2" style={{fontSize: '.73em', ...labelStyles}}>
                                <span>{label}:</span>
                                <b>
                                    {
                                        value === 'unrealizedGainsBase' ?
                                            ` ${isLoading ? 'Loading...' : (+data[value] + parseFloat(generalValue))?.toFixed(2)}`
                                            :
                                            ` ${isLoading ? 'Loading...' : data[value]?.toFixed(2)}`
                                    }
                                </b>
                            </Typography>
                        </CustomListItemText>
                    </Tooltip>
                </ListItem>
            ))
        }
    </List>
);

export default AnalysisStockList;
