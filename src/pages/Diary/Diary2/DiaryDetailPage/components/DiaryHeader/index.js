import React, {useState} from "react";
import {Box, Typography, Divider, MenuItem} from "@mui/material";
import {styled} from "@mui/material/styles";
import BookIcon from '@mui/icons-material/Book';
import EditIcon from '@mui/icons-material/Edit';
import CloseIcon from '@mui/icons-material/Close';
import CheckIcon from '@mui/icons-material/Check';
import {useTheme} from "@mui/styles";
import InputBase from "@mui/material/InputBase";
import MoreVertIcon from '@mui/icons-material/MoreVert';
import MenuPopover from "../../../../../../components/MenuPopover";
import DeleteIcon from '@mui/icons-material/Delete';
////////
const Container = styled(Box)(({ theme }) => ({
    display: 'flex',
    gap: 10,
    justifyContent: 'space-between'
}));
const TitleContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'flex-start',
    gap: 4,
}));
const EditContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center'
}));
const SubtitleContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
}));
const TitleText = styled(Typography)(({ theme }) => ({
    fontSize: '1.6em',
    paddingTop: 3,
    lineHeight: 1
}));
const SubtitleText = styled(Typography)(({ theme }) => ({
    fontSize: '0.75em',
    paddingTop: 9,
    lineHeight: 1,
    color: theme.palette.widgetItemTextColor,
}));
const Icon = styled(Box)(({ theme }) => ({
    fontSize: 16,
    marginRight: 3,
    color: theme.palette.widgetItemTextColor,
    '&:hover': {
        cursor: 'pointer'
    }
}));
const StyledInputBase = styled(InputBase)(({ theme }) => ({
    width: 'inherit',
    color: 'white',
    marginTop: 4,
    height: 30,
    backgroundColor: theme.palette.common.black,
    borderRadius: 4,
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        paddingLeft: 10,
        transition: theme.transitions.create('width'),
        width: '100%'
    }
}));

const DiaryHeader = ({
    diaryName, isTitleEditing, stocksQuantity,
    onChangeDiaryTitle, onDiaryEditModeOn,
    onDiaryEditModeOff, diaryTitleValue,
    onApproveNewDiaryName, diaryId, onDeleteDiary
}) => {
    const theme = useTheme();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [isDiaryMenuOpen, setIsDiaryMenuOpen] = useState(false);
    const handleMenuClose = () => {
        setAnchorEl(null);
        setIsDiaryMenuOpen(false);
    };

    const handleMenuOpen = (e) => {
        setAnchorEl(e.currentTarget);
        setIsDiaryMenuOpen(true);
    };

    const handleDeleteDiary = () => {
        handleMenuClose();
        onDeleteDiary();
    };

    return (
        <Container>
            <TitleContainer>
                <BookIcon style={{fontSize: 60}}/>
                <Box>
                    <EditContainer>
                        {
                            isTitleEditing ?
                                (
                                    <StyledInputBase
                                        placeholder='Search…'
                                        inputProps={{ 'aria-label': 'search' }}
                                        onChange={e => onChangeDiaryTitle(e.target.value)}
                                        value={diaryTitleValue}
                                        autoFocus
                                    />
                                )
                                :
                                <TitleText>{diaryName.toUpperCase()}</TitleText>
                        }
                        {
                            isTitleEditing ?
                                <>
                                    <Icon
                                        component={CloseIcon}
                                        onClick={onDiaryEditModeOff}
                                        style={{ color: theme.palette.primary.main, fontSize: 18, marginTop: 2, marginLeft: 5}}
                                    />
                                    <Icon
                                        component={CheckIcon}
                                        style={{ color: theme.palette.success.main, fontSize: 18}}
                                        onClick={onApproveNewDiaryName}
                                    />
                                </>
                                :
                                <Icon
                                    component={EditIcon}
                                    style={{ marginLeft: 10}}
                                    onClick={onDiaryEditModeOn}
                                />
                        }
                    </EditContainer>
                    <SubtitleContainer>
                        <SubtitleText>Stocks: {stocksQuantity}</SubtitleText>
                    </SubtitleContainer>
                </Box>
            </TitleContainer>
            <MoreVertIcon onClick={handleMenuOpen}/>
            <MenuPopover
                open={isDiaryMenuOpen}
                onClose={handleMenuClose}
                anchorEl={anchorEl}
                sx={{ width: 220 }}
            >
                <Box sx={{ my: 1.5, px: 2.5 }}>
                    <Typography variant='subtitle2' noWrap>
                        Name: {diaryName}
                    </Typography>
                    <Typography variant='caption' sx={{ color: 'text.secondary' }}>
                        Id: {diaryId}
                    </Typography>
                </Box>
                <Divider sx={{ my: 1 }} />
                <MenuItem
                    onClick={handleDeleteDiary}
                    sx={{ typography: 'body2', py: 1, px: 2.5 }}
                >
                    <Icon
                        component={DeleteIcon}
                        style={{fontSize: 24, color: theme.palette.common.black}}
                    />
                    Delete diary
                </MenuItem>
            </MenuPopover>
        </Container>
    );
};

export default DiaryHeader;
