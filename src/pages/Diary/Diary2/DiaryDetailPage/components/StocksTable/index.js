import React, {useEffect, useState} from "react";
import {
    Box, Typography,
    TableHead, Table,
    TableContainer, TableRow,
    TableBody, Button,
    TableCell, Divider, Grid
} from "@mui/material";
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import AddIcon from '@mui/icons-material/Add';
import Loader from "../../../../../../components/Loader";
import MUIDataTable from 'mui-datatables';
import IconButton from '@mui/material/IconButton';
import {cols, tableOptions} from "./constants";
import {useTheme} from "@mui/styles";
import formatDiaryStocks from "../../../../../../formatters/diaryStocksData";

const StocksTable = ({stocks, isLoading, shortQuotesList, onStocksDeleteClick}) => {
    const [selectableRows, setSelectableRows] = useState(false);
    const theme = useTheme();
    const formattedStocks = formatDiaryStocks(stocks, shortQuotesList);

    const handleRowsSelectableOn = () => setSelectableRows(old => !old);

    const handleRowsDelete = (data) => {
        const stockIds = data.map(({index}) => ({
            diaryStockId: stocks[index].id,
            companyName: stocks[index].companyName
        }));
        onStocksDeleteClick(stockIds);
    };

    const modifiedTableOptions = {
        ...tableOptions,
        selectableRows: selectableRows ? 'multiple' : 'none',
        renderExpandableRow: (rowData, rowMeta) => {
            const colSpan = rowData.length + 1;
            return (
                <TableRow>
                    <TableCell colSpan={colSpan}>Empty for now...</TableCell>
                </TableRow>
            );
        },
        onRowsDelete: ({data}) => {
            handleRowsDelete(data);
            return false; // don't delete rows locally
        },
        customToolbar: () => (
                <IconButton onClick={handleRowsSelectableOn}>
                    <CheckBoxIcon/>
                </IconButton>
        )
    };

    if (isLoading) {
        return <Loader size={60} style={{marginTop: 20}}/>;
    }

    return (
        <Box style={{marginTop: 40}}>
            <MUIDataTable
                columns={cols}
                data={formattedStocks}
                title={
                        <Button
                            variant="outlined"
                            size="small"
                            style={{borderColor: theme.palette.success.main, color: theme.palette.success.main}}
                            color="success"
                            startIcon={<AddIcon style={{fontSize: 24}}/>}
                        >
                            Symbol
                        </Button>
                }
                options={modifiedTableOptions}
            />
        </Box>
    )
};

export default StocksTable;
