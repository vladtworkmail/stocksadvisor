export const cols = [
    { label: 'Name', name: 'companyName', options: { sort: true, sortThirdClickReset: true } },
    { label: 'Symbol', name: 'symbol', options: { sort: true, sortThirdClickReset: true } },
    { label: 'Qty', name: 'quantity', options: { sort: true, sortThirdClickReset: true } },
    { label: 'Value', name: 'value', options: { sort: true, sortThirdClickReset: true } },
    { label: 'Purchase price', name: 'averageBuyingPrice', options: { sort: true, sortThirdClickReset: true } },
    { label: 'Market Price', name: 'latestPrice', options: { sort: true, sortThirdClickReset: true } },
    { label: 'Profit', name: 'profit', options: { sort: true, sortThirdClickReset: true } },
    { label: 'Change', name: 'change', options: { sort: true, sortThirdClickReset: true } },
    { label: 'Change %', name: 'changePercent', options: { sort: true, sortThirdClickReset: true } },
];

export const tableOptions = {
    expandableRows: true,
    fixedHeader: true,
    isRowExpandable: (dataIndex, expandedRows) => {
        console.log(dataIndex, expandedRows)
        return true;
    },
};
