import React, {useEffect, useState} from "react";
import {alpha, styled} from "@mui/material/styles";
import {Box, Card} from "@mui/material";
import DiaryHeader from "./components/DiaryHeader";
import useUpdateDiaryName from "../../../../hook/useUpdateDiaryName";
import {ImageContainer} from "../DiaryHome/DiaryHomeContainer";
import DiaryHomeBgImage from "../../../../assets/diaryBack.jpg";
import useDeleteDiary from "../../../../hook/useDeleteDiary";
import RemoveDiaryModal from "../../../../modals/RemoveDiaryModal";
import EmptyPortfolioState from "./components/EmptyPortfolioState";
import ActionSymbolDialog from "../../../../modals/ActionSymbolsDialog";
import useFetchDiaryStocks from "../../../../hook/useFetchDiaryStocks";
import Loader from "../../../../components/Loader";
import useCreateDiaryStock from "../../../../hook/useCreateDiaryStock";
import StocksTable from "./components/StocksTable";
import fetchShortQuote from "../../../../queries/fetchShortQuote";
import RemoveDiaryStocksModal from "../../../../modals/RemoveDiaryStocks";
import useDeleteDiaryStock from "../../../../hook/useDeleteDiaryStock";

////////
const Container = styled(Card)(({ theme }) => ({
    backgroundColor: alpha(theme.palette.diaryDetailsBackground, 0.89),
    color: theme.palette.common.white,
    position: 'relative',
    marginTop: -250,
    marginLeft: 24,
    marginRight: 24,
    padding: 24,
    minHeight: 600,
    boxShadow: 'none'
}));

const DiaryDetailPage = ({diaryId, diaryName,  onChooseOtherDiary}) => {
    const {mutate: updateDiaryTitle} = useUpdateDiaryName();
    const {mutate: deleteDiary} = useDeleteDiary();
    const {stocks, isLoadingDiaryStocks} = useFetchDiaryStocks(diaryId);
    const { deleteDiaryStock } = useDeleteDiaryStock();
    const [shortQuotesList, setShortQuotesList] = useState([]);
    const { creationDiaryStockLoading, createDiaryStock } = useCreateDiaryStock();
    const [isTitleEditing, setIsTitleEditing] = useState(false);
    const [isRemoveDiaryModalOpen, setIsRemoveDiaryModalOpen] = useState(false);
    const [isAddSymbolDialogOpen, setIsAddSymbolDialogOpen] = useState(false);
    const [isDeleteStockDialogOpen, setIsDeleteStockDialogOpen] = useState(false);
    const [stocksToDelete, setStocksToDelete] = useState([]);
    const [diaryTitleValue, setDiaryTitleValue] = useState('');

    useEffect(() => {
        if (!stocks.length) {
            return [];
        }
        const fetchAll = () => Promise.all(stocks.map(({symbol}) => {
            const params = {
                queryKey: [null, symbol]
            };
            return fetchShortQuote(params);
        }));

        fetchAll().then(data => setShortQuotesList(data));
    }, [stocks.length]);

    const handleDiaryEditModeOn = () => setIsTitleEditing(true);
    const handleDiaryEditModeOff = () => {
        setIsTitleEditing(false);
        setDiaryTitleValue(diaryName);
    };
    const handleApproveNewDiaryName = () => {
        handleDiaryEditModeOff();
        if (diaryTitleValue === diaryName) {
            return false;
        }
        const params = {
            diaryId,
            diaryTitleValue
        };
        updateDiaryTitle(params);
    };

    useEffect(() => {
        setDiaryTitleValue(diaryName);
    }, [diaryName]);

    const handleChangeDiaryTitle = (value) => setDiaryTitleValue(value);

    const handleRemoveDiaryModalClose = () => setIsRemoveDiaryModalOpen(false);
    const handleRemoveDiaryModalOpen = () => setIsRemoveDiaryModalOpen(true);

    const handleDeleteDiary = () => {
        onChooseOtherDiary(diaryId);
        deleteDiary(diaryId);
        setIsRemoveDiaryModalOpen(false);
    };

    const handleCreateSymbol = (params) => {
        setIsAddSymbolDialogOpen(false);
        const diaryId = params.diaryId;
        params.stocks.forEach(({symbol: stockSymbol, quantity, price, companyName}) => {
            const params = {
                diaryId, stockSymbol,
                quantity, price, companyName
            }
           createDiaryStock(params);
        });
    };

    const handleAddSymbolClick= () => setIsAddSymbolDialogOpen(true);
    const handleActionSymbolsDialogClose= () => setIsAddSymbolDialogOpen(false);

    const handleDeleteStocksModalClose= () => {
        setIsDeleteStockDialogOpen(false);
        setStocksToDelete([]);
    }

    const handleStocksDeleteClick = (stocks) => {
        setStocksToDelete(stocks);
        setIsDeleteStockDialogOpen(true);
    };

    const handleDeleteStocks= () => {
        stocksToDelete.forEach(({diaryStockId}) => deleteDiaryStock(diaryStockId));
        setIsDeleteStockDialogOpen(false);
    };

    const showContent = () => {
        if (isLoadingDiaryStocks) {
            return <Loader/>;
        }
        return stocks.length ?
            (
                <StocksTable
                    stocks={stocks}
                    shortQuotesList={shortQuotesList}
                    isLoading={creationDiaryStockLoading}
                    onStocksDeleteClick={handleStocksDeleteClick}
                />
            )
            :
            (
                <EmptyPortfolioState
                    onAddSymbolClick={handleAddSymbolClick}
                />
            );
    };

    return (
        <Box>
            <ImageContainer backgroundimage={DiaryHomeBgImage}/>
            <Container>
                <DiaryHeader
                    diaryName={diaryName}
                    diaryId={diaryId}
                    diaryTitleValue={diaryTitleValue}
                    isTitleEditing={isTitleEditing}
                    onDeleteDiary={handleRemoveDiaryModalOpen}
                    onChangeDiaryTitle={handleChangeDiaryTitle}
                    onDiaryEditModeOn={handleDiaryEditModeOn}
                    onApproveNewDiaryName={handleApproveNewDiaryName}
                    onDiaryEditModeOff={handleDiaryEditModeOff}
                    stocksQuantity={stocks.length}
                />
                {showContent()}
            </Container>

            <RemoveDiaryModal
                onClose={handleRemoveDiaryModalClose}
                open={isRemoveDiaryModalOpen}
                onDeleteDiary={handleDeleteDiary}
                diaryName={diaryName}
                diaryId={diaryId}
            />
            <ActionSymbolDialog
                isOpenDialog={isAddSymbolDialogOpen}
                title="Add symbols"
                titleList="Added symbols"
                handleClose={handleActionSymbolsDialogClose}
                onSubmitClick={handleCreateSymbol}
                diaryId={diaryId}
            />
            <RemoveDiaryStocksModal
                open={isDeleteStockDialogOpen}
                stocks={stocksToDelete}
                onDeleteStocks={handleDeleteStocks}
                onClose={handleDeleteStocksModalClose}
            />
        </Box>
    )
};

export default DiaryDetailPage;
