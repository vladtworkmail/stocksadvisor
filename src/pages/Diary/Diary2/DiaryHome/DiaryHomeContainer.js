import React from "react";
import {Box} from "@mantine/core";
import Grid from '@mui/material/Grid';
import {styled} from "@mui/material/styles";
import DiaryHomeBgImage from '../../../../assets/diary_home_background.jpg';
import {drawerWidth} from "../DiaryNavBar/DiaryNavBar";
import RecentNotesWidget from "../../../../components/RecentNotesWidget";
import {MarketData, MarketOverview} from "react-tradingview-embed";
import useFetchStocksList from "../../../../hook/useFetchStocksList";

//////////
export const ImageContainer = styled(Box)(({theme, backgroundimage}) => ({
    width: `calc(100vw - ${drawerWidth}px - 9px)`,
    height: 470,
    backgroundImage: `url(${backgroundimage})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: '50% 40%',
    backgroundSize: 'cover',
    filter: "blur(1.2px)",
}));

const Container = styled(Grid)(({theme}) => ({
    position: 'relative',
    padding: 24,
    marginTop: -400,
}));
const ElementContainer = styled(Grid)(({theme}) => ({
    paddingTop: theme.spacing(20)
}));

const DiaryHomeContainer = () => {
    const {list} = useFetchStocksList();
    return (
        <Box>
            <ImageContainer backgroundimage={DiaryHomeBgImage}/>
            <Container container>
                <Grid item xs={12} md={12} lg={8} sm={12}>
                    <ElementContainer>
                        <MarketData
                            widgetProps={{
                                symbolsGroups: [
                                    {
                                        name: 'Trending stocks',
                                        originalName: 'Trending stocks',
                                        symbols: list.map(({symbol, companyName}) => ({
                                            name: symbol,
                                            displayName: companyName
                                        }))
                                    }
                                ]
                            }}
                        />
                    </ElementContainer>
                </Grid>
                <Grid item xs={12} md={12} lg={4} sm={12}>
                    <ElementContainer>
                        <MarketOverview
                            widgetProps={{
                                height: 450,
                                tabs: [
                                    {
                                        title: 'Indices',
                                        symbols: [
                                            {
                                                s: 'FOREXCOM:SPXUSD',
                                                d: 'S&P 500'
                                            },
                                            {
                                                s: 'FOREXCOM:DJI',
                                                d: 'Dow 30'
                                            },
                                            {
                                                s: 'AMEX:VOO',
                                                d: 'Vanguard 500'
                                            }
                                        ]
                                    }
                                ]
                            }}
                        />
                    </ElementContainer>
                </Grid>
            </Container>
        </Box>
    )
};

export default DiaryHomeContainer;
