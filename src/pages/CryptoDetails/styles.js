import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
    priceContainer: {
        display: 'flex',
        alignItems: 'center',
        gap: '10px'
    }
});
