import React, { useState } from 'react';
import { Box, Typography, Container, Tabs, Tab } from '@mui/material';
import { useTheme } from '@mui/styles';
import { styled } from '@mui/material/styles';
import moment from 'moment';
import { useLocation, useNavigate } from 'react-router-dom';
import queryString from 'query-string';
import useFetchCryptoDetails from '../../hook/useFetchCryptoDetails';
import useFetchCryptoChartDetails from '../../hook/useFetchCryptoChartDetails';
import Loader from '../../components/Loader';
import TabPanel from '../../components/tabPanels';
import Page from '../../components/Page';
import CryptoSummary from '../../components/CryptoSummary';
import StockFinancials from '../../components/StockFinancials';
import { countMarketPrice, countPriceChangeColor } from '../../utils';
import { DEFAULT_COMPANY_LOGO } from '../../constants';
import { SymbolImage, Header } from '../StockDetails/StockDetailScreen';
import { useStyles } from './styles';

const CryptoDetailScreen = () => {
    const navigate = useNavigate();
    const location = useLocation();
    const theme = useTheme();
    const classes = useStyles();

    const {
        query: {
            id,
            range: chartRange,
            news: newsSelected,
            article: articleSelected
        }
    } = queryString.parseUrl(location.search);
    const queryParams = queryString.parse(location.search);
    const [selectedChartFilterValue, setSelectedChartFilterValue] = useState(chartRange);
    const [tabValue, setTabValue] = useState(0);
    const {
        details: {
            name,
            symbol,
            image,
            currentPrice = 0,
            priceChange_24h,
            priceChangePercentage_24h,
            current_price = 0,
            price_change_percentage_24h_in_currency,
            high_24h,
            low_24h,
            ...cryptoDetails
        },
        isLoadingDetails
    } = useFetchCryptoDetails(id);

    // @TODO: Remove this logics when it's done on backend, replace from & to parameters to range.
    let from;
    let to;
    const date = moment();
    switch(selectedChartFilterValue) {
        case '1mo':
            from = Date.parse(date.format());
            to = Date.parse(date.clone().subtract(1, 'M').format());
    }
    const { chartData, isLoadingChart } = useFetchCryptoChartDetails(id, from, to);

    const handleTabChange = (_, newValue) => setTabValue(newValue);

    const handleChartFilter = range => {
        setSelectedChartFilterValue(range);
        queryParams.range = range;
        navigate({ search: queryString.stringify(queryParams) });
    };

    return (
        <Page title='Crypto Details'>
            <Container maxWidth='xl' style={{ paddingTop: '116px' }}>
                <Header>
                    <SymbolImage src={image || DEFAULT_COMPANY_LOGO} />
                    { isLoadingDetails ? <Loader /> : (
                        <Box>
                            <Typography variant='h5' sx={{ opacity: 0.88, color: theme.palette.common.white }}>
                                { symbol } - { name }
                            </Typography>
                            <div className={classes.priceContainer}>
                                <Typography variant='h4' sx={{ color: theme.palette.common.white }}>
                                    { `${countMarketPrice(current_price)} $` }
                                </Typography>
                                <Typography variant='h6' sx={{ color: countPriceChangeColor(price_change_percentage_24h_in_currency) }}>
                                    { `${1000?.toLocaleString()} $ (${price_change_percentage_24h_in_currency?.toFixed(2)}%)` }
                                </Typography>
                            </div>
                        </Box>
                    ) }
                </Header>
                <Box>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <Tabs value={tabValue} onChange={handleTabChange}>
                            <Tab label='Summary' sx={{ color: theme.palette.common.white }} />
                        </Tabs>
                    </Box>
                    <TabPanel value={tabValue} index={0}>
                        <CryptoSummary
                            symbol={symbol}
                            chartData={chartData}
                            selectedChartFilterValue={selectedChartFilterValue}
                            handleChartFilterChange={handleChartFilter}
                            info={{}}
                            // info={cryptoInfo}
                            // isLoadingInfo={isLoadingCryptoInfo}
                            cryptoDetails={cryptoDetails}
                            isLoadingDetails={isLoadingDetails}
                        />
                    </TabPanel>
                </Box>
            </Container>
        </Page>
    );
};

export default CryptoDetailScreen;
