const paymentCardsData = [
    {
        id: 1,
        type: 'free',
        title: 'Free',
        description: 'Test your Stocks Advisor',
        price: 0,
        recurrency: 0,
        mostPopular: false,
        data: ['1 personal diary', '3 stocks in portfolio', 'Access to full financial reports']
    },
    {
        id: 2,
        type: 'basic',
        title: 'Personal',
        description: 'Best choice for personal usage',
        price: 4.99,
        recurrency: 3.99,
        mostPopular: true,
        data: [
            '2 personal diaries', '10 stocks in portfolio',
            'Access to full financial reports', 'Export financial reports as XLS',
        ]
    },
    {
        id: 3,
        type: 'pro',
        title: 'Professional',
        description: 'Best choice for teams / brokers / family',
        price: 20,
        mostPopular: false,
        recurrency: 18,
        data: [
            '10 personal diaries', '50 stocks in portfolio',
            'Access to full financial reports', 'Export companies financial reports',
            'Export financial reports as XLS',
            'Integrate with online brokers',
            'Attach files in your notes',
        ]
    }
];

export default paymentCardsData;
