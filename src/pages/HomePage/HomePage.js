import React from "react";
import {Grid, Container, Box, Button, ButtonBase, Divider, Typography} from '@mui/material';
import Page from '../../components/Page';
import {styled} from "@mui/material/styles";
import './styles.css';
import {Link, useNavigate} from "react-router-dom";
import notesImg from '../../assets/Finance analytics _Monochromatic.png';
import MainInfoText from "../../components/MainInfoText";
import PaymentCard from "../../components/PaymentCard";
import paymentCardsData from "./constants";
import {useAuth} from "../../hook/auth";
import {useTheme} from "@mui/styles";
// ----------------------------------------------------------------------
const SloganBox = styled(Box)(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    color: theme.palette.common.white,
    fontSize: 40,
    paddingBottom: 75,
    paddingTop: 40,
    textTransform: 'uppercase',
}));

const StyledContainer = styled(Container)(({ theme }) => ({
    color: theme.palette.common.white,
    display: 'flex',
    paddingTop: 116,
    flexDirection: 'column',
    alignItems: 'center',
}));

const StyledSubtitle = styled('h2')(({ theme }) => ({
    width: '70%',
    fontWeight: 400,
    paddingTop: 2,
    fontSize: '1.4rem',
    textAlign: 'center',
    paddingBottom: 40,
}));

const RegisterButton = styled(ButtonBase)(({ theme }) => ({
    borderBottom: `1px solid ${theme.palette.common.white}`,
    padding: 3,
}));

const MainInfoContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    width: '95%',
    paddingTop: 100,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 90,
    [theme.breakpoints.down('md')]: {
        flexDirection: 'column'
    },
}));

const MainInfoTextContainer = styled(Box)(({ theme }) => ({
    [theme.breakpoints.down('md')]: {
        display: 'flex',
        justifyContent: 'space-around',
    },
}));

const MainImage = styled('img')(({ theme }) => ({
    height: 620,
    width: 'auto',
    [theme.breakpoints.down('md')]: {
        height: 440,
        marginBottom: 20,
    },
}));
const BestPlanTitle = styled('h3')(({ theme }) => ({
    color: theme.palette.common.white,
    textAlign: 'center',
    fontSize: 36,
    fontWeight: 900,
    lineHeight: '150%'
}));
const BestPlanText = styled('p')(({ theme }) => ({
    color: theme.palette.common.white,
    textAlign: 'center',
    fontSize: 23,
    fontWeight: 400,
    width: '75%',
    lineHeight: '150%'
}));
const CardPricesContainer = styled(Box)(({ theme }) => ({
    paddingTop: 40,
    display: 'flex',
    width: '80%',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    color: theme.palette.common.black,
    justifyContent: 'space-around',
}));
const Footer = styled('footer')(({ theme }) => ({
    color: theme.palette.common.black,
    justifyContent: 'space-around',
}));
const FooterSignUp = styled(Box)(({ theme }) => ({
    padding: 25,
    display: 'flex',
    flexWrap: 'wrap',
    gap: 40,
    alignItems: 'center',
    backgroundColor: theme.palette.grey[500_48],
    color: theme.palette.common.white,
    justifyContent: 'center',
}));
const FooterSupportContainer = styled(Box)(({ theme }) => ({
    padding: 20,
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    color: theme.palette.common.white,
    justifyContent: 'space-between',
}));


export default function HomePage() {
    const navigate = useNavigate();
    const { isAuthorized } = useAuth();
    const theme = useTheme();
    const handleRegister = () => navigate('auth/register');
    const handleLogin = () => navigate('auth/login');

  return (
    <Page title="Home Page">
      <StyledContainer maxWidth="xl">
          <SloganBox>
              <Box className="title-text-container left-container">
                  <h1>turn your loses</h1>
              </Box>
              <Box className="title-text-container right-container">
                  <h1>into your gains</h1>
              </Box>
          </SloganBox>
          <h2 style={{fontSize: '3.2rem'}}>Stocks Portfolio Manager</h2>
          <StyledSubtitle>
              Improve your portfolio performance, note your ideas and thoughts,
              track received gains and
              manage stocks in your portfolio all in one app.
          </StyledSubtitle>
          {
              !isAuthorized &&
              (
                  <>
                      <Button
                          variant="contained"
                          size="large"
                          onClick={handleRegister}
                          sx={{fontSize: 22, width: 270, marginBottom: 2}}
                      >
                          Sign up for free
                      </Button>
                      <RegisterButton onClick={handleLogin}>
                          Already have an account? Log in
                      </RegisterButton>
                  </>
              )
          }
          <MainInfoContainer>
              <MainInfoTextContainer>
                  <MainInfoText
                      title="Manage portfolio"
                      description="If you are tired of writing down all your trades in a table. Don't like counting: cost, profit, dividends for each asset in your portfolio. Leave it to us. We will track all your trades on the fly."
                  />
                  <MainInfoText
                      title="Make notes"
                      description="Don't forget to take notes about your ideas and plans. Use our powerful text editor to remember everything and bring your plans to life."
                  />
                  <MainInfoText
                      title="track gains"
                      description="Keep track of your profits, current and future dividends. Create the passive income you want with using Stocks Advisor smart tools."
                  />
                  <MainInfoText
                      title="Keep yourself up to date"
                      description="Check the latest extended financial statements of your shares. Get complete financial statements of companies absolutely free."
                  />
              </MainInfoTextContainer>
              <MainImage src={notesImg} alt="notes"/>
          </MainInfoContainer>
          {/*<BestPlanTitle>Choose your best Stocks Advisor</BestPlanTitle>*/}
          {/*<BestPlanText>*/}
          {/*    Whether you want to get organized, keep your portfolio on track,*/}
          {/*    or boost incomes, Stocks Advisor has the right plan for you.*/}
          {/*</BestPlanText>*/}
          {/*<CardPricesContainer>*/}
          {/*    {*/}
          {/*        paymentCardsData.map(props => (*/}
          {/*            <PaymentCard*/}
          {/*                {...props}*/}
          {/*                key={props.id}*/}
          {/*                onClick={() => {}}*/}
          {/*            />*/}
          {/*        ))*/}
          {/*    }*/}
          {/*</CardPricesContainer>*/}
      </StyledContainer>
        <Footer>
            <FooterSignUp>
                <Typography variant="h5">
                    Start managing your portfolio
                </Typography>
                {
                    !isAuthorized && (
                        <Button
                            variant="contained"
                            size="large"
                            onClick={handleRegister}
                            sx={{fontSize: 22, width: 270}}
                        >
                            Sign up for free
                        </Button>
                    )
                }
            </FooterSignUp>
            <FooterSupportContainer>
                <Box>
                    <Link
                        to="/terms/service"
                        style={{textDecoration: 'none', color: theme.palette.common.white, marginRight: 30}}
                    >
                        Terms of Service
                    </Link>
                    <Link
                        to="/terms/policy"
                        style={{textDecoration: 'none', color: theme.palette.common.white}}
                    >
                        Privacy Policy
                    </Link>
                </Box>
                <Typography> &#169;Stocks Advisor</Typography>
            </FooterSupportContainer>
        </Footer>
    </Page>
  );
}
