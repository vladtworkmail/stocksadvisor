import React, {useState} from 'react';
import Page from '../../components/Page';
import { Container } from '@mui/material';
import NewsList from "../../components/NewsList";
import useFetchGeneralNews from "../../hook/useFetchGeneralNews";

const News = () => {
    const [isNewsModalOpen, setIsNewsModalOpen] = useState(false);
    const [newsContent, setNewsContent] = useState({});
    const {news, isLoadingNews} = useFetchGeneralNews();

    const handleNewsCardClick = (newsData) => {
        setIsNewsModalOpen(true);
        setNewsContent(newsData);
    };

    const handleNewsModalClose = () => setIsNewsModalOpen(false);

    return (
        <Page title='News' style={{ marginTop: '116px'}}>
            <Container maxWidth='xl'>
                <NewsList
                    title="General News"
                    news={news}
                    isLoading={isLoadingNews}
                    onNewsCardClick={handleNewsCardClick}
                    isNewsModalOpen={isNewsModalOpen}
                    onNewsModalClose={handleNewsModalClose}
                    newsContent={newsContent}
                />
            </Container>
        </Page>
    );
};

export default News;
