import React, {useState} from 'react';
import { StocksList } from '../../components/_dashboard/app';
import { useNavigate } from 'react-router-dom';
import queryString from 'query-string';
import useFetchStocksList from '../../hook/useFetchStocksList';
import useSearchStocks from '../../hook/useSearchStocks';

const Stocks = () => {
    const navigate = useNavigate();
    const [searchValue, setSearchValue] = useState('');
    const {
        list,
        fetchNextPage,
        isFetching,
        isFetchingNextPage,
    } = useFetchStocksList();
    const { searchList, isLoadingSearch } = useSearchStocks(searchValue, setSearchValue);
    const hasSearchResults = (!!searchList.length || isLoadingSearch) && searchValue;

    const loadMore = () => fetchNextPage();
    const handleSearch = (value) => setSearchValue(value);
    const handleSearchClear = () => setSearchValue('');

    const handleStockClick = (symbol, logo) => {
        const queryParams = {
            symbol,
            logo
        };
        navigate(`details?${queryString.stringify(queryParams)}`);
    };

    return (
        <StocksList
            isLoading={isFetchingNextPage || isFetching}
            stocksList={list}
            loadMore={loadMore}
            onStockCardClick={handleStockClick}
            onSearch={handleSearch}
            searchValue={searchValue}
            hasSearchResults={hasSearchResults}
            onSearchClear={handleSearchClear}
            isLoadingSearch={isLoadingSearch}
            searchList={searchList}
        />
    );
};

export default Stocks;
