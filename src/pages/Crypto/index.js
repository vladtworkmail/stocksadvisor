import React, { useState } from 'react';
import { CryptoList } from '../../components/_dashboard/app';
import { useNavigate, useLocation } from 'react-router-dom';
import useFetchCryptoList from '../../hook/useFetchCryptoList';
import useSearchCrypto from '../../hook/useSearchCrypto';
import queryString from 'query-string';

const Crypto = () => {
    const navigate = useNavigate();
    const location = useLocation();
    const queryParams = queryString.parse(location.search);
    const [searchValue, setSearchValue] = useState('');
    const {
        list,
        fetchNextPage,
        isFetchingNextPage
    } = useFetchCryptoList();
    const { searchList, isLoadingSearch } = useSearchCrypto(searchValue, setSearchValue);

    const handleItemClick = id => {
        queryParams.range = '1m';
        queryParams.id = id;
        navigate(`details?${queryString.stringify(queryParams)}`);
    };

	const handleSearch = value => setSearchValue(value);
	const handleSearchClear = () => setSearchValue('');
    const hasSearchResults = (!!searchList.length || isLoadingSearch) && searchValue;
    return (
        <CryptoList
            isLoading={isFetchingNextPage}
            list={list}
            loadMore={fetchNextPage}
            onItemCardClick={handleItemClick}
            onSearch={handleSearch}
            onSearchClear={handleSearchClear}
            hasSearchResults={hasSearchResults}
            searchValue={searchValue}
            searchList={searchList}
            isLoadingSearch={isLoadingSearch}
        />
    );
};

export default Crypto;
