import { Link as RouterLink } from 'react-router-dom';
// material
import { styled } from '@mui/material/styles';
import {Box, Link, Container, Typography, FormGroup} from '@mui/material';
// layouts
import AuthLayout from '../layouts/AuthLayout';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
// components
import Page from '../components/Page';
import { MHidden } from '../components/@material-extend';
import { RegisterForm } from '../components/authentication/register';
import AuthSocial from '../components/authentication/AuthSocial';
import { localizeText } from '../i18next';
import { useTheme } from '@mui/styles';
import {useState} from "react";

// ----------------------------------------------------------------------

const RootStyle = styled(Page)(({ theme }) => ({
  [theme.breakpoints.up('md')]: {
    display: 'flex'
  }
}));

const ContentStyle = styled('div')(({ theme }) => ({
  maxWidth: 530,
  margin: 'auto',
  display: 'flex',
  minHeight: '100vh',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: theme.spacing(12, 0)
}));

// ----------------------------------------------------------------------

export default function Register() {
  const theme = useTheme();
  const [agreed, setAgreed] = useState(true);
  return (
    <RootStyle title='Register | Stocks-Advisor-UI'>
      <AuthLayout>
        { `${localizeText('register.isAccount')} ` }
        <Link underline='none' variant='subtitle2' component={RouterLink} to='/auth/login'>
          { localizeText('register.login') }
        </Link>
      </AuthLayout>

      <Container>
        <ContentStyle>
          <Box sx={{ mb: 5 }}>
            <Typography variant='h4' gutterBottom sx={{ color: theme.palette.common.white }}>
              { localizeText('register.title') }
            </Typography>
            <Typography sx={{ color: theme.palette.common.white }}>
              { localizeText('register.subtitle') }
            </Typography>
          </Box>
          <AuthSocial action="register" disabled={!agreed} />
          <RegisterForm registerDisabled={!agreed} />

            <Box style={{display: 'flex', alignItems: 'center', paddingTop: 10}}>
                <Checkbox checked={agreed} onChange={() => setAgreed(old => !old)} />
                <Typography variant='body2' align='center' sx={{ color: theme.palette.grey[500] }}>
                    { `${localizeText('register.iAgree')} ` }
                    <Link
                        underline='always'
                        component={RouterLink}
                        to='/terms/service'
                        sx={{ color: theme.palette.common.white }}
                    >
                        { localizeText('register.termsOfService') }
                    </Link>
                    { ` ${localizeText('register.and')} ` }
                    <Link
                        underline='always'
                        component={RouterLink}
                        to='/terms/policy'
                        sx={{ color: theme.palette.common.white }}
                    >
                        { localizeText('register.privacyPolicy') }
                    </Link>
                    .
                </Typography>
            </Box>
        </ContentStyle>
      </Container>
    </RootStyle>
  );
};
