import React from 'react';
import {Box, Typography} from "@mui/material";
import {styled} from "@mui/material/styles";
import {useAuth} from "../../../hook/auth";
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import PeopleIcon from '@mui/icons-material/People';
import InsertChartIcon from '@mui/icons-material/InsertChart';
import ArticleIcon from '@mui/icons-material/Article';
import SupervisorAccountIcon from '@mui/icons-material/SupervisorAccount';
////////
export const Container = styled(Box)(({theme}) => ({
    paddingLeft: 30,
    paddingRight: 30,
    color: theme.palette.common.white,
    display: 'flex',
    justifyContent: 'center',
}));
export const UserInfoContainer = styled(Box)(({theme}) => ({
    paddingTop: 15,
    paddingLeft: 20,
}));
export const ReputationContainer = styled(Box)(({theme}) => ({
    display: 'flex',
    gap: 55,
    paddingTop: 20
}));
export const ReputationBox = styled(Box)(({theme}) => ({
    display: 'flex',
    flexWrap: 'wrap',
    color: theme.palette.widgetItemTextColor,
    justifyContent: 'center',
    maxWidth: 90,
}));
export const ReputationTitle = styled(Typography)(({theme}) => ({
    textTransform: 'uppercase'
}));
export const ReputationNumber = styled(Typography)(({theme}) => ({
    marginRight: 10
}));

const ProfileInfo = () => {
    const {userMail, userName} = useAuth();
    return (
        <Container>
            <AccountBoxIcon style={{width: 150, height: 150}}/>
            <UserInfoContainer>
                <Typography variant="h5">{userName}</Typography>
                <Typography variant="subtitle2">{userMail}</Typography>
                <ReputationContainer>
                    <ReputationBox>
                        <ReputationNumber variant="subtitle1">0</ReputationNumber>
                        <InsertChartIcon />
                        <ReputationTitle variant="subtitle2">reputation</ReputationTitle>
                    </ReputationBox>
                    <ReputationBox>
                        <ReputationNumber variant="subtitle1">0</ReputationNumber>
                        <ArticleIcon />
                        <ReputationTitle variant="subtitle2">articles</ReputationTitle>
                    </ReputationBox>
                    <ReputationBox>
                        <ReputationNumber variant="subtitle1">0</ReputationNumber>
                        <ThumbUpIcon />
                        <ReputationTitle variant="subtitle2">total likes</ReputationTitle>
                    </ReputationBox>
                    <ReputationBox>
                        <ReputationNumber variant="subtitle1">0</ReputationNumber>
                        <PeopleIcon />
                        <ReputationTitle variant="subtitle2">followers</ReputationTitle>
                    </ReputationBox>
                    <ReputationBox>
                        <ReputationNumber variant="subtitle1">0</ReputationNumber>
                        <SupervisorAccountIcon />
                        <ReputationTitle variant="subtitle2">following</ReputationTitle>
                    </ReputationBox>
                </ReputationContainer>
            </UserInfoContainer>
        </Container>
    )
};

export default ProfileInfo;
