import React, {useEffect, useState} from "react";
import {TextField, Stack, Box, Typography, Button} from "@mui/material";
import {styled} from "@mui/material/styles";
import {useTheme} from "@mui/styles";

export const Container = styled(Box)(({theme}) => ({
    color: theme.palette.common.white,
    border: `0.6px solid ${theme.palette.widgetItemTextColor}`,
    padding: 40,
    minWidth: 600
}));

export const InputContainer = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    marginTop: 45,
    gap: 20,
    justifyContent: 'flex-end',
}));

const ProfileSettings = ({userMail, onDeleteAccountClick}) => {
    const theme = useTheme();
    const [newMail, setNewMail] = useState(userMail);
    const [password, setPassword] = useState('');
    const [changePasswordButtonVisible, setChangePasswordButtonVisible] = useState(false);

    useEffect(() => {
        setChangePasswordButtonVisible(!!password);
    }, [password]);

    return (
        <Container>
            <Typography variant="h5">Private settings:</Typography>
            <InputContainer>
                <Typography variant="subtitle1">Email:</Typography>
                <TextField
                    fullWidth
                    type='email'
                    autoComplete='off'
                    label='Email address'
                    value={newMail}
                    inputProps={{
                        style: { color: theme.palette.common.white }
                    }}
                    onChange={(e) => setNewMail(e.target.value)}
                    error={false}
                />
            </InputContainer>
            <InputContainer>
                <Typography variant="subtitle1">Password:</Typography>
                <TextField
                    fullWidth
                    type='password'
                    autoComplete='off'
                    label='Password'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    inputProps={{
                        style: { color: theme.palette.common.white }
                    }}
                    error={false}
                />
                {
                    changePasswordButtonVisible &&
                    <Button variant="contained" color="secondary" size="medium">Update password</Button>
                }
            </InputContainer>
            <Button
                variant="contained"
                size="medium"
                style={{marginTop: 60}}
                onClick={onDeleteAccountClick}
            >
                Delete Account
            </Button>
        </Container>
    );
};

export default ProfileSettings;
