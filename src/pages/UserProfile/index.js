import React, {useState} from 'react';
import Page from "../../components/Page";
import UserProfileContainer from './UserProfileContainer';
import {Divider, Tab, Tabs} from "@mui/material";
import {useTheme} from "@mui/styles";
import TabPanel from "../../components/tabPanels";
import {styled} from "@mui/material/styles";
import ProfileSettings from "./components/ProfileSettings";
import {useAuth} from "../../hook/auth";
import DeleteUserAccountModal from "../../modals/DeleteUserAccountModal";
import useDeleteUserAccount from "../../hook/useDeleteUserAccount";

export const Container = styled(Page)(({theme}) => ({
    color: theme.palette.common.white,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
}));
export const CustomTab = styled(Tab)(({theme}) => ({
    textTransform: 'uppercase',
    fontSize: 18,
    color: 'white',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
}));

const UserProfile = () => {
    const theme = useTheme();
    const [tab, setTab] = useState(0);
    const [deleteAccountModal, setDeleteAccountModal] = useState(false);
    const { userMail, userName } = useAuth();
    const {mutate: deleteUserAccount} = useDeleteUserAccount();
    const handleChangeTab = (event, newValue) => setTab(newValue);

    const handleDeleteAccountClick= () => setDeleteAccountModal(true);
    const handleDeleteAccount = () => deleteUserAccount();

    return (
        <Container
            title="User Profile"
        >
            <UserProfileContainer/>
            <Divider style={{background: theme.palette.widgetItemTextColor, width: '100%'}}/>
            <Tabs
                variant='scrollable'
                value={tab}
                onChange={handleChangeTab}
                sx={{color: theme.palette.common.white}}
            >
                <CustomTab label='ARTICLES' />
                <CustomTab label='FOLLOWERS' />
                <CustomTab label='FOLLOWING' />
                <CustomTab label='SETTINGS' />
            </Tabs>
            <TabPanel value={tab} index={0}>
               Here will be your articles
            </TabPanel>
            <TabPanel value={tab} index={1}>
                Here will be your followers
            </TabPanel>
            <TabPanel value={tab} index={2}>
                Users you following
            </TabPanel>
            <TabPanel value={tab} index={3}>
                <ProfileSettings
                    userMail={userMail}
                    onDeleteAccountClick={handleDeleteAccountClick}
                />
            </TabPanel>
            <DeleteUserAccountModal
                open={deleteAccountModal}
                onClose={() => setDeleteAccountModal(false)}
                onDelete={handleDeleteAccount}
                userName={userName}
            />
        </Container>
    )
};

export default UserProfile;
