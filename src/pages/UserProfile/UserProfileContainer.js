import React from 'react';
import {Box} from "@mantine/core";
import {styled} from "@mui/material/styles";
import ProfileInfo from "./components/ProfileInfo";

export const Container = styled(Box)(({theme}) => ({
   paddingTop: 116,
    color: theme.palette.common.white
}));

const UserProfileContainer = () => {
    return (
        <Container>
            <ProfileInfo/>
        </Container>
    )
};

export default UserProfileContainer;
