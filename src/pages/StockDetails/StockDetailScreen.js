import React, { useState } from 'react';
import { Box, Typography, Container, Tabs, Tab } from '@mui/material';
import { useTheme } from '@mui/styles';
import { useLocation, useNavigate } from 'react-router-dom';
import queryString from 'query-string';
import Page from '../../components/Page';
import Loader from '../../components/Loader';
import TabPanel from '../../components/tabPanels';
import StockSummary from '../../components/StockSummary/StockSummary';
import StockFinancials from '../../components/StockFinancials';
import { useStyles } from './styles';
import useFetchStockDetails from "../../hook/useFetchStockDetails";
import useFetchStockNews from "../../hook/useFetchStockNews";
import {DEFAULT_COMPANY_LOGO} from "../../constants";
import {styled} from "@mui/material/styles";
import useFetchStockChart from "../../hook/useFetchStockChart";
import useFetchStockFundamentals from "../../hook/useFetchStockFundamentals";

export const SymbolImage = styled('img')(() => ({
    height: 'auto',
    width: 70,
    marginRight: '15px'
}));

export const Header = styled(Box)(() => ({
    paddingBottom: 35,
    display: 'flex',
    alignItems: 'center'
}));

const StockDetailScreen = () => {
    const location = useLocation();
    const theme = useTheme();
    const navigate = useNavigate();
    const classes = useStyles();
    const {
        query: {
            symbol,
            logo = '',
            range,
        }
    } = queryString.parseUrl(location.search);
    const queryParams = queryString.parse(location.search);
    const [selectedChartFilterValue, setSelectedChartFilterValue] = useState(range || '1m');
    const [tabValue, setTabValue] = useState(0);
    const [isNewsModalOpen, setIsNewsModalOpen] = useState(false);
    const [newsContent, setNewsContent] = useState({});
    const {chartData } = useFetchStockChart(symbol, selectedChartFilterValue);
    const {fundamentals, isLoadingFundamentals} = useFetchStockFundamentals(symbol);

    const {
        fullQuote: {
            latestPrice,
            currency,
            changePercent,
            change,
            companyName,
        },
        fullQuote,
        isLoadingDetail
    } = useFetchStockDetails(symbol);
    const { news } = useFetchStockNews(symbol);

    const handleTabChange = (_, newValue) => setTabValue(newValue);

    const handleNewsCardClick = (newsData) => {
        setIsNewsModalOpen(true);
        setNewsContent(newsData);
    };

    const handleNewsModalClose = () => setIsNewsModalOpen(false);

    const handleChartFilter = value => {
        setSelectedChartFilterValue(value);
        queryParams.range = value;
        navigate({ search: queryString.stringify(queryParams)});
    };

    return (
        <Page title='Stock Details'>
            <Container maxWidth='xl' style={{paddingTop: '116px'}}>
                <Header>
                    <SymbolImage src={logo || DEFAULT_COMPANY_LOGO} />
                    { isLoadingDetail ? <Loader/> : (
                        <Box>
                            <Typography variant='h5' sx={{ opacity: 0.88, color: theme.palette.common.white }}>
                                { symbol } - { companyName }
                            </Typography>
                            <div className={classes.priceContainer}>
                                <Typography variant='h4' sx={{ color: theme.palette.common.white }}>
                                    { `${latestPrice} ${currency}` }
                                </Typography>
                                <Typography variant='h6' sx={{ color: change >= 0 ? 'green' : theme.palette.primary.main }}>
                                    { `${change} ${currency} (${changePercent}%)` }
                                </Typography>
                            </div>

                        </Box>
                    ) }
                </Header>
                <Box>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <Tabs
                            value={tabValue}
                            onChange={handleTabChange}
                        >
                            <Tab label='Summary' sx={{ color: theme.palette.common.white }} />
                            <Tab label='Financials' sx={{ opacity: 0.88, color: theme.palette.common.white }} />
                            {/*<Tab label='Fair Value Metrics' sx={{ opacity: 0.88, color: theme.palette.common.white }} />*/}
                        </Tabs>
                    </Box>
                    <TabPanel value={tabValue} index={0}>
                        <StockSummary
                            symbol={symbol}
                            fullQuote={fullQuote}
                            selectedChartFilterValue={selectedChartFilterValue}
                            handleChartFilterChange={handleChartFilter}
                            chartData={chartData}
                            stockDetails={fundamentals}
                            isLoadingDetail={isLoadingFundamentals}
                            onNewsCardClick={handleNewsCardClick}
                            news={news}
                            isNewsModalOpen={isNewsModalOpen}
                            onNewsModalClose={handleNewsModalClose}
                            newsContent={newsContent}
                        />
                    </TabPanel>
                    <TabPanel value={tabValue} index={1}>
                        <StockFinancials/>
                    </TabPanel>
                </Box>
            </Container>
        </Page>
    );
};

export default StockDetailScreen;
