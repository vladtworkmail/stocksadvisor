import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
    priceContainer: {
        display: 'flex',
        alignItems: 'flex-end',
        gap: '10px'
    }
});
