import { Link as RouterLink } from 'react-router-dom';
// material
import { styled } from '@mui/material/styles';
import { Stack, Link, Container, Typography } from '@mui/material';
// layouts
import AuthLayout from '../layouts/AuthLayout';
// components
import Page from '../components/Page';
import { MHidden } from '../components/@material-extend';
import { LoginForm } from '../components/authentication/login';
import AuthSocial from '../components/authentication/AuthSocial';
import { localizeText } from '../i18next';
import { useTheme } from '@mui/styles';

// ----------------------------------------------------------------------

const RootStyle = styled(Page)(({ theme }) => ({
  [theme.breakpoints.up('md')]: {
    display: 'flex'
  }
}));

const ContentStyle = styled('div')(({ theme }) => ({
  maxWidth: 480,
  margin: 'auto',
  display: 'flex',
  minHeight: '100vh',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: theme.spacing(12, 0)
}));

// ----------------------------------------------------------------------

export default function Login() {
  const theme = useTheme();
  return (
    <RootStyle title='Login | Stocks-Advisor-UI'>
      <AuthLayout>
        { `${localizeText('login.noAccount')} ` }
        <Link underline='none' variant='subtitle2' component={RouterLink} to='/auth/register'>
          { localizeText('login.getStarted') }
        </Link>
      </AuthLayout>

      <Container maxWidth='sm'>
        <ContentStyle>
          <Stack sx={{ mb: 5 }}>
            <Typography variant='h4' gutterBottom sx={{ color: theme.palette.common.white }}>
              { localizeText('login.signIn') }
            </Typography>
            <Typography sx={{ color: theme.palette.common.white }}>
              { localizeText('login.details') }
            </Typography>
          </Stack>
          <AuthSocial action="login" />
          <LoginForm />
          <MHidden width='smUp'>
            <Typography variant='body2' align='center' sx={{ mt: 3 }}>
              { `${localizeText('login.noAccount')} ` }
              <Link variant='subtitle2' component={RouterLink} to='register'>
                { localizeText('login.getStarted') }
              </Link>
            </Typography>
          </MHidden>
        </ContentStyle>
      </Container>
    </RootStyle>
  );
};
