
export const getChartOptions = (theme, chartColor) => ({
    chart: {
        type: 'area',
        zoom: { enabled: false },
        toolbar: { show: false },
        brush: { enabled: false }
    },
    dataLabels: { enabled: false },
    stroke: {
        curve: 'smooth',
        width: 2
    },
    xaxis: {
        type: 'datetime',
        labels: { show: false },
        axisBorder: { show: false },
        axisTicks: { show: false }
    },
    yaxis: { labels: { show: false } },
    colors: [chartColor || theme.palette.primary.main],
    fill: {
        type: 'gradient',
        gradient: {
            gradientToColors: [theme.palette.grey[0], 'white'],
            shadeIntensity: 1,
            opacityFrom: 0,
            opacityTo: 0.7,
            stops: [0, 100]
        }
    },
    grid: { show: false },
    legend: { show: false },
    tooltip: {
        x: { show: false },
        fixed: { enabled: false },
        items: { display: 'none' },
        onDatasetHover: { highlightDataSeries: false }
    }
});
