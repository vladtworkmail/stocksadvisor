// Auth constants

export const USER_ID = 'userId';
export const USER_MAIL = 'userEmail';
export const USER_NAME = 'userName';
export const JWT = 'jwt';
export const DEFAULT_COMPANY_LOGO = 'https://webcolours.ca/wp-content/uploads/2020/10/webcolours-unknown.png';
export const LIST_ITEM_CARD_CHART_WIDTH = 370;
export const LIST_ITEM_CARD_CHART_HEIGHT = 100;
