import {useMutation} from "react-query";
import registerWithGoogle from "../queries/registerWithGoogle";
import {useAuth} from "./auth";
import {setupUserData} from "../utils/auth";
import {useNavigate} from "react-router-dom";

const useRegisterWithGoogle = () => {
    const {logOut} = useAuth();
    const navigate = useNavigate();
    const { isLoading, error, mutate } = useMutation(
        tokenId => registerWithGoogle(tokenId),
        {
            onSuccess: (userData) => {
                setupUserData(userData);
                navigate('/');
            },
            // If the mutation fails, use the context returned from onMutate to roll back
            onError: (err, newTodo, context) => {
                logOut();
            }
        }
        );
  return {
      registerWithGoogleIsLoading: isLoading,
      error,
      registerWithGoogle: mutate
  };
};

export default useRegisterWithGoogle;
