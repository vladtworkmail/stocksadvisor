import {useQuery} from "react-query";
import fetchDiaries from "../queries/fetchDiaries";

const useFetchDiaries = () => {
    const {data, isLoading} = useQuery(['userDiaries'], fetchDiaries);
    return {
        diaries: data?.data || [],
        isLoadingDiaries: isLoading
    }
};

export default useFetchDiaries;
