import {useQuery} from "react-query";
import fetchStocksSearch from "../queries/fetchStocksSearch";

const useSearchStocks = (searchValue, setSearchValue) => {
    const {data, isLoading, error } = useQuery([
        'stocksSearch', searchValue],
        () => fetchStocksSearch(searchValue),
        {
            enabled: !!searchValue
        }
    );

    if (error) {
        setSearchValue('');
    }
    return {
        searchList: data || [],
        isLoadingSearch: isLoading,
    }
};

export default useSearchStocks;
