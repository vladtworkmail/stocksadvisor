import { useQuery } from 'react-query';
import fetchCryptoCardShortQuote from '../queries/fetchCryptoCardShortQuote';

const useFetchCryptoCardShortQuote = id => {
    const { data, isLoading } = useQuery(['cryptoCardShortQuote', id], fetchCryptoCardShortQuote);

    return {
        shortQuote: data || {},
        isLoadingShortQuote: isLoading
    }
};

export default useFetchCryptoCardShortQuote;
