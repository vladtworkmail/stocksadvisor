import {useQuery} from "react-query";
import fetchDiaryStocks from "../queries/fetchDiaryStocks";
import formatDiaryActionHistory from "../formatters/diaryActionHistory";

const useFetchDiaryActionHistory = (diaryId) => {
    const {data, isLoading} = useQuery(
        ['diaryActionHistory', diaryId],
        fetchDiaryStocks
    );

    const formattedData = formatDiaryActionHistory(data);
    return {
        diaryActionHistory: formattedData,
        isLoadingDiaryActionHistory: isLoading
    }
};

export default useFetchDiaryActionHistory;
