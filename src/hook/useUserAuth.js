import {useMutation} from "react-query";
import {loginUser, registerUser} from "../queries/userAuthQueries";
import {setupUserData} from "../utils/auth";
import {useNavigate} from "react-router-dom";

export const useUserAuth = () => {
    const navigate = useNavigate();

    return useMutation(loginUser, {
        onSuccess: (userData) => {
            setupUserData(userData);
            navigate('/');
        }
    });
};

export const useUserRegister = () => {
    const navigate = useNavigate();

    return useMutation(registerUser, {
        onSuccess: (userData) => {
            setupUserData(userData);
            navigate('/');
        }
    });
};

