import {useMutation, useQueryClient} from "react-query";
import createDiary from "../queries/createDiary";

const useCreateDiary = () => {
    const queryClient = useQueryClient();
    const { isLoading, error, mutate } = useMutation(
        diaryName => createDiary(diaryName),
        {
            // When mutate is called:
            onMutate: async newDiary => {
                // Cancel any outgoing refetches (so they don't overwrite our optimistic update)
                await queryClient.cancelQueries('userDiaries');

                // Snapshot the previous value
                const previousDiaries = queryClient.getQueryData('userDiaries');

                // Optimistically update to the new value
                queryClient.setQueryData('userDiaries', ({data}) => ([...data, newDiary]));

                // Return a context object with the snapshotted value
                return { previousDiaries }
            },
            // If the mutation fails, use the context returned from onMutate to roll back
            onError: (err, newTodo, context) => {
                queryClient.setQueryData('userDiaries', context.previousDiaries)
            },
            // Always refetch after error or success:
            onSettled: () => {
                queryClient.invalidateQueries('userDiaries')
            },
        }
        );
  return {
      creationDiaryLoading: isLoading,
      error,
      createDiary: mutate
  };
};

export default useCreateDiary;
