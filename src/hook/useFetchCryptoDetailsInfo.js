import { useQuery } from 'react-query';
import fetchCryptoDetailsInfo from '../queries/fetchCryptoDetailsInfo';

const useFetchCryptoDetailsInfo = id => {
    const { data, isLoading } = useQuery(
        ['cryptoDetailsInfo', id],
        () => fetchCryptoDetailsInfo(id)
    );

    return {
        cryptoInfo: data || {},
        isLoadingCryptoInfo: isLoading
    };
};

export default useFetchCryptoDetailsInfo;
