import {useQuery} from "react-query";

import fetchGeneralNews from "../queries/fetchGeneralNews";

const useFetchGeneralNews = () => {
    const { data, isLoading} = useQuery(
        ['generalNews'],
        fetchGeneralNews
    );

    return {
        news: data || [],
        isLoadingNews: isLoading
    }
};

export default useFetchGeneralNews;
