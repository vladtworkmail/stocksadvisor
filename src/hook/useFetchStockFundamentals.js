import {useQuery} from "react-query";
import fetchStockFundamentals from "../queries/fetchStockFundamentals";

const useFetchStockFundamentals = (symbol) => {
    const { data, isLoading } = useQuery(
        ['stockFundamentals'],
        () => fetchStockFundamentals(symbol),
        {
            enabled: !!symbol,
            refetchOnMount: true
        }
    );

    return {
        fundamentals: data || {},
        isLoadingFundamentals: isLoading
    };
};

export default useFetchStockFundamentals;
