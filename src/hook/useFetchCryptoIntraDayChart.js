import { useQuery } from 'react-query';
import fetchCryptoIntraDayChart from '../queries/fetchCryptoIntraDayChart';
import { formatCryptoChartData } from '../formatters/cryptoChartData';

const useFetchCryptoIntraDayChart = id => {
    const { data, isLoading } = useQuery(
        ['cryptoIntraDayChart', id],
        fetchCryptoIntraDayChart
    );

    const formattedChartData = formatCryptoChartData(data?.prices || []);
    return {
        chartData: formattedChartData,
        isLoadingChart: isLoading
    }
};

export default useFetchCryptoIntraDayChart;
