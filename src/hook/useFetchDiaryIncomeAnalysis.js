import {useQuery} from "react-query";
import fetchDiaryAnalysis from "../queries/fetchDiaryAnalysis";

const useFetchDiaryIncomeAnalysis = (diaryId) => {
    const {data, isLoading} = useQuery(
        ['diaryIncomeAnalysis', diaryId],
        fetchDiaryAnalysis,
        {
            enabled: !!diaryId && diaryId !== 'undefined'
        }
    );

    return {
        diaryIncomeAnalysis: data || {},
        isLoadingDiaryIncomeAnalysis: isLoading
    };
};

export default useFetchDiaryIncomeAnalysis;
