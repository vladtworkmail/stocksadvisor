import {useMutation, useQueryClient} from "react-query";
import changeDiaryStockQuantity from "../queries/changeDiaryStockQuantity";

const useChangeDiaryStockQuantity = () => {
    const queryClient = useQueryClient();
    const { isLoading, mutate } = useMutation(changeDiaryStockQuantity, {
        onSettled: () => {
            queryClient.invalidateQueries('diaryStocks');
            queryClient.invalidateQueries('diaryActionHistory');
        }
    });
    return {
        creationDiaryStockLoading: isLoading,
        changeDiaryStockQuantity: mutate
    };
};

export default useChangeDiaryStockQuantity;
