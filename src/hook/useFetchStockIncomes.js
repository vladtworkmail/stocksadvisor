import {useQuery} from "react-query";
import fetchStockIncomes from "../queries/fetchStockIncomes";

const useFetchStockIncomes = (symbol) => {
    const {data, isLoading} = useQuery(
        ['stockIncomes', symbol],
        () => fetchStockIncomes(symbol)
    );

    return {
        incomes: data || [],
        isLoadingIncomes: isLoading
    };
};

export default useFetchStockIncomes;
