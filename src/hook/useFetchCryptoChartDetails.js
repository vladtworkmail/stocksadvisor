import { useQuery } from 'react-query';
import fetchCryptoChartDetails from '../queries/fetchCryptoChartDetails';

const useFetchCryptoChartDetails = (id, from, to) => {
    const { data, isLoading } = useQuery(
        ['cryptoChartDetails', id, from, to],
        () => fetchCryptoChartDetails(id, from, to)
    );

    return {
        chartData: data?.prices || [],
        isLoadingChart: isLoading
    };
};

export default useFetchCryptoChartDetails;
