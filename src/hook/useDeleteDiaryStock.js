import {useMutation, useQueryClient} from "react-query";
import deleteDiaryStock from "../queries/deleteDiaryStock";

const useDeleteDiaryStock = () => {
    const queryClient = useQueryClient();
    const { isLoading, mutate } = useMutation(deleteDiaryStock, {
        onSettled: () => {
            queryClient.invalidateQueries('diaryStocks');
        }
    });
    return {
        deletingDiaryStockLoading: isLoading,
        deleteDiaryStock: mutate
    };
};

export default useDeleteDiaryStock;
