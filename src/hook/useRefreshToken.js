import {useAuth} from "./auth";
import axios from "axios";

const useRefreshToken = () => {
    const { isAuthorized, setIsAuthorized } = useAuth();

    const refreshToken = async () => {
        const {jwtToken } = await axios.post('user/refreshToken', { withCredentials: true });
        setIsAuthorized(!!jwtToken);
        return jwtToken;
    };
};

export default useRefreshToken;
