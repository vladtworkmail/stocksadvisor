import {useQuery} from "react-query";
import fetchStockBalance from "../queries/fetchStockBalance";

const useFetchStockBalance = (symbol) => {
    const {data, isLoading} = useQuery(
        ['stockBalance', symbol],
        () => fetchStockBalance(symbol)
    );

    return {
        balance: data || [],
        isLoadingBalance: isLoading
    };
};

export default useFetchStockBalance;
