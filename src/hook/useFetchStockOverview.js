import {useQuery} from "react-query";
import fetchStockOverview from "../queries/fetchStockOverview";

const useFetchStockOverview = (symbol) => {
    const {data, isLoading} = useQuery(
        ['stockOverview', symbol],
        () => fetchStockOverview(symbol)
    );

    return {
        overview: data || {},
        isLoadingOverview: isLoading,
    };
};

export default useFetchStockOverview;
