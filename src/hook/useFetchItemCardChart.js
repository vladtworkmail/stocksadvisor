import {useQuery} from "react-query";
import fetchIntraDayPrice from "../queries/fetchIntraDayPrice";

const useFetchItemCardChart = (symbol) => {
    const {data, isLoading} = useQuery(
        ['itemCardChart', symbol],
        fetchIntraDayPrice
    );

    return {
        isLoadingChart: isLoading,
        chartData: data || []
    }
};

export default useFetchItemCardChart;
