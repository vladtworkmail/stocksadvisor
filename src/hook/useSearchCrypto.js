import { useQuery } from 'react-query';
import fetchCryptoSearch from '../queries/fetchCryptoSearch';

const useSearchCrypto = (searchValue, setSearchValue) => {
    const { data, isLoading, error } = useQuery([
        'cryptoSearch', searchValue],
        () => fetchCryptoSearch(searchValue),
        { enabled: !!searchValue }
    );

    if(error) {
        setSearchValue('');
    }
    return {
        searchList: data || [],
        isLoadingSearch: isLoading
    }
};

export default useSearchCrypto;
