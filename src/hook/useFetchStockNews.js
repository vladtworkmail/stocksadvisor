import {useQuery} from "react-query";
import fetchStockNews from "../queries/fetchStockNews";

const useFetchStockNews = (symbol) => {
    const { data, isLoading} = useQuery(
        ['stockNews', symbol],
        () => fetchStockNews(symbol)
    );

    return {
        news: data || [],
        isLoadingNews: isLoading
    }
};

export default useFetchStockNews;
