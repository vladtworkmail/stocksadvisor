import {useQuery} from "react-query";
import fetchShortQuote from "../queries/fetchShortQuote";

const useFetchItemCardQuote = (symbol) => {
    const { data, isLoading} = useQuery(
        ['itemCardShortQuote', symbol], fetchShortQuote);

    return {
        shortQuoteIsLoading: isLoading,
        shortQuote: data || {}
    }
};

export default useFetchItemCardQuote;
