import {useQuery} from "react-query";
import fetchHistoricalChart from "../queries/fetchHistoricalChart";
import {formatChartData} from "../formatters/chartData";

const useFetchStockChart = (symbol, range) => {
    const {data, isLoading} = useQuery(
        ['historicalChart', {symbol, range}],
        fetchHistoricalChart,
        {
            enabled: !!symbol
        }
    );

    const formattedChartData = formatChartData(data || []);
    return {
        chartData: formattedChartData,
        isLoadingChart: isLoading,
    }
};

export default useFetchStockChart;
