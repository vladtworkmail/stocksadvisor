import { useState, useEffect } from 'react';
import { JWT, USER_ID, USER_MAIL, USER_NAME } from '../constants';

export const useAuth = () => {
    const [isAuthorized, setIsAuthorized] = useState(false);
    const [token, setToken] = useState('');
    const [userId, setUserId] = useState('');
    const [userName, setUserName] = useState('');
    const [userMail, setUserMail] = useState('');

    const logOut = () => {
        setIsAuthorized(false);
        setToken('');
        setUserId('');
        setUserName('');
        setUserMail('');
        localStorage.clear();
    };

    useEffect(() => {
        const token = localStorage.getItem(JWT);
        const id = localStorage.getItem(USER_ID);
        const mail = localStorage.getItem(USER_MAIL);
        const name = localStorage.getItem(USER_NAME);

        if (token && id) {
            setToken(token);
            setUserId(id);
            setUserMail(mail);
            setUserName(name);
            setIsAuthorized(true);
        } else {
            logOut();
        }
    });

    return {
        isAuthorized, token,
        userId, userMail,
        setIsAuthorized,
        userName, logOut
    };
}
