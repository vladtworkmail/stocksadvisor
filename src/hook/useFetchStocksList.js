import { useInfiniteQuery } from 'react-query';
import fetchStocksList from '../queries/fetchStocksList';

const useFetchStocksList = () => {
    const {
        data,
        error,
        fetchNextPage,
        hasNextPage,
        isFetching,
        isFetchingNextPage,
    } = useInfiniteQuery(
        'stocksList',
        fetchStocksList,
        {
            getNextPageParam: (_, allPages) => allPages.length + 1,
            keepPreviousData: true,
            cacheTime: 1800000 // 30 min
        }
    );

    const list = data?.pages?.map(page => page.map(item => item)) || [];

    return {
        list: list.flat(),
        fetchNextPage,
        hasNextPage,
        isFetching,
        isFetchingNextPage,
        error
    }
};

export default useFetchStocksList;
