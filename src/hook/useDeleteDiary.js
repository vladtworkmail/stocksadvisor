import {useMutation, useQueryClient} from "react-query";
import {toast} from "react-toastify";
import deleteDiary from "../queries/deleteDiary";

const useDeleteDiary = () => {
    const queryClient = useQueryClient();
    return useMutation(deleteDiary, {
        onSuccess: (diaryId) => {
            queryClient.setQueryData('userDiaries', (oldData) => {
                const newData = oldData.data.filter(diary => diary.id !== diaryId);
                return {
                    ...oldData,
                    data: newData
                }
            });
            toast.success('Diary was successfully deleted.');
        }
    });
};

export default useDeleteDiary;
