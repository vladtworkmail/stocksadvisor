import {useMutation} from "react-query";
import {toast} from "react-toastify";
import {useAuth} from "./auth";
import deleteUserAccount from "../queries/deleteUserAccount";
import {useNavigate} from "react-router-dom";

const useDeleteUserAccount = () => {
    const { logOut } = useAuth();
    const navigate = useNavigate();
    return useMutation(deleteUserAccount, {
        onSuccess: () => {
            logOut();
            navigate('/');
            toast.success('Account was successfully deleted.');
        }
    });
};

export default useDeleteUserAccount;
