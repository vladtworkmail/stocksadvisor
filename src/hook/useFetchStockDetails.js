import {useQuery} from "react-query";
import fetchStockDetails from "../queries/fetchStockDetails";

const useFetchStockDetails = (symbol) => {
    const {data, isLoading} = useQuery(
        ['stockDetails', symbol],
        () => fetchStockDetails(symbol),
        {
            enabled: !!symbol
        }
    );

    return {
        fullQuote: data || {},
        details: {},
        historicalData: data?.historicalData || {},
        predictions: data?.predictions || {},
        isLoadingDetail: isLoading
    };
};

export default useFetchStockDetails;
