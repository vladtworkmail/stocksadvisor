import {useMutation, useQueryClient} from "react-query";
import updateDiaryName from "../queries/updateDiaryName";
import {toast} from "react-toastify";

const useUpdateDiaryName = () => {
    const queryClient = useQueryClient();
    return useMutation(updateDiaryName, {
        onSuccess: (data) => {
            queryClient.setQueryData('userDiaries', (oldData) => {
                const newData = oldData.data.map(diary => {
                    if (diary.id === data.id) {
                        return data;
                    }
                    return diary
                });

                return {
                    ...oldData,
                    data: newData
                }
            })
        },
        onError: () => {
            toast.error('Something went wrong.');
        }
    });
};

export default useUpdateDiaryName;
