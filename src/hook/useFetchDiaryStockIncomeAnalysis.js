import {useQuery} from "react-query";
import fetchDiaryStockIncomeAnalysis from "../queries/fetchDiaryStockIncomeAnalysis";

const useFetchDiaryStockIncomeAnalysis = (stockId) => {
    const {data, isLoading} = useQuery(
        ['diaryStockIncomeAnalysis', stockId],
        fetchDiaryStockIncomeAnalysis,
        {
            enabled: !!stockId && stockId !== 'undefined'
        }
    );

    return {
        diaryStockIncomeAnalysis: data || {},
        isLoadingDiaryStockIncomeAnalysis: isLoading
    };
};

export default useFetchDiaryStockIncomeAnalysis;
