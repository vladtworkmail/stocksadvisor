import {useQuery} from "react-query";
import fetchStockBalance from "../queries/fetchStockBalance";
import fetchStockCashFlow from "../queries/fetchStockCashFlow";

const useFetchStockCashFlow = (symbol) => {
    const {data, isLoading} = useQuery(
        ['stockCashFlow', symbol],
        () => fetchStockCashFlow(symbol)
    );

    return {
        cashFlow: data || [],
        isLoadingCashFlow: isLoading
    };
};

export default useFetchStockCashFlow;
