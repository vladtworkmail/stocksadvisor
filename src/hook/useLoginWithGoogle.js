import {useMutation} from "react-query";
import {useAuth} from "./auth";
import loginWithGoogle from "../queries/loginWithGoogle";
import {setupUserData} from "../utils/auth";
import {useNavigate} from "react-router-dom";

const useLoginWithGoogle = () => {
    const {logOut} = useAuth();
    const navigate = useNavigate();
    const { isLoading, error, mutate } = useMutation(
        tokenId => loginWithGoogle(tokenId),
        {
            // If the mutation fails, use the context returned from onMutate to roll back
            onError: (err, newTodo, context) => {
                logOut();
            },
            onSuccess: (userData) => {
                setupUserData(userData);
                navigate('/');
            }
        }
        );
  return {
      loginWithGoogleIsLoading: isLoading,
      error,
      loginWithGoogle: mutate
  };
};

export default useLoginWithGoogle;
