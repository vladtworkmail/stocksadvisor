import { useInfiniteQuery } from 'react-query';
import fetchCryptoList from '../queries/fetchCryptoList';

const useFetchCryptoList = () => {
    const {
        data,
        error,
        fetchNextPage,
        hasNextPage,
        isFetching,
        isFetchingNextPage,
    } = useInfiniteQuery(
        'cryptoList',
        fetchCryptoList,
        {
            getNextPageParam: lastPage => lastPage?.data?.next.page,
            keepPreviousData: true,
            cacheTime: 1800000 // 30 min
        }
    );

    const list = data?.pages?.map(page => page.map(item => item)) || [];

    return {
        list: list.flat(),
        fetchNextPage,
        hasNextPage,
        isFetching,
        isFetchingNextPage,
        error
    }
};

export default useFetchCryptoList;
