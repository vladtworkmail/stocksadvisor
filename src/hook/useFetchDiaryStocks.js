import {useQuery} from "react-query";
import fetchDiaryStocks from "../queries/fetchDiaryStocks";
import {formatDiaryStocksList} from "../formatters/diaryStocksList";

const useFetchDiaryStocks = (diaryId) => {
    const {data, isLoading} = useQuery(
        ['diaryStocks', diaryId],
        fetchDiaryStocks
    );
    const formattedStocks = formatDiaryStocksList(data);
    return {
        stocks: formattedStocks,
        isLoadingDiaryStocks: isLoading
    }
};

export default useFetchDiaryStocks;
