import {useMutation, useQueryClient} from "react-query";
import createDiaryStock from "../queries/createDiaryStock";
import {toast} from "react-toastify";

const useCreateDiaryStock = () => {
    const queryClient = useQueryClient();
    const { isLoading, error, mutate } = useMutation(createDiaryStock, {
        onSettled: () => {
            queryClient.invalidateQueries('diaryStocks');
        }
    });
    return {
        creationDiaryStockLoading: isLoading,
        error,
        createDiaryStock: mutate
    };
};

export default useCreateDiaryStock;
