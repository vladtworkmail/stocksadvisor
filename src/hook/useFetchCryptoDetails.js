import { useQuery } from 'react-query';
import fetchCryptoDetails from '../queries/fetchCryptoDetails';

const useFetchCryptoDetails = id => {
    const { data, isLoading } = useQuery(
        ['cryptoDetails', id],
        () => fetchCryptoDetails(id)
    );

    return {
        details: data || {},
        isLoadingDetails: isLoading
    };
};

export default useFetchCryptoDetails;
