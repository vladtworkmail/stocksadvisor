import {useMutation, useQueryClient} from "react-query";
import createDiaryStockNote from "../queries/createDiaryStockNote";
import {toast} from "react-toastify";

const useCreateDiaryStockNote = () => {
    const queryClient = useQueryClient();
    const { isLoading, mutate } = useMutation(createDiaryStockNote, {
        onSettled: () => {
            toast.success('Note was added successfully!');
            queryClient.invalidateQueries('diaryStocks');
        }
    });
    return {
        createNewNoteIsLoading: isLoading,
        createNewNote: mutate
    };
};

export default useCreateDiaryStockNote;
