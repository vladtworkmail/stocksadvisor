import 'simplebar/src/simplebar.css';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { HelmetProvider } from 'react-helmet-async';
import App from './App';
import { GoogleOAuthProvider } from '@react-oauth/google';
import { MantineProvider } from '@mantine/core';
import * as serviceWorker from './serviceWorker';
import reportWebVitals from './reportWebVitals';
import {ToastContainer} from 'react-toastify';
import {
    QueryClient,
    QueryCache,
    MutationCache,
    QueryClientProvider,
} from 'react-query';
import { requestErrorHandler } from './utils';
import 'react-toastify/dist/ReactToastify.css';
// ----------------------------------------------------------------------
const queryClient = new QueryClient({
    queryCache: new QueryCache({
        onError: requestErrorHandler
    }),
    mutationCache: new MutationCache({
        onError: requestErrorHandler
    }),
    defaultOptions: {
        queries: {
            retry: false,
            refetchOnWindowFocus: false,
            refetchOnMount: false
        }
    }
});

ReactDOM.render(
    <GoogleOAuthProvider clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}>
        <QueryClientProvider client={queryClient}>
            <HelmetProvider>
                <BrowserRouter>
                    <App/>
                    <ToastContainer
                        position='top-center'
                        autoClose={3000}
                        hideProgressBar
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover
                    />
                </BrowserRouter>
            </HelmetProvider>
        </QueryClientProvider>
    </GoogleOAuthProvider>,
  document.getElementById('root')
);

// If you want to enable client cache, register instead.
serviceWorker.unregister();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
