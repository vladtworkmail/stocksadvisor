import { Navigate, useRoutes } from 'react-router-dom';
// layouts
import DashboardLayout from './layouts/dashboard';
import LogoOnlyLayout from './layouts/LogoOnlyLayout';
//
import Login from './pages/Login';
import Register from './pages/Register';
import HomePage from './pages/HomePage/HomePage';
import News from './pages/News';
import Dairy from './pages/Diary';
import NotFound from './pages/Page404';
import Stocks from './pages/Stocks';
import Crypto from './pages/Crypto';
import StockDetailScreen from './pages/StockDetails/StockDetailScreen';
import CryptoDetailScreen from './pages/CryptoDetails/CryptoDetailScreen';
import {useAuth} from "./hook/auth";
import {toast} from "react-toastify";
import {JWT} from "./constants";

// ----------------------------------------------------------------------

const PrivateRout = ({children, isAuthorized}) => {
    const token = localStorage.getItem(JWT);
    if (!token && !isAuthorized) {
        toast.warn('User should be authorized.');
        return <Navigate to="/auth/login" replace/>;
    }
    return children;
};

export default function Router() {
    const { isAuthorized } = useAuth();
    return useRoutes([
        {
            path: '/',
            element: <DashboardLayout/>,
            children: [
                { path: '', element: <HomePage /> },
                { path: '404', element: <NotFound /> },
                { path: '*', element: <Navigate to='/404' /> }
            ]
        },
        {
            path: '/dashboard',
            element: <DashboardLayout />,
            children: [
                { path: '', element: <PrivateRout isAuthorized={isAuthorized}><Dairy /></PrivateRout> },
            ]
        },
        {
            path: '/news',
            element: <DashboardLayout />,
            children: [
                { path: '', element: <News /> },
            ]
        },
        {
            path: '/stock',
            element: <DashboardLayout />,
            children: [
                { path: '', element: <Stocks /> },
                { path: 'details', element: <StockDetailScreen /> },
            ]
        },
        {
            path: '/crypto',
            element: <DashboardLayout />,
            children: [
                { path: '', element: <Crypto /> },
                { path: 'details', element: <CryptoDetailScreen /> }
            ]
        },

        {
            path: '/auth',
            element: <LogoOnlyLayout />,
            children: [
                { path: 'login', element: <Login /> },
                { path: 'register', element: <Register /> }
            ]
        },
        { path: '*', element: <Navigate to='/404' replace /> }
    ]);
}
