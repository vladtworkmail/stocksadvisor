import queryString from "query-string";
import {postData} from "../api";

const changeDiaryStockQuantity = async (params) => {
    const url = `user/diaryStock/addAction?${queryString.stringify(params)}`;
    const {data} = await postData(url);
    return data;
};

export default changeDiaryStockQuantity;
