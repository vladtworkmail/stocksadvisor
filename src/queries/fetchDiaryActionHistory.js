import {fetchData} from "../api";

const fetchDiaryActionHistory = async ({queryKey}) => {
    const diaryId = queryKey[1];
    const url = `user/diary/actionHistory?diaryId=${diaryId}`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchDiaryActionHistory;
