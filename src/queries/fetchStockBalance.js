import {fetchData} from "../api";

const fetchStockBalance = async (symbol) => {
    const url = `financial/balanceSheet?symbol=${symbol}`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchStockBalance;
