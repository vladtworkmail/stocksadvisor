import {fetchData} from "../api";

const fetchShortQuote = async ({queryKey}) => {
    const symbol = queryKey[1];
    const url = `stock/shortQuote?symbol=${symbol}`;
    const {data} = await fetchData(url);
    return data;
};

export default fetchShortQuote;
