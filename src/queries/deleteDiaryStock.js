import {deleteData} from "../api";

const deleteDiaryStock = async(diaryStockId) => {
    const url = `user/diaryStock/delete?diaryStockId=${diaryStockId}`;
    const { data } = await deleteData(url);
    return data;
};

export default deleteDiaryStock;
