import {fetchData} from "../api";

const fetchStockDetails = async (symbol) => {
    const url = `stock/fullQuote?symbol=${symbol}`;
    const { data } = await fetchData(url );
    return data;
};

export default fetchStockDetails;
