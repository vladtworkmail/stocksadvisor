import {postData} from "../api";

const createDiary = async (name) => {
    const url = `user/diary/create?newDiaryName=${name}`;
    const { data } = await postData(url);
    return data;
};

export default createDiary;
