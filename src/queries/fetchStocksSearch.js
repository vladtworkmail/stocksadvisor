
import {fetchData} from "../api";

const fetchStocksSearch = async (searchValue) => {
    const url = `stock/search?searchText=${searchValue}`;
    const { data } = await fetchData(url );
    return data;
};

export default fetchStocksSearch;
