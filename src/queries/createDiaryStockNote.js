import {postData} from "../api";
import queryString from "query-string";

const createDiaryStockNote = async(params) => {
    const url = `user/diaryStock/createNewNotes?${queryString.stringify(params)}`;
    const { data } = await postData(url);
    return data;
};

export default createDiaryStockNote;
