import {fetchData} from "../api";

const fetchDiaryStocks = async ({queryKey}) => {
    const diaryId = queryKey[1];
    const url = `user/diaryStock/getAll?diaryId=${diaryId}`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchDiaryStocks;
