import {fetchData} from "../api";

const fetchGeneralNews = async () => {
    const url = 'news?limit=20';
    const { data } = await fetchData(url);
    return data;
};

export default fetchGeneralNews;
