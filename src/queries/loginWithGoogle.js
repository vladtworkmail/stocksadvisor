import {postData} from "../api";

const loginWithGoogle = async (tokenId) => {
    const url = `user/authenticateWithGoogle?idToken=${tokenId}`;
    const {data} = await postData(url);
    return data;
};

export default loginWithGoogle;
