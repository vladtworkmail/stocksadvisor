import {fetchData} from "../api";

const fetchStockCashFlow = async (symbol) => {
    const url = `financial/cashFlow?symbol=${symbol}`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchStockCashFlow;
