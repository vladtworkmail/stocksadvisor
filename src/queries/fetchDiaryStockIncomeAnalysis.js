import {fetchData} from "../api";

const fetchDiaryStockIncomeAnalysis = async ({queryKey}) => {
    const stockId = queryKey[1];
    const url = `user/diaryStock/historyIncomeAnalysis?diaryStockId=${stockId}`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchDiaryStockIncomeAnalysis;
