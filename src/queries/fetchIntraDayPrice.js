import {fetchData} from "../api";

const fetchIntraDayPrice = async ({queryKey}) => {
    const symbol = queryKey[1];
    const url = `stock/intraDayPrices?symbol=${symbol}&chartInterval=15`;
    const {data} = await fetchData(url);
    return data;
};

export default fetchIntraDayPrice;
