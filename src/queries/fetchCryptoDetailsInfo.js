import { fetchData } from '../api';

const fetchCryptoDetailsInfo = async id => {
    const url = `crypto/details/info?id=${id}`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchCryptoDetailsInfo;
