import {deleteData} from "../api";

const deleteUserAccount = async () => {
    const url = `user`;
    await deleteData(url);
};

export default deleteUserAccount;
