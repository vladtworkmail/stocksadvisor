import {fetchData} from "../api";

const fetchStockOverview = async (symbol) => {
    const url = `stock/company?symbol=${symbol}`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchStockOverview;
