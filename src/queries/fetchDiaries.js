import {fetchData} from "../api";

const fetchDiaries = async () => {
    const url = 'user/diary/getAll';
    return await fetchData(url);
};

export default fetchDiaries;
