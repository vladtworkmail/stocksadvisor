import { fetchData } from '../api';

const fetchCryptoDetails = async id => {
    const url = `crypto/fullQuote?id=${id}&priceChangePercentage=24h`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchCryptoDetails;
