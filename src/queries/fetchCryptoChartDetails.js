import { fetchData } from '../api';

const fetchCryptoChartDetails = async (id, from, to) => {
    const url = `crypto/historicalChart?id=${id}&from=${from}&to=${to}`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchCryptoChartDetails;
