import {updateData} from "../api";

const updateDiaryName = async ({diaryId, diaryTitleValue}) => {
    const url = `user/diary/updateDiary?diaryId=${diaryId}&newName=${diaryTitleValue}`;
    const {data} = await updateData(url);
    return data;
};

export default updateDiaryName;
