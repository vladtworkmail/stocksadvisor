import {postData} from "../api";

const registerWithGoogle = async (tokenId) => {
    const url = `user/registerWithGoogle?idToken=${tokenId}`;
    const { data } = await postData(url);
    return data;
};

export default registerWithGoogle;
