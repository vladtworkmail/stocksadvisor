import {fetchData} from "../api";

const fetchStockIncomes = async (symbol) => {
    const url = `financial/income?symbol=${symbol}`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchStockIncomes;
