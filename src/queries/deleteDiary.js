import {deleteData} from "../api";

const deleteDiary = async (diaryId) => {
    const url = `user/diary/delete?diaryId=${diaryId}`;
    await deleteData(url);
    return diaryId;
};

export default deleteDiary;
