import queryString from "query-string";
import {postData} from "../api";

export const registerUser = async params => {
    const url = `user/register?${queryString.stringify(params)}`;
    const {data} = await postData(url, params);
    return data;
};

export const loginUser = async params => {
    const url = `user/authenticate?${queryString.stringify(params)}`;
    const {data} = await postData(url, params);
    return data;
};

