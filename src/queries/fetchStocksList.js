import { fetchData } from '../api';

const fetchStocksList = async ({ pageParam = 1 }) => {
    const url = `stock/mostActive?pageNumber=${pageParam}&pageLimit=${30}`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchStocksList;
