import {fetchData} from "../api";

const fetchStockFundamentals = async (symbol) => {
    const url = `financial/fundamental?symbol=${symbol}`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchStockFundamentals;
