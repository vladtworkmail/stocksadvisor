import { fetchData } from '../api';

const fetchCryptoSearch = async searchValue => {
    const url = `crypto/search?searchText=${searchValue}`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchCryptoSearch;
