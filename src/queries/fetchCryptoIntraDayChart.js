import { fetchData } from '../api';

const fetchCryptoIntraDayChart = async ({ queryKey }) => {
    const id = queryKey[1];
    const url = `crypto/intraDayChart?id=${id}`;
    const { data } = await fetchData(url);

    return data;
};

export default fetchCryptoIntraDayChart;
