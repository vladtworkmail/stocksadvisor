import {fetchData} from "../api";

const fetchHistoricalChart = async ({queryKey}) => {
    const {symbol, range} = queryKey[1];
    const url = `stock/historicalData?symbol=${symbol}&range=${range}`;
    const {data} = await fetchData(url);
    return data;
};

export default fetchHistoricalChart;
