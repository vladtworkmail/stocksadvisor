import {fetchData} from "../api";

const fetchStockAnalysis = async (symbol) => {
    const url = `stock/news/analysis?symbol=${symbol}`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchStockAnalysis;
