import { fetchData } from '../api';

const fetchCryptoCardShortQuote = async ({ queryKey }) => {
    const id = queryKey[1];
    const url = `crypto/fullQuote?id=${id}&priceChangePercentage=24h`;
    const { data } = await fetchData(url);

    return data;
};

export default fetchCryptoCardShortQuote;
