import { fetchData } from '../api';

const fetchCryptoList = async ({ pageParam = 1 }) => {
    const url = `crypto/mostActive?pageNumber=${pageParam}&pageLimit=${16}`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchCryptoList;
