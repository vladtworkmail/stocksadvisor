import {fetchData} from "../api";

const fetchStockNews = async (symbol) => {
    const url = `news/symbolLastNews?symbol=${symbol}&limit=12`;
    const { data } = await fetchData(url);
    return data;
};

export default fetchStockNews;
