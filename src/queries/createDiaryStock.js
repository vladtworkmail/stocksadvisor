import {postData} from "../api";
import queryString from "query-string";

const createDiaryStock = async(params) => {
    const url = `user/diaryStock/create?${queryString.stringify(params)}`;
    const { data } = await postData(url);
    return data;
};

export default createDiaryStock;
