// routes
import Router from './routes';
// theme
import ThemeConfig from './theme';
import GlobalStyles from './theme/globalStyles';
// components
import ScrollToTop from './components/ScrollToTop';
import { BaseOptionChartStyle } from './components/charts/BaseOptionChart';
import CookieConsent from 'react-cookie-consent';
import './App.css';
// ----------------------------------------------------------------------
import {gapi} from 'gapi-script'
import {useEffect, useState} from "react";
import {Link} from "react-router-dom";
export default function App() {
    const CookiesConst = 'Accepted Cookies';
    const [debug] = useState(!localStorage.getItem(CookiesConst));

    const handleAcceptCookies = () => {
        localStorage.setItem(CookiesConst, 'true');
    };

    useEffect(() => {
        gapi.load('client:auth2', () => gapi.client.init({clientId: process.env.REACT_APP_GOOGLE_CLIENT_ID}));
    }, [])

  return (
    <ThemeConfig>
        <CookieConsent
            debug={debug}
            buttonText="Agree"
            onAccept={handleAcceptCookies}
        >
            Stocks Advisor uses cookies. Click
            <Link style={{backgroundColor: 'white', marginLeft: 4, marginRight: 4}} to="/terms/policy">here</Link>
            for more information
        </CookieConsent>
      <ScrollToTop />
      <GlobalStyles />
      <BaseOptionChartStyle />
      <Router />
    </ThemeConfig>
  );
}
